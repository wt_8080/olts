<%--
  Created by IntelliJ IDEA.
  User: WT
  Date: 2018/10/8
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>学生注册</title>
    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="vendor/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">

<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">OLTS</h1>

        </div>
        <h3>Register to OLTS</h3>
        <p>Create account to see it in action.</p>
        <div class="col-md-10 column col-lg-offset-1">
        <form class="m-t" role="form" action="user/register">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="学号" required="" name="stuNo">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="身份证" required="" name="idCardNo">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="用户名" required="" name="userName">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="密码" required="" name="passWord">
            </div>
            <div class="form-group">
                <input type="tel" class="form-control" placeholder="手机" required="" name="mobile">
            </div>
            <div class="form-group">
                <input type="tel" class="form-control" placeholder="家庭电话" required="" name="homeTel">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="家庭地址" required="" name="homeAddr">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="学校地址" required="" name="schAddr">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="QQ" required="" name="qq">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="邮箱" required="" name="email">
            </div>
<%--            <div class="form-group">
                <input type="radio" id="gender1" name="gender"><label for="gender1">男</label>
                <input type="radio" id="gender2" name="gender" checked="checked"><label for="gender2">女</label>
            </div>--%>
            <div class="form-group">
                        <div class="input-group">
                            <input type="text" placeholder="性别" class="form-control">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                   - <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="#">男</a>
                                    </li>
                                    <li>
                                        <a href="#">女</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
            </div>
            <div class="form-group">
                <input type="date" class="form-control" placeholder="出生日期" required="" name="birthday">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="籍贯" required="" name="nationPlace">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="专业" required="" name="marjor">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

            <a class="btn btn-sm btn-white btn-block" href="login.jsp">Login</a>
        </form>
        </div>

        <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2018</small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="vendor/js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
</body>

</html>
