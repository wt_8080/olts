<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/10/10 0010
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">更新课程信息</h4>
</div>
<div class="modal-body" id="update-modal-body">
    <!-- body -->
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <div class="row clearfix">
                    <%--<div class="col-md-1 column"></div>--%>
                    <div class="col-md-12 column text-center">

                        <form class="form-horizontal" role="form" id="updateForm">
                            <input type="hidden" name="cid" value="${courses.id}">
                            <div class="form-group">
                                <label for="id" class="col-sm-2 control-label text-right">序号</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="id" value="${courses.id}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="courseNameUpdate" class="col-sm-2 control-label">课程名</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="courseNameUpdate" name="courseNameUpdate" value="${courses.courseName}" placeholder="请输入姓名">
                                </div>
                            </div>
                        </form>
                    </div>
                    <%--<div class="col-md-1 column"> </div>--%>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal-footer">
    <!-- data-dismiss="modal" 该属性让模态框关闭 -->
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
    <button type="button" class="btn btn-primary" id="updateBtn">保存</button>
</div>
<div id="updateAlert" class="alert alert-success col-md-3 hide" style="margin-right: 5px;position: fixed; right: 5px; bottom: 5px;">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong>更新成功！</strong>结果是成功的。
</div>
<script>
    <%--$( document ).ready(function() {--%>
        <%--$.get('classify/selectCoursesById?id='+${param.id},function (actionFlag) {--%>
                <%--if (actionFlag){--%>
                    <%--console.log("success!");--%>
                    <%--$('#cid').val('${courses.id}');--%>
                <%--}else {--%>
                    <%--console.log("failed!");--%>
                <%--}--%>
            <%--},'json'--%>
        <%--);--%>
    <%--});--%>
    $(function () {
        $('#updateBtn').click(function () {
            var updateForm = document.getElementById('updateForm');
                //检查表单原生验证是否通过
            if (updateForm.checkValidity()) {
                var data = $('#updateForm').serialize();
                //send ajax request
                $.get('classify/updateCourses', data, function(json){ // {"actionFlag": true}
                    //1.把该表单的数据更新父窗口表格中相应的行
                    $('#tr_' + $('#cid').val() + ' td:eq(1)').text($('#courseNameUpdate').val());
                    //2. 关闭模态框窗口
                    if(json.actionFlag){
                        $('#myModal').modal('hide');
                        $('#updateAlert').show();
                        setTimeout("$('#updateAlert').hide()", 5000);
                    }
                },'json');
            }
            window.location.reload();
        });
    })
</script>
