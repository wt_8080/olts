<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    session.setAttribute("rootPath",request.getContextPath());
%>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <!-- Sweet alert -->
    <script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Sweet Alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>







<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li class="active">
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="classify/selectCourses">课程名</a></li>
                            <li class="active"><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>
                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>
                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/select/fillselect.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/select/fillselect.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>知识点管理</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>分类管理</a>
                    </li>
                    <li class="active">
                        <strong>知识点管理</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <%--                   <div class="ibox-title">
                                               <h5>Bootstrap Panels <small>Custom background colors.</small></h5>
                                           </div>--%>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            知识点管理
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">

                                                <label class="col-sm-3 control-label" style="height: 80px;">课程名：</label>
                                                <div class="col-sm-6">
                                                    <select id="usertype" size="1" name="usertype" class="selectpicker show-tick form-control"  data-max-options="1" data-live-search="true" required="">
                                                        <%--动态加载课程名--%>
                                                        <c:forEach var="course" items="${courses}">
                                                            <option value="${course.id}">${course.courseName}</option>
                                                        </c:forEach>
                                                    </select>
                                                    <br/>

                                                    <div class="row">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="tech_ctgr" placeholder="请输入课程对应知识点" required="">
                                                                <span class="input-group-btn">
                                        <button class="btn btn-success" type="button" onclick="saveTech()">保存</button>
                                        </span>
                                                            </div>
                                                            <br>
                                                            <div class="col-sm-1"></div>
                                                            <div class="col-sm-10">
                                                                <a href="javascript:void(0);" class="btn btn-default btn-lg" role="button" onclick="selectTech()">查询</a>&nbsp&nbsp&nbsp&nbsp
                                                                <a href="javascript:void(0);" class="btn btn-primary btn-lg" role="button" onclick="reset()">重置</a>
                                                            </div>
                                                            <div class="col-sm-1"></div>
                                                        </div>
                                                        <div class="col-sm-2"></div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-3">

                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 30px">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-10" id="tbody">
                                                    <%--此处Ajax插入数据表格--%>

                                                </div>
                                                <div class="col-md-1"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                </div>
                                                <div class="col-md-3"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>
<script src="vendor/js/bootbox/bootbox.min.js"></script>
<script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->

<script src="vendor/js/inspinia.js"></script>
<script src="vendor/js/plugins/pace/pace.min.js"></script>

<!-- Toastr -->
<script src="vendor/js/plugins/toastr/toastr.min.js"></script>

<script>

    function selectTech() {
        var id = $('#usertype option:selected').val();
        $("#tbody").load("tech/listTechByCourseId?id="+id);
    }

    function pageTech(x) {
        $("#tbody").load(x);
    }

    function saveTech(){
        var id = $('#usertype option:selected').val();
        var techCtgr = $('#tech_ctgr').val();
        $.ajax({
            url: 'tech/saveTech?courseId='+id+"&techCtgr="+techCtgr,
            type: 'get',
            dataType: 'json',
            beforeSend: function(){
                /*console.log("保存测试:"+id+" "+techCtgr);*/
                /* return $(dialog).find('form').valid(); */
            },
            success: function(result){
                console.log(result.success);
                if(result.success){
                    // 关闭
                    swal("保存成功","请继续操作","success");
                    $("#tbody").load("tech/listTechByCourseId?id="+id);
                }else{
                    alert(result.message);
                }
            },
            error: function(){
                swal("保存失败","请重试！","error");
            }

        });

    }

    function reset() {
        $('#tech_ctgr').val('');
    }

    <%--更新方法--%>
    function update(id,techCtgr,courseId){
        console.log("更新测试");
        $.get('tech/updatePage?id='+id+"&techCtgr="+techCtgr+"&courseId="+courseId,function(html){
            bootbox.dialog({
                title: '知识点更新',
                message: html,
                buttons:{
                    ok:{
                        label: '确定',
                        className: 'btn btn-info',
                        callback: function(){
                            var dialog = this;
                            // 使用ajax提交
                            $.ajax({
                                url: 'tech/updateTech',
                                type: 'post',
                                dataType: 'json',
                                data: $(dialog).find('form').serialize(),
                                beforeSend: function(){
                                    console.log("123");
                                    /* return $(dialog).find('form').valid(); */
                                },
                                success: function(result){
                                    console.log(result.success)
                                    if(result.success){
                                        // 关闭
                                        var courseId = $('#usertype option:selected').val();
                                        $(dialog).modal('hide');
                                        swal("更新成功","请继续操作","success");
                                        $("#tbody").load("tech/listTechByCourseId?id="+courseId);
                                    }else{
                                        alert(result.message);
                                    }
                                },
                                error: function(){
//                                    alert('服务器错误');
                                    swal("服务器错误", "服务器错误 :)", "error");
                                }

                            });


                            return false;
                        }
                    },
                    cancel:{
                        label: '取消',
                        className: 'btn btn-default'
                    }
                }
            });

        });
    }


    function deleteById(id,techCtgr,courseId){
        console.log("删除测试");
        $.get('tech/deletePage?id='+id+"&techCtgr="+techCtgr+"&courseId="+courseId,function(html){
            bootbox.dialog({
                title: '知识点删除',
                message: html,
                buttons:{
                    ok:{
                        label: '确定删除',
                        className: 'btn btn-info',
                        callback: function(){
                            var dialog = this;
                            // 使用ajax提交
                            $.ajax({
                                url: 'tech/deleteById',
                                type: 'post',
                                dataType: 'json',
                                data: $(dialog).find('form').serialize(),
                                beforeSend: function(){
                                    console.log("123");
                                    /* return $(dialog).find('form').valid(); */
                                },
                                success: function(result){
                                    console.log(result.success)
                                    if(result.success){
                                        // 关闭
                                        var courseId = $('#usertype option:selected').val();
                                        $(dialog).modal('hide');
                                        swal("删除成功","请继续操作","success");
                                        $("#tbody").load("tech/listTechByCourseId?id="+courseId);
                                    }else{
                                        alert(result.message);
                                    }
                                },
                                error: function(){
//                                    alert('服务器错误');
                                    swal("服务器错误", "服务器错误 :)", "error");

                                }

                            });
                            return false;
                        }
                    },
                    cancel:{
                        label: '取消',
                        className: 'btn btn-default'
                    }
                }
            });

        });
    }
</script>
<%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>



</body>
</html>
