<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: BLTM
  Date: 2018/10/8
  Time: 14:08
  To change this template use File | Settings | File Templates.
--%>
<%--此页面用于新增知识点时展示已有知识点--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<table class="table table-hover">
    <thead>
        <tr>
            <td class="active">序号</td>
            <td class="active">知识点</td>
            <td class="active">操作</td>
        </tr>
    </thead>
    <c:forEach items="${page}" var="tech">
        <tr class="active">
            <td class="active">${tech.id}</td>
            <td class="active">${tech.techCtgr}</td>
            <td class="actions">

                <a href="javascript:void(0);"
                   class="btn btn-info btn-sm" role="button"
                   onclick="update(${tech.id},'${tech.techCtgr}',${tech.courseId})">
                    <span class="glyphicon glyphicon-refresh"></span> 更新
                </a>
                <a href="javascript:void(0);"
                   class="btn btn-danger btn-sm" role="button" onclick="deleteById(${tech.id},'${tech.techCtgr}',${tech.courseId})">
                    <span class="glyphicon glyphicon-trash"></span> 删除
                </a>
            </td>
        </tr>
    </c:forEach>
    <tr>
        <td style="column-span: 3">
                <jsp:include page="pagingTech.jsp">
                    <jsp:param name="contextPath" value="tech/listTechByCourseId?id=${page[0].courseId}"/>
                    <jsp:param name="page" value="page"/>
                </jsp:include>
        </td>
    </tr>

</table>
