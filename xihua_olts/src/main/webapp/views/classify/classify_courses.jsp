<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/10/8 0008
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    session.setAttribute("rootPath",request.getContextPath());
%>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>
    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>

</head>

<body>





    <div id="wrapper">
        <!-- 左侧导航栏 -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">

                    <!-- 用户信息 -->
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                                <li>用户名：${sessionScope.logUser.userName}</li>
                                <li>性别：${sessionScope.logUser.gender}</li>
                                <li>QQ:${sessionScope.logUser.qq}</li>
                                <li>专业：${sessionScope.logUser.marjor}</li>
                                <li>手机：${sessionScope.logUser.mobile}</li>
                                <li>身份证：${sessionScope.logUser.idCardNo}</li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <!-- 导航链接 -->
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                                <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                                <li><a href="views/add/completion.jsp">填空题新增</a></li>
                                <li><a href="views/add/checking.jsp">判断题新增</a></li>
                                <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                                <li><a href="views/add/programming.jsp">编程题新增</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                                <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                                <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                                <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                                <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                                <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${logUser.userType != null}">
                        <li  class="active">
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="classify/selectCourses">课程名</a></li>
                                <li><a href="tech/listCourses">知识点</a></li>
                            </ul>
                        </li>
                    </c:if>

                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${logUser.userType == null}">
                                <li class="active"><a href="paper/selectPaper">考试</a></li>
                            </c:if>
                            <c:if test="${logUser.userType != null}">
                                <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                                <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                                <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                            </c:if>
                        </ul>
                    </li>

                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${logUser.userType !=null}">
                                <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                                <li><a href="user/search">批量操作用户</a></li>
                                <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                            </c:if>
                            <c:if test="${logUser.userType == null}">
                                <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                            </c:if>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                        </li>
                        <li>
                            <a href="user/exit">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-12">
                    <h2>分类管理</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.jsp">首页</a>
                        </li>
                        <li>
                            <a>课程管理</a>
                        </li>
                        <li class="active">
                            <strong>课程管理</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <%--                   <div class="ibox-title">
                                                   <h5>Bootstrap Panels <small>Custom background colors.</small></h5>
                                               </div>--%>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                课程管理
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-10 column col-lg-offset-1">
                                                    <div class="col-md-12 column" style="border: groove ;margin: 5px;padding: 5px;">
                                                        <h2 class="text-center">新增课程</h2><br>
                                                        <form class=" form-horizontal" role="form" action="classify/insertCourses" method="post">
                                                            <div class="form-group">
                                                                <label for="courseName" class="col-sm-2 control-label">课程名：</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" id="courseName" name="courseName" required=""/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class="btn btn-lg btn-primary">保存</button> &nbsp;&nbsp;&nbsp;
                                                                    <button type="reset" class="btn btn-warning btn-lg">重置</button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                    <h2 class="text-center">查询结果</h2><br>
                                                    <table class="table table-bordered table-hover text-center">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">
                                                                序号
                                                            </th>
                                                            <th class="text-center">
                                                                课程名
                                                            </th>
                                                            <th  class="text-center">
                                                                操作
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach var="courses" items="${mypage}" varStatus="stat">
                                                            <tr class="active" id="tr_${courses.id}">
                                                                <td>${courses.id}</td>
                                                                <td>${courses.courseName}</td>
                                                                <td>
                                                                    <a id="toUpdateBtn" cid="${courses.id}" onclick="toUpdate(this)" href="#" class="btn btn-info btn-sm"
                                                                       data-toggle="modal" data-target="#myModal" data-backdrop="static">
                                                                        <span class="glyphicon glyphicon-refresh"></span> 更新
                                                                    </a>
                                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" cid="${courses.id}" id="deleteBtn"
                                                                       data-toggle="modal" data-target="#deleteModal" data-backdrop="static">
                                                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                        <!-- 分页栏 -->
                                                        <tr class="page-nav">
                                                            <td colspan='3'>
                                                                <jsp:include page="../../paging.jsp">
                                                                    <jsp:param name="contextPath" value="classify/selectCourses"/>
                                                                    <jsp:param name="page" value="mypage"/>
                                                                </jsp:include>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="pull-right">
                    蓝桥小组
                </div>
                <div>
                    <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
                </div>
            </div>
        </div>
    </div>



<%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>

<!-- update Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="myModal-content">
            <!-- 更新员工的信息表单的内容部分，加入到这里 -->

        </div>
    </div>
</div>

<script>
    //ready
    // href="classify/updateCourses"
    function toUpdate(c) {

        $('#myModal-content').empty();
        //console.log($(e).attr('empno'));

        var id = $(c).attr('cid');
        //加载更新员工的信息表单，并加入到模态框的content部分
        console.log("2");
        $.get('classify/selectCoursesById?id=' + id, function (html) {
            //console.dirxml(html)
            $('#myModal-content').append(html);
        }, 'html');

    }

</script>

<!-- delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel"> 删除确认 </h4>
            </div>
            <div class="modal-body">
                <span class="fa fa-exclamation fa-2x" style="color:#f15b6c;"></span>
                您真的要删除序号为：<span id="idMsg"></span> 的课程吗？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button id="deleteConfirmBtn" type="button" class="btn btn-primary">删除</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<div id="deleteAlert" class="alert alert-success col-md-3" style="margin-right: 5px;position: fixed; right: 5px; bottom: 5px;">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong>删除成功！</strong>结果是成功的。
</div>
<script>
    //ready
    // href="classify/deleteCourses"

        $('#deleteAlert').hide();

        //处理当模态框打开时的事件：show.bs.modal
        $('#deleteModal').on('show.bs.modal', function (event) { //event表示的是点元素产生事件，此处是<a id="deleteBtn">...</a>的事件
            // 获得触发事件的源头的元素
            //console.dirxml(event.relatedTarget);
            var id = $(event.relatedTarget).attr('cid');
            $('#idMsg').text(id);
                $('#deleteConfirmBtn').click(function () {
                    //发送异步请求
                    //url传参数
                $.get('classify/deleteCourses?id=' + id,function (json) {
                    if (json.actionFlag) {
                        //关闭模态框
                        $('#deleteModal').modal('hide');
                        //1.提示删除成功
                        $('#deleteAlert').show();
                        setTimeout("$('#deleteAlert').hide()", 5000);
                        //2. 把表格的行移除
                        $('#tr_'+id).remove();
                    }
                },'json');
            });
        });


</script>

</body>
</html>
