<%--
  Created by IntelliJ IDEA.
  User: BLTM
  Date: 2018/10/12
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form id="form-student" class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-3">序号</label>
        <div class="col-sm-6">
            <input name="id" class="form-control" value="${tech.id}"  readonly="readonly">
        </div>
        <div class="form-right-tip"></div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">知识点</label>
        <div class="col-sm-6">
            <input name="techCtgr" class="form-control" value="${tech.techCtgr}" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">课程名序号</label>
        <div class="col-sm-6">
            <input name="courseId" class="form-control" value="${tech.courseId}" readonly>
        </div>
    </div>
</form>
