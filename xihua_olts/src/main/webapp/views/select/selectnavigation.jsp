<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wxk
  Date: 2018/10/8
  Time: 9:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--    <base href="${sessionScope.rootPath}/"/>--%>
    <base href="${pageContext.request.contextPath}/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <link href="vendor/prettify/prettify-my.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>
    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>
    <!-- Sweet alert -->
    <script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Sweet Alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- CodeMirror -->
    <script src="vendor/js/plugins/codemirror/codemirror.js"></script>
    <script src="vendor/js/plugins/codemirror/mode/javascript/javascript.js"></script>
    <script src="vendor/prettify/prettify.js"></script>
    <title>试题查询</title>
    <script type="text/javascript">
        $(function () {
            //google pretty
            prettyPrint();

            $.ajax({
                url: "select/load_course",  // 请求URL
                type: "GET",
                data: {}, //请求的参数
                dataType: "json", // 响应的内容类型：text, html, json, script, xml, jsonp
                success: function (resp) {
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.courseName, item.id);
                        $('#courseid').append(opt);
                    });
                }
            });
            //选择课程时加载
            $('#courseid').change(function () {
                console.log("课程编号：" + $('#courseid').val());
                $('#techId').empty();
                $('#techId').append('<option value="">不限--</option>');

                $.get("select/load_TechCategory", {"id": $('#courseid').val()}, function (resp) {
                    console.dirxml(resp);
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.techCtgr, item.id);
                        $('#techId').append(opt);
                    });
                }, 'json');
            });

        });


    </script>


    <script>
        $(document).ready(function(){

            var editor_one = CodeMirror.fromTextArea(document.getElementById("code1"), {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,

            });

        });
    </script>

</head>




<body>

<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li class="active">
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li  class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>单选题查询</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>单选题查询</a>
                    </li>
                    <li class="active">
                        <strong>单选题查询</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <%--                   <div class="ibox-title">
                                               <h5>Bootstrap Panels <small>Custom background colors.</small></h5>
                                           </div>--%>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            单选题查询
                                            <br>
                                            <ul class="nav nav-tabs">
                                                <li  class="active">
                                                    <a href="views/select/selectnavigation.jsp">查询单项选择题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/muticheckselect.jsp">查询多项选择题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/judgeselect.jsp">查询判断题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/fillselect.jsp">查询填空题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/askselect.jsp">查询简答题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/programselect.jsp">查询编程题</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">

                                            <form action="select/searchsmd_radio" method="get">
                                                <div class="form-group">
                                                    <input type="text" name="questionType" value="1" hidden>
                                                    <label class="col-sm-2 control-label" for="courseid">*题干：</label>
                                                    <div>
                                                        <div class="col-sm-6 input-group">

                                                            <input type="text" name="question" class="form-control" placeholder="请输入你要搜索的题干">
                                                        </div>
                                                        <br>
                                                    </div>
                                                    <br><br>
                                                    <label class="col-sm-2 control-label" for="courseid">*课程：</label>
                                                    <div class="col-sm-4">
                                                        <select id="courseid" class="form-control" name="courseId">
                                                            <option value="">--不限--</option>

                                                        </select>
                                                    </div>
                                                    <label class="col-sm-2 control-label" for="techId">*知识点：</label>
                                                    <div class="col-sm-4">
                                                        <select id="techId" class="form-control" name="techCateId">
                                                            <option value="">--不限--</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br><br>
                                                <div class="row clearfix">
                                                    <div class="col-md-6 column" align="center">
                                                        <button type="submit" class="btn btn-info">查询</button>
                                                    </div>
                                                    <div class="col-md-6 column">
                                                        <button type="reset" class="btn btn-warning" align="center">重置</button>
                                                    </div>
                                                </div>
                                            </form>

                                            <c:forEach items="${mypage}" var="smdQuestion" varStatus="stat">
                                                <div class="panel-group" id="panel-${stat.index+847716}">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <a class="panel-title collapsed" data-toggle="collapse"
                                                               data-parent="#panel-${stat.index+847716}"
                                                               href="#panel-element-${stat.index+932996}">
                                                                <h4><c:out value="${stat.index+1}:${smdQuestion.question}" escapeXml="false"/></h4>
                                                            </a>
                                                        </div>
                                                        <div id="panel-element-${stat.index+932996}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                    <%--选项--%>
                                                                <div class="form-group">
                                                                    <label>*选项：(正确答案请选中前面的单选按钮)</label>
                                                                    <br>

                                                                    <div class="input-group input-group-lg">
                                                                        <span class="input-group-addon blue-bg ">A:</span>
                                                                        <input readonly type="text" class="form-control inline" placeholder="A选项内容"
                                                                               required="" name="optionA" value=${smdQuestion.smdOptions.optionA}>
                                                                        </span>
                                                                    </div>
                                                                    <BR>
                                                                    <div class="input-group input-group-lg">
                                                                        <span class="input-group-addon blue-bg ">B:</span>
                                                                        <input readonly type="text" class="form-control inline" placeholder="B选项内容"
                                                                               required="" name="optionB" value=${smdQuestion.smdOptions.optionB}>
                                                                        </span>
                                                                    </div>
                                                                    <br>
                                                                    <div class="input-group input-group-lg">
                                                                        <span class="input-group-addon blue-bg ">C:</span>
                                                                        <input readonly type="text" class="form-control inline" placeholder="C选项内容"
                                                                               required="" name="optionC" value=${smdQuestion.smdOptions.optionC}>
                                                                        </span>
                                                                    </div>
                                                                    <br>
                                                                    <div class="input-group input-group-lg">
                                                                        <span class="input-group-addon blue-bg ">D:</span>
                                                                        <input readonly type="text" class="form-control inline" placeholder="D选项内容"
                                                                               required="" name="optionD" value=${smdQuestion.smdOptions.optionD}>
                                                                    </div>
                                                                </div>

                                                                <font color="red">正确答案是：${smdQuestion.correct}</font>
                                                                <br><br>
                                                                出题描述：${smdQuestion.descrpt}
                                                                <br><br>

                                                                <div class="row clearfix">
                                                                    <div class="col-md-6 column" align="center">

                                                                        <div class="text-center">
                                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                                    data-target="#myModal${stat.index}">
                                                                                更新
                                                                            </button>
                                                                        </div>


                                                                        <div class="modal inmodal" id="myModal${stat.index}" tabindex="-1" role="dialog"
                                                                             aria-hidden="true">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content animated bounceInRight">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">
                                                                                            <span aria-hidden="true">&times;</span><span
                                                                                                class="sr-only">Close</span></button>

                                                                                        <h5 class="modal-title">修改多选题</h5>

                                                                                    </div>
                                                                                    <form action="select/updateChoice1">
                                                                                        <div class="modal-body">


                                                                                            <h4>题干：</h4>
                                                                                            <input type="hidden" value="${smdQuestion.id}" name="id">
                                                                                            <input type="hidden" value="${smdQuestion.id}" name="smdOptions.questionId">
                                                                                            <input  type="text" class="form-control inline"  name="question" value=${smdQuestion.question} >

                                                                                            <div class="input-group input-group-lg">



                                                                                                <span class="input-group-addon blue-bg ">A:</span>
                                                                                                <input  type="text" class="form-control inline" placeholder="A选项内容" required="" name="smdOptions.optionA" value=${smdQuestion.smdOptions.optionA} >
                                                                                                </span>
                                                                                            </div>
                                                                                            <BR>
                                                                                            <div class="input-group input-group-lg">
                                                                                                <span class="input-group-addon blue-bg ">B:</span>
                                                                                                <input  type="text" class="form-control inline" placeholder="B选项内容" required="" name="smdOptions.optionB" value=${smdQuestion.smdOptions.optionB}>
                                                                                                </span>
                                                                                            </div>
                                                                                            <br>
                                                                                            <div class="input-group input-group-lg">
                                                                                                <span class="input-group-addon blue-bg ">C:</span>
                                                                                                <input  type="text" class="form-control inline" placeholder="C选项内容" required="" name="smdOptions.optionC" value=${smdQuestion.smdOptions.optionC}>
                                                                                                </span>
                                                                                            </div>
                                                                                            <br>
                                                                                            <div class="input-group input-group-lg">
                                                                                                <span class="input-group-addon blue-bg ">D:</span>
                                                                                                <input  type="text" class="form-control inline" placeholder="D选项内容" required="" name="smdOptions.optionD" value=${smdQuestion.smdOptions.optionD}>
                                                                                                </span>
                                                                                            </div>


                                                                                        </div>

                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-white"
                                                                                                    data-dismiss="modal">关闭
                                                                                            </button>
                                                                                            <button type="submit" class="btn btn-primary">保存</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>

                                                                            </div>
                                                                        </div>




                                                                    </div>

                                                                    <div class="col-md-6 column">
                                                                            <%-- <form action="" method="post">--%>
                                                                        <input type="text" name="id" value="${smdQuestion.id}" id="deleteNo" hidden>
                                                                        <button class="btn btn-warning btn-md demo3" onclick="getId(${smdQuestion.id})">删除</button>
                                                                            <%--</form>--%>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                            <c:if test="${mypage != null}">
                                                <jsp:include page="../../paging.jsp">
                                                    <jsp:param name="contextPath" value="select/searchsmd_radio"/>
                                                    <jsp:param name="page" value="mypage"/>
                                                </jsp:include>
                                            </c:if>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>
<script>
    function getId(id) {
        swal({
            title: "确认删除吗?",
            text: "删除后无法恢复！！!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Deleted",
            closeOnConfirm: false
        }, function () {
            $.get("select/deleteJudge",{id:id},function (resp) {
                console.log(resp);

                if(resp){
                    swal("Deleted!", "你的删除已成功！", "success","","","",true);
                    window.location.href="select/searchsmd_radio?questionType=1";
                }
            },'json');

        });

    }
</script>
</body>


</html>
