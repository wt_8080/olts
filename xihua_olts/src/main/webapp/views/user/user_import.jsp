<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">

        <!-- 左侧导航栏 -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">

                <ul class="nav metismenu" id="side-menu">

                    <!-- 用户信息 -->
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                                <li>用户名：${sessionScope.logUser.userName}</li>
                                <li>性别：${sessionScope.logUser.gender}</li>
                                <li>QQ:${sessionScope.logUser.qq}</li>
                                <li>专业：${sessionScope.logUser.marjor}</li>
                                <li>手机：${sessionScope.logUser.mobile}</li>
                                <li>身份证：${sessionScope.logUser.idCardNo}</li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <!-- 导航链接 -->
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                                <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                                <li><a href="views/add/completion.jsp">填空题新增</a></li>
                                <li><a href="views/add/checking.jsp">判断题新增</a></li>
                                <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                                <li><a href="views/add/programming.jsp">编程题新增</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                                <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                                <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                                <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                                <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                                <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/select/selectnavigation.jsp">课程名</a></li>
                                <li><a href="views/select/muticheckselect.jsp">知识点</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${logUser.userType == null}">
                                <li class="active"><a href="views/select/selectnavigation.jsp">考试</a></li>
                            </c:if>
                            <c:if test="${logUser.userType != null}">
                                <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                                <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                                <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                            </c:if>
                        </ul>
                    </li>

                    <li class="active">
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${logUser.userType !=null}">
                                <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                                <li><a href="user/search">批量操作用户</a></li>
                                <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                            </c:if>
                            <c:if test="${logUser.userType == null}">
                                <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                            </c:if>
                        </ul>
                    </li>

                </ul>

            </div>
        </nav>


        <!-- 页面显示 -->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <!-- 上面导航 -->
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>
        </div>
            <!-- 上面导航 -->
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">欢迎来到蓝桥考试系统</span>
                </li>
                <li>
                    <a href="user/loginOut">
                        <i class="fa fa-sign-out"></i> 退出系统
                    </a>
                </li>
            </ul>
        </nav>
        </div>


            <div class="row  border-bottom white-bg dashboard-header">
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>批量导入用户</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.jsp">主页</a>
                            </li>
                            <li>
                                <a>用户管理</a>
                            </li>
                            <li class="active">
                                <strong>批量导入用户</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="panel panel-primary" style="margin-top: 20px">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            导入学生信息
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form action="javascript:void(0);" class="form-inline" method="post" enctype="multipart/form-data">
                            <div class="input-group">
                                <div class="input-group-addon">学生信息excel表：</div>
                                <input type="file" class="form-control" id="file" name="file">
                            </div>
                            <div class="input-group">
                                <button type="button" id="submit" class="btn btn-info">提交</button>
                            </div>
                            <div class="input-group">
                                <button type="reset" class="btn btn-default">重置</button>
                            </div>
                        </form>
                        <div style="color: red">
                            注意事项：Excel表格式说明如下<br>
                            &nbsp;&nbsp;1、导入的学生信息必须在Excel文件的第一个工作表中；<br>
                            &nbsp;&nbsp;2、学生信息要从第二行开始，<b>从第二列开始分别为学号，身份证号，最高学历，专业，手机号码，QQ号码</b>；<br>
                            &nbsp;&nbsp;3、<br><img src="upload/Excel.jpg" style="border: 1px solid red;margin-left: 20px;">
                        </div>

                    </div>
                </div>


            </div>
            <div class="wrapper wrapper-content"></div>
            <div class="footer">
                <div class="pull-right">
                    蓝桥小组
                </div>
                <div>
                    <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
                </div>
            </div>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->

    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>

    <!-- Sweet alert -->
    <script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>

<%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>
<script>
    $('#submit').on('click',function () {
        var formData = new FormData();
        var file = document.getElementById("file").files[0];
        formData.append('file', file);
        $.ajax({
            url: "file/addStuALL",
            type: "post",
            data: formData,
            contentType: false,
            processData: false,
            mimeType: "multipart/form-data",
            success: function (resp) {
                if(JSON.parse(resp).msg){
                    swal({
                        title: "成功",
                        text: "上传学生信息成功",
                        type: "success"
                    });
                }else {
                    swal({
                        title: "失败",
                        text: "上传学生信息失败",
                        type: "error"
                    });

                }
            }
        });
    });
</script>
</body>
</html>
