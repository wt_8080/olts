<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="upload/favicon2.ico" type="image/x-icon">
    <link rel="bookmark" href="upload/favicon2.ico" type="image/x-icon"/>

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="vendor/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <!-- Fileinput -->
    <link href="vendor/bootstrap-fileinput/fileinput.css" rel="stylesheet" >


</head>

<body>
    <div id="wrapper">

        <!-- 左侧导航栏 -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">

                <ul class="nav metismenu" id="side-menu">

                    <!-- 用户信息 -->
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                                <li>用户名：${sessionScope.logUser.userName}</li>
                                <li>性别：${sessionScope.logUser.gender}</li>
                                <li>QQ:${sessionScope.logUser.qq}</li>
                                <li>专业：${sessionScope.logUser.marjor}</li>
                                <li>手机：${sessionScope.logUser.mobile}</li>
                                <li>身份证：${sessionScope.logUser.idCardNo}</li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <!-- 导航链接 -->
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                                <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                                <li><a href="views/add/completion.jsp">填空题新增</a></li>
                                <li><a href="views/add/checking.jsp">判断题新增</a></li>
                                <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                                <li><a href="views/add/programming.jsp">编程题新增</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                                <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                                <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                                <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                                <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                                <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${logUser.userType != null}">
                        <li>
                            <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="classify/selectCourses">课程名</a></li>
                                <li><a href="tech/listCourses">知识点</a></li>
                            </ul>
                        </li>
                    </c:if>
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${logUser.userType == null}">
                                <li class="active"><a href="views/select/selectnavigation.jsp">考试</a></li>
                            </c:if>
                            <c:if test="${logUser.userType != null}">
                                <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                                <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                                <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                            </c:if>
                        </ul>
                    </li>

                    <li class="active">
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${logUser.userType !=null}">
                                <li><a href="views/user/user_import.jsp">批量导入用户</a></li>
                                <li class="active"><a href="user/search">批量操作用户</a></li>
                                <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                            </c:if>
                            <c:if test="${logUser.userType == null}">
                                <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                            </c:if>
                        </ul>
                    </li>

                </ul>

            </div>
        </nav>


        <!-- 页面显示 -->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <!-- 上面导航 -->
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>
        </div>
            <!-- 上面导航 -->
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">欢迎来到蓝桥考试系统</span>
                </li>
                <li>
                    <a href="user/loginOut">
                        <i class="fa fa-sign-out"></i> 退出系统
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row  border-bottom white-bg dashboard-header">

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>批量操作用户</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.jsp">主页</a>
                            </li>
                            <li>
                                <a>用户管理</a>
                            </li>
                            <li class="active">
                                <strong>批量操作用户</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>

                <div class="panel panel-primary" style="margin-top: 20px">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            查询信息
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form action="user/search" class="form-inline" method="post">
                            <div class="input-group">
                                <div class="input-group-addon">专业：</div>
                                <input type="text" class="form-control" name="marjor" value="${sessionScope.selectBySelective}" placeholder="请输入学生专业">
                            </div>
                            <div class="input-group">
                                <button type="submit" class="btn btn-info">查询</button>
                            </div>
                            <div class="input-group">
                            <button type="reset" class="btn btn-default">重置</button>
                            </div>
                        </form>
                        <c:if test="${not empty page}">
                            <table class="table progress-striped">
                                <caption class="text-center"><b>学生列表</b></caption>
                                <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>姓名</th>
                                    <th>学号</th>
                                    <th>手机</th>
                                    <th>专业</th>
                                    <th>毕业院校</th>
                                    <th colspan="2">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="u" items="${page}">
                                    <tr>
                                        <td><input type="checkbox" id="${u.id}"></td>
                                        <td>${u.userName}</td>
                                        <td>${u.stuNo}</td>
                                        <td>${u.mobile}</td>
                                        <td>${u.marjor}</td>
                                        <td>${u.graduateSchool}</td>
                                        <td>
                                            <a class="updateById" id="${u.id}" data-toggle="modal" data-target="#update_modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        </td>
                                        <td>
                                            <a class="deleteById" id="${u.id}" stu_id="${u.stuNo}"><i class="glyphicon glyphicon-minus"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <input type="checkbox" id="checkAll">全选
                                        <a href="javascript:void(0)" id="deleteByAll" class="btn btn-danger">批量删除</a>
                                         &nbsp;&nbsp;总${page.getTotal()}条 &nbsp;&nbsp; 第${page.getPageNum()}页/共${page.getPages()}页
                                    </td>
                                    <td colspan="4" class="text-right">
                                        <jsp:include page="../../paging.jsp">
                                            <jsp:param name="contextPath" value="user/search"/>
                                            <jsp:param name="page" value="page"/>
                                        </jsp:include>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </c:if>

                    </div>
                </div>



            </div>
            <div class="wrapper wrapper-content"></div>
            <div class="footer">
                <div class="pull-right">
                    蓝桥小组
                </div>
                <div>
                    <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
                </div>
            </div>

        </div>
    </div>

    <div class="modal inmodal fade" id="update_modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">信息修改</h4>
                </div>
                <div class="modal-body" id="updateUser-content">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary" id="save_update">保存</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>

    <!-- Sweet alert -->
    <script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- validate -->
    <script src="vendor/jquery-validation-1.16.0/jquery.validate.min.js"></script>

    <!-- iCheck -->
    <script src="vendor/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Fileinput -->
    <script src="vendor/bootstrap-fileinput/fileinput.js"></script>
    <%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>
<script>
    $(function () {
        /*表格序号*/
        var len = $('tbody tr').length;
        for(var i = 1;i<=len;i++){
            $('table tr:eq('+i+') td:first').append(' '+ i);
        };

        /*全选*/
        $('#checkAll').on('click',function () {
            for(var i = 0; i < $('tbody input').length; i++) {
                $('tbody input')[i].checked = this.checked;
            }
        });

        /*批量删除*/
        $('#deleteByAll').on('click',function () {
            var url = 'user/deleteById?';
            var item = 0;
            for(var i = 0; i < $('tbody input').length; i++) {
                if($('tbody input')[i].checked){
                    url=url+'&id='+$($('tbody input')[i]).attr('id');
                    item++;
                }
            };
            swal({
                    title: "确定删除",
                    text: "即将删除"+ item +"条学生信息！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "删除",
                    cancelButtonText: "取消",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href=url;
                    } else {
                        swal("已取消", "你取消了删除", "error");
                    }
                });
        });

        /*删除*/
        $('.deleteById').on('click',function () {
            var id = $(this).attr('id');
            swal({
                    title: "确定删除",
                    text: "即将删除学号为"+$(this).attr('stu_id')+"的学生信息！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "删除",
                    cancelButtonText: "取消",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href="user/deleteById?id="+id;
                    } else {
                        swal("已取消", "你取消了删除", "error");
                    }
                });
        });

        /*修改学生信息*/
        $('.updateById').on('click',function () {
            $('#updateUser-content').empty();
            var no = $(this).attr('id');
            $.get('views/user/update_form.jsp?id='+no, function (html) {
                $('#updateUser-content').append(html);
            }, 'html');
        });

        /*保存修改*/
        $('#save_update').on('click',function () {
            if($('#update-form').valid()){
                $('#update-form').submit();
            }
        });
    });
</script>
</body>
</html>
