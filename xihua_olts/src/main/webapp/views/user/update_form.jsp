<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    .form-right-tip .error{
        color: red;
    }
    .form-right-tip .success{
        color: green;
    }
</style>
<form class="form-horizontal" id="changePathFrom" enctype="multipart/form-data" >
    <div class="form-group">
        <div class="col-md-1">头像</div>
        <div class="col-md-8">
            <div class="fileinput fileinput-new" data-provides="fileinput" id="uploadImageDiv">
                <div class="fileinput-new thumbnail">
                    <img id="path" src="" alt="" style="max-width: 100px; max-height: 100px;"/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"
                     style="max-width: 100px; max-height: 100px;"></div>
                <div>
                        <span class="btn default btn-file">
                            <span class="fileinput-new btn btn-info">选择图片</span>
                            <span class="fileinput-exists btn btn-info">更改</span>
                            <input type="file" name="uploadImage" id="uploadImage"/>
                        </span>
                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">移除</a>
                    <span class="btn default btn-file">
                        <a class="btn btn-success fileinput-exists" id="changePath">更改头像</a>
                    </span>
                </div>
            </div>
        </div>

    </div>
</form>

<form id="update-form" class="form-horizontal" action="user/updateById" method="get">
    <div class="form-group">
        <label class="control-label col-sm-1">学号</label>
        <div class="col-sm-3">
            <input id="id" name="id" value="${param.id}" type="hidden">
            <input id="stuNo" name="stuNo" class="form-control" readonly>
        </div>
        <label class="control-label col-md-2">身份证号</label>
        <div class="col-md-4">
            <input id="idCardNo" name="idCardNo" class="form-control">
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-1">性别</label>
        <div class="col-md-2">
            <div class="i-checks pull-left"><label><input type="radio" name="gender" id="man" value="男"> <i></i> 男 </label></div>
            <div class="i-checks pull-right"><label><input type="radio" name="gender" id="woman" value="女"> <i></i> 女 </label></div>
        </div>
        <label class="control-label col-md-2 col-md-offset-1">出生日期</label>
        <div class="col-md-4">
            <input id="birthday" name="birthday" type="date" class="form-control">
        </div>
        <div class="col-md-2 form-right-tip"></div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-1">姓名</label>
        <div class="col-md-2">
            <input id="userName" name="userName" class="form-control">
        </div>
        <div class="col-md-1 form-right-tip"></div>
        <label class="control-label col-md-2">旧密码</label>
        <div class="col-md-4">
            <input id="oldPsd" type="hidden">
            <input id="oldPassWord" name="oldPassWord" type="text" class="form-control">
        </div>
        <div class="col-md-2 form-right-tip"></div>
    </div>
    <hr>
    <div class="form-group">
        <label class="control-label col-md-1">密码</label>
        <div class="col-md-3">
            <input id="newPassWord" type="password" name="passWord" class="form-control">
        </div>
        <label class="control-label col-md-2">确认密码</label>
        <div class="col-md-3">
            <input id="passWord2" name="passWord2" type="password" class="form-control">
        </div>
        <div class="col-md-3 form-right-tip"></div>
    </div>
    <hr>
    <div class="form-group">
        <label class="control-label col-md-1">手机</label>
        <div class="col-md-4">
            <input id="mobile" name="mobile" class="form-control">
        </div>
        <label class="control-label col-md-2">家庭电话</label>
        <div class="col-md-4">
            <input id="homeTel" name="homeTel" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-1">QQ</label>
        <div class="col-md-4">
            <input id="qq" name="qq" class="form-control">
        </div>
        <label class="control-label col-md-2">Email</label>
        <div class="col-md-4">
            <input id="email" name="email" class="form-control">
        </div>
        <div class="col-md-1 form-right-tip"></div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-1">籍贯</label>
        <div class="col-md-4">
            <input id="nationPlace" name="nationPlace" class="form-control">
        </div>
        <label class="control-label col-md-2">家庭住址</label>
        <div class="col-md-4">
            <input id="homeAddr" name="homeAddr" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-1">专业</label>
        <div class="col-md-4">
            <input id="marjor" name="marjor" class="form-control">
        </div>
        <label class="control-label col-md-2">毕业院校</label>
        <div class="col-md-4">
            <input id="graduateSchool" name="graduateSchool" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-1">学历</label>
        <div class="col-md-4">
            <select id="eduBackground" name="eduBackground" class="form-control">
                <option value="高中">高中</option>
                <option value="专科">专科</option>
                <option value="本科">本科</option>
            </select>
        </div>
        <label class="control-label col-md-2">学校地址</label>
        <div class="col-md-4">
            <input id="schAddr" name="schAddr" class="form-control">
        </div>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
    function FormatDate (strTime) {
        var now = new Date(strTime);
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var date = now.getFullYear()+"-"+(month)+"-"+(day) ;
        return date;
    }
    $(function () {
        var id = $('#id').attr("value");
        $.get('user/load_user_update/' + id,function (resp) {
            $.each(resp, function (i, item) {
                if(i!='gender'&&i!='eduBackground'&&i!='passWord'&&i!='path') {
                    if(i == 'birthday'){
                        var date = FormatDate(item);

                        $('#' + i).val(date);
                    }else {
                        $('#' + i).attr('value',item);
                    };
                }else if(i == 'gender'){
                    $('input[value="'+item+'"]').parent().attr('class','iradio_square-green checked');
                }else if(i == 'eduBackground'){
                    $('#eduBackground option[value="'+item+'"]').attr('selected','selected');
                }else if(i == 'passWord'){
                    $('#oldPsd').attr('value',item);
                }else if(i == 'path'){
                    $('#path').attr('src','upload/'+item)
                };
            });
        });
    });
    //表单验证
    $('#update-form').validate({
        errorElement: 'span',
        errorPlacement: function(error, input){

            input.parent().next().append(error);

        },
        success: function(error){
            error.html('<i class="fa fa-check success"></i>');
        },
        rules: {
            oldPassWord:{
                equalTo: "#oldPsd"
            },
            passWord2:{
                minlength:6,
                equalTo: "#newPassWord"
            },
            email:{
                email: true
            }
        },
        messages: {
            oldPassWord: {
                equalTo: '<i class="fa fa-remove"></i> 旧密码不正确'
            },
            passWord2: {
                minlength:'<i class="fa fa-remove"></i> 密码长度不能小于6',
                equalTo: '<i class="fa fa-remove"></i> 两次密码不一样'
            },
            email: {
                email: '<i class="fa fa-remove"></i>邮箱格式不对'
            }
        }
    });
    /*头像添加*/
    $("#uploadImage").on("fileuploadadd", function(e, data) {
        // 当文件添加上去时候调用
        $("#titleImageError").html("");
        data.submit()
    });

    $('#changePath').on('click',function () {
        var formData = new FormData();
        var id = $('#id').attr("value");
        var file = document.getElementById("uploadImage").files[0];
        formData.append('id', id);
        formData.append('file', file);
        $.ajax({
            url: "file/changePath",
            type: "post",
            data: formData,
            contentType: false,
            processData: false,
            mimeType: "multipart/form-data",
            success: function (resp) {
                $('#uploadImageDiv').attr('class','fileinput fileinput-new');
                if(JSON.parse(resp).msg){
                    $('#path').attr('src','upload/'+ JSON.parse(resp).path +'');
                }else {
                    swal({
                        title: "失败",
                        text: "修改头像失败",
                        type: "error"
                    });

                }
            }
        });
    });
</script>


