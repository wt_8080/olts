<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="upload/favicon2.ico" type="image/x-icon">
    <link rel="bookmark" href="upload/favicon2.ico" type="image/x-icon"/>

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="vendor/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <!-- Fileinput -->
    <link href="vendor/bootstrap-fileinput/fileinput.css" rel="stylesheet" >

    <style>
        .form-right-tip .error{
            color: red;
        }
        .form-right-tip .success{
            color: green;
        }
    </style>
</head>

<body>



<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li  class="active">
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li  class="active"><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>修改用户信息</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>用户管理</a>
                    </li>
                    <li class="active">
                        <strong>修改用户信息</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">

                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            修改用户信息
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal" id="changePathFrom" enctype="multipart/form-data" >
                                                <div class="form-group">
                                                    <div class="col-md-1">头像</div>
                                                    <div class="col-md-8">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput" id="uploadImageDiv">
                                                            <div class="fileinput-new thumbnail">
                                                                <img id="path" src="upload/${sessionScope.logUser.path}" alt="" style="max-width: 100px; max-height: 100px;"/>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                                 style="max-width: 100px; max-height: 100px;"></div>
                                                            <div>
                        <span class="btn default btn-file">
                            <span class="fileinput-new btn btn-info">选择图片</span>
                            <span class="fileinput-exists btn btn-info">更改</span>
                            <input type="file" name="uploadImage" id="uploadImage"/>
                        </span>
                                                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">移除</a>
                                                                <span class="btn default btn-file">
                        <a class="btn btn-success fileinput-exists" id="changePath">更改头像</a>
                    </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>

                                            <form id="update-form" class="form-horizontal" action="user/updateByMy" method="post">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-1">学号</label>
                                                    <div class="col-sm-3">
                                                        <input id="id" name="id" value="${sessionScope.logUser.id}" type="hidden">
                                                        <input id="stuNo" name="stuNo" value="${sessionScope.logUser.stuNo}" class="form-control" readonly>
                                                    </div>
                                                    <label class="control-label col-md-2">身份证号</label>
                                                    <div class="col-md-4">
                                                        <input id="idCardNo" name="idCardNo" value="${sessionScope.logUser.idCardNo}" class="form-control">
                                                    </div>
                                                    <div class="col-md-2"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-1">性别</label>
                                                    <div id="gender" class="col-md-2">
                                                        <div class="i-checks pull-left"><label><input type="radio" name="gender" id="man" value="男"> <i></i> 男 </label></div>
                                                        <div class="i-checks pull-right"><label><input type="radio" name="gender" id="woman" value="女"> <i></i> 女 </label></div>
                                                    </div>
                                                    <label class="control-label col-md-2 col-md-offset-1">出生日期</label>
                                                    <div class="col-md-4">
                                                        <input id="birthday" name="birthday" type="date" class="form-control">
                                                    </div>
                                                    <div class="col-md-2 form-right-tip"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-1">姓名</label>
                                                    <div class="col-md-2">
                                                        <input id="userName" name="userName" value="${sessionScope.logUser.userName}" class="form-control">
                                                    </div>
                                                    <div class="col-md-1 form-right-tip"></div>
                                                    <label class="control-label col-md-2">旧密码</label>
                                                    <div class="col-md-4">
                                                        <input id="oldPsd" type="hidden">
                                                        <input id="oldPassWord" name="oldPassWord" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-md-2 form-right-tip"></div>
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-1">密码</label>
                                                    <div class="col-md-3">
                                                        <input id="newPassWord" type="password" name="passWord" class="form-control">
                                                    </div>
                                                    <label class="control-label col-md-2">确认密码</label>
                                                    <div class="col-md-3">
                                                        <input id="passWord2" name="passWord2" type="password" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 form-right-tip"></div>
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-1">手机</label>
                                                    <div class="col-md-4">
                                                        <input id="mobile" name="mobile" value="${sessionScope.logUser.mobile}" class="form-control">
                                                    </div>
                                                    <label class="control-label col-md-2">家庭电话</label>
                                                    <div class="col-md-4">
                                                        <input id="homeTel" name="homeTel" value="${sessionScope.logUser.homeTel}" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-1">QQ</label>
                                                    <div class="col-md-4">
                                                        <input id="qq" name="qq" type="text" value="${sessionScope.logUser.qq}" class="form-control">
                                                    </div>
                                                    <label class="control-label col-md-2">Email</label>
                                                    <div class="col-md-4">
                                                        <input id="email" name="email" value="${sessionScope.logUser.email}" class="form-control">
                                                    </div>
                                                    <div class="col-md-1 form-right-tip"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-1">籍贯</label>
                                                    <div class="col-md-4">
                                                        <input id="nationPlace" name="nationPlace" value="${sessionScope.logUser.nationPlace}" class="form-control">
                                                    </div>
                                                    <label class="control-label col-md-2">家庭住址</label>
                                                    <div class="col-md-4">
                                                        <input id="homeAddr" name="homeAddr" value="${sessionScope.logUser.homeAddr}" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-1">专业</label>
                                                    <div class="col-md-4">
                                                        <input id="marjor" name="marjor" value="${sessionScope.logUser.marjor}" class="form-control">
                                                    </div>
                                                    <label class="control-label col-md-2">毕业院校</label>
                                                    <div class="col-md-4">
                                                        <input id="graduateSchool" name="graduateSchool" value="${sessionScope.logUser.graduateSchool}" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-1">学历</label>
                                                    <div class="col-md-4">
                                                        <select id="eduBackground" name="eduBackground" class="form-control">
                                                            <option value="高中">高中</option>
                                                            <option value="专科">专科</option>
                                                            <option value="本科">本科</option>
                                                        </select>
                                                    </div>
                                                    <label class="control-label col-md-2">学校地址</label>
                                                    <div class="col-md-4">
                                                        <input id="schAddr" name="schAddr" value="${sessionScope.logUser.schAddr}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-2">
                                                        <button type="button" id="save_update" class="btn btn-info col-md-12">更改</button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="reset" class="btn btn-danger col-md-12">重置</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>
<script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="vendor/js/inspinia.js"></script>
<script src="vendor/js/plugins/pace/pace.min.js"></script>

<!-- Toastr -->
<script src="vendor/js/plugins/toastr/toastr.min.js"></script>

<!-- Sweet alert -->
<script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- validate -->
<script src="vendor/jquery-validation-1.16.0/jquery.validate.min.js"></script>

<!-- iCheck -->
<script src="vendor/js/plugins/iCheck/icheck.min.js"></script>

<!-- Fileinput -->
<script src="vendor/bootstrap-fileinput/fileinput.js"></script>
<%--
<!-- Flot -->
<script src="vendor/js/plugins/flot/jquery.flot.js"></script>
<script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
<script src="vendor/js/demo/peity-demo.js"></script>

<!-- jQuery UI -->
<script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="vendor/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
--%>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

    function FormatDate (strTime) {
        var now = new Date(strTime);
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var date = now.getFullYear()+"-"+(month)+"-"+(day) ;
        return date;
    };

    $(function () {
        $('option[value="${sessionScope.logUser.eduBackground}"]').attr('selected','selected');
        $('#gender input[value="${sessionScope.logUser.gender}"]').parent().attr('class','iradio_square-green checked');
        $('#oldPsd').attr('value','${sessionScope.logUser.passWord}');
        $('#birthday').val(FormatDate('${sessionScope.logUser.birthday}'));

        $('#update-form').validate({
            errorElement: 'span',
            errorPlacement: function(error, input){

                input.parent().next().append(error);

            },
            success: function(error){
                error.html('<i class="fa fa-check success"></i>');
            },
            rules: {
                oldPassWord:{
                    equalTo: "#oldPsd"
                },
                passWord2:{
                    minlength:6,
                    equalTo: "#newPassWord"
                },
                email:{
                    email: true
                }
            },
            messages: {
                oldPassWord: {
                    equalTo: '<i class="fa fa-remove"></i> 旧密码不正确'
                },
                passWord2: {
                    minlength:'<i class="fa fa-remove"></i> 密码长度不能小于6',
                    equalTo: '<i class="fa fa-remove"></i> 两次密码不一样'
                },
                email: {
                    email: '<i class="fa fa-remove"></i>邮箱格式不对'
                }
            }
        });


        /*头像添加*/
        $("#uploadImage").on("fileuploadadd", function(e, data) {
            // 当文件添加上去时候调用
            $("#titleImageError").html("");
            data.submit()
        });

        $('#save_update').on('click',function () {
            if($('#update-form').valid()){
                $('#update-form').submit();
            }
        });

        $('#changePath').on('click',function () {
            var formData = new FormData();
            var id = $('#id').attr("value");
            var file = document.getElementById("uploadImage").files[0];
            formData.append('id', id);
            formData.append('file', file);
            $.ajax({
                url: "file/changePathMy",
                type: "post",
                data: formData,
                contentType: false,
                processData: false,
                mimeType: "multipart/form-data",
                success: function (resp) {
                    $('#uploadImageDiv').attr('class','fileinput fileinput-new');
                    if(JSON.parse(resp).msg){
                        $('#path').attr('src','upload/'+ JSON.parse(resp).path +'');
                        $('#nav-path').attr('src','upload/'+ JSON.parse(resp).path +'');
                        swal({
                            title: "成功",
                            text: "修改头像成功",
                            type: "success"
                        });
                    }else {
                        swal({
                            title: "失败",
                            text: "修改头像失败",
                            type: "error"
                        });

                    }
                }
            });
        });
    });
</script>
</body>
</html>
