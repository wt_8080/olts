<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: WT
  Date: 2018/10/8
  Time: 20:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="${pageContext.request.contextPath}/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>编程题新增</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->

    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>
    <!-- Sweet Alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Sweet alert -->
    <script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $.ajax({
                url: "select/load_course",  // 请求URL
                type: "GET",
                data: {}, //请求的参数
                dataType: "json", // 响应的内容类型：text, html, json, script, xml, jsonp
                success: function (resp) {
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.courseName, item.id);
                        $('#kc').append(opt);
                    });
                }
            });
            //选择课程时加载
            $('#kc').change(function () {

                $('#zsd').empty();
                $('#zsd').append('<option value="">--不限--</option>');

                $.get("select/load_TechCategory", {"id": $('#kc').val()}, function (resp) {
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.techCtgr, item.id);
                        $('#zsd').append(opt);
                    });
                }, 'json');
            });

        });
    </script>
    <script>
        function codeAdd() {
            console.log(document.getElementById("stem").innerHTML);
            var code = '<pre class="prettyprint linenums lang-*">\n代码位置\n</pre>';
            /* document.getElementById("stem").removeAttribute("textarea");
             console.log(document.getElementById("stem").innerHTML);*/
            document.getElementById("stem").innerHTML=code;
            document.getElementById("answer").innerHTML=code;
        }
    </script>
    <script>
        $(document).ready(function () {
            if(${modelAdd eq 1}){
                    swal({
                        title: "新增编程题成功!",
                        text: "点击这返回！",
                        type: "success"
                    });
            }
        });

    </script>

</head>
<body>


<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li class="active">
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li class="active"><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>编程题新增</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>试题新增</a>
                    </li>
                    <li class="active">
                        <strong>编程题新增</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <%--                   <div class="ibox-title">
                                               <h5>Bootstrap Panels <small>Custom background colors.</small></h5>
                                           </div>--%>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            编程题新增
                                        </div>
                                        <div class="panel-body">
                                            <form role="form" action="add/insertAsk" method="post">
                                                <div class="form-group">
                                                    <label for="stem">*编程题题干：</label>
                                                    <textarea class="form-control" rows="5" id="stem" name="question"  required=""></textarea>
                                                    <button type="button" class="btn btn-light" onclick="codeAdd()" id="codeButt1">代码格式</button>
                                                </div>

                                                <div class="form-group">
                                                    <label for="answer">*参考答案：</label>
                                                    <textarea class="form-control" rows="5" id="answer" name="stdAnswer"  required=""></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="kc">*课程：</label>
                                                    <div class="col-sm-4">
                                                        <select id="kc" name="" class="form-control" required="">

                                                            <option>--不限--</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-sm-2 control-label" for="zsd">*知识点：</label>
                                                    <div class="col-sm-4">
                                                        <select id="zsd" name="techCateId" class="form-control" required="">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="descrpt">考题说明信息：</label>
                                                    <textarea class="form-control" rows="3" id="descrpt" name="descrpt"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <%--题目类型--%>
                                                    <input type="text" class="hidden" name="questionType" value="6" >
                                                </div>
                                                <br>
                                                <br>

                                                <div class="form-group" >
                                                    <div class="inline col-lg-offset-3">
                                                        <button type="submit" class="btn btn-primary btn-lg demo2">提交</button>
                                                    </div>
                                                    <div class="inline col-lg-offset-3">
                                                        <button type="reset" class="btn btn-primary btn-lg">重置</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>
</body>
</html>
