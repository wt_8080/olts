<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    session.setAttribute("rootPath",request.getContextPath());
%>
<html>

<head>
    <base href="${pageContext.request.contextPath}/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->

    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>


    <script type="text/javascript">
        $(function () {
            $.ajax({
                url: "Paper/selectExamNo",  // 请求URL
                type: "GET",
                data: {}, //请求的参数
                dataType: "json", // 响应的内容类型：text, html, json, script, xml, jsonp
                success: function (resp) {
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.examNo, item.examNo);
                        $('#examNo').append(opt);
                    });
                }
            });
        });
    </script>
<script>
    function selectScore(id) {
        console.log("测试" + id);
        var exam_no = $('#examNo').val();
        console.log(exam_no);
        $('#tobodyOne').load("Paper/select_score?id=" + id + "&examNo=" + exam_no);
    };
    function selectAllScore() {

        var exam_no=$('#examNo').val();
        console.log(exam_no);
        $('#tobodyOne').load("Paper/select_All_Score?examNo="+exam_no);
    };
    /*function selectAll(a) {
        console.log(a.checked);
        if(a.checked){
            $('input[name="checkSon"]').prop('checked',true);
        }else {
            $('input[name="checkSon"]').prop('checked',false);
        }
        console.log($('#checkList').prop('checked'));
        console.log($('input[name="checkSon"]').prop('checked'));
    };*/
    function deleteOne(id) {
        console.log("my"+id);
        var exam_no=$('#examNo').val();
        /*$('#tobodyOne').load("Paper/delete_One?id="+id+"&examNo="+exam_no);*/
        $.get("Paper/delete_One",{"id":id,"examNo":exam_no},function (resp) {
            console.log(resp);
            if(resp){
                alert("删除成功");
                $('#tobodyOne').load("Paper/select_All_Score?examNo="+exam_no)
            }else {
                alert("删除失败");
            }
        },"json")
    }
</script>

</head>

<body>



<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li class="active">
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li  class="active"><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>查看成绩</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>考卷管理</a>
                    </li>
                    <li class="active">
                        <strong>查看成绩</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <%--                   <div class="ibox-title">
                                               <h5>Bootstrap Panels <small>Custom background colors.</small></h5>
                                           </div>--%>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            查看成绩
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-inline pull-left" action="javascript:void(0)">
                                                <div class="input-group">
                                                    <select id="examNo" name="examNo"  class="form-control" placeholder="请输入试卷编号" value="">
                                                        <option value="">--不限--</option>
                                                    </select>
                                                    <span class="input-group-btn">
                                    <button class="btn btn-info"  onclick="selectAllScore()" style="background-color: #aac6e3">
                                        <i class="fa fa-search"></i>
                                        查询成绩
                                    </button>
                                </span>
                                                </div>
                                            </form>
                                            <%--<button onclick="selectScore(${sessionScope.logUser.id})">查询</button>--%>
                                            <div class="pull-right">
                                                <form action="javascript:void(0)">
                                                    <button class="btn btn-info" onclick="selectAllScore()">
                                                        <i class="fa fa-refresh"></i>
                                                        刷新
                                                    </button>
                                                </form>
                                            </div>
                                            <table class="table table-bordered table-hover" style="text-align: center">
                                                <thead>
                                                <tr class="tr_one">
                                                    <td>编号</td>
                                                    <td>序号</td>
                                                    <td>姓名</td>
                                                    <td>成绩</td>
                                                    <td>性别</td>
                                                    <td>在读/毕业院校</td>
                                                    <td>专业</td>
                                                    <td>最高学历</td>
                                                    <%--<td>
                                                        <input name="checkList" type="checkbox" id="checkList" onclick="selectAll(this)">全选
                                                    </td>--%>
                                                    <td>操作</td>
                                                </tr>
                                                </thead>
                                                <tbody id="tobodyOne">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer">
                                            <div>
                                                <c:if test="page != null">
                                                    <jsp:include page="../../paging.jsp">
                                                        <jsp:param name="contextPath" value="Paper/search"/>
                                                        <jsp:param name="page" value="page"/>
                                                    </jsp:include>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>

<%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>

</body>
</html>
