<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--
        <c:forEach items="${score}" var="score" step="1" varStatus="scoreStatus">
            <tr>
                    &lt;%&ndash;<id property="id" column="ID"></id>
                    <result property="userName" column="USER_NAME"></result>
                    <result property="oltsScore.score" column="SCORE"></result>
                    <result property="oltsScore.fspScore" column="FSP_SCORE"></result>
                    <result property="gender" column="GENDER"></result>
                    <result property="graduateSchool" column="GRADUATE_SCHOOL"></result>
                    <result property="marjor" column="MARJOR"></result>
                    <result property="eduBackground" column="EDU_BACKGROUND"> + score.oltsScore.fspScor</result>&ndash;%&gt;
                <td>${scoreStatus.index}</td>
                <td>${score.id}</td>
                <td>${score.userName}</td>
                <td>${score.oltsScore.score + score.oltsScore.fspScore}</td>
                <td>${score.gender}</td>
                <td>${score.graduateSchool}</td>
                <td>${score.marjor}</td>
                <td>${score.eduBackground}</td>
&lt;%&ndash;
                <td><input type="checkbox" name="checkSon" id="check+'${scoreStatus.index}'"></td>
&ndash;%&gt;
                <td>
                    <button class="btn btn-danger">
                        <i class="fa fa-remove"></i> 删除
                    </button>
                </td>
            </tr>
        </c:forEach>--%>

<c:forEach items="${AllScore}" var="Allscore" step="1" varStatus="scoreStatus">
    <tr>
            <%--<id property="id" column="ID"></id>
            <result property="userName" column="USER_NAME"></result>
            <result property="oltsScore.score" column="SCORE"></result>
            <result property="oltsScore.fspScore" column="FSP_SCORE"></result>
            <result property="gender" column="GENDER"></result>
            <result property="graduateSchool" column="GRADUATE_SCHOOL"></result>
            <result property="marjor" column="MARJOR"></result>
            <result property="eduBackground" column="EDU_BACKGROUND"> + score.oltsScore.fspScor</result>--%>
        <td>${scoreStatus.index}</td>
        <td>${Allscore.id}</td>
        <td>${Allscore.userName}</td>
        <td>${Allscore.oltsScore.score + Allscore.oltsScore.fspScore}</td>
        <td>${Allscore.gender}</td>
        <td>${Allscore.graduateSchool}</td>
        <td>${Allscore.marjor}</td>
        <td>${Allscore.eduBackground}</td>
<%--

        <td><input type="checkbox" name="checkSon" id="check+'${scoreStatus.index}'"></td>
--%>
        <td>
            <button class="btn btn-danger" onclick="deleteOne(${Allscore.id})">
                <i class="fa fa-remove"></i> 删除
            </button>
        </td>
    </tr>
</c:forEach>

