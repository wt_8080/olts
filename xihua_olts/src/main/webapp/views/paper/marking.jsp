<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    session.setAttribute("rootPath",request.getContextPath());
%>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style>
        .title{
            color: #FE233E;
        }
        .subject{
            color: #2E38F1;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li class="active">
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li class="active"><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>主观题阅卷</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>考卷管理</a>
                    </li>
                    <li class="active">
                        <strong>主观题阅卷</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <%--                   <div class="ibox-title">
                                               <h5>Bootstrap Panels <small>Custom background colors.</small></h5>
                                           </div>--%>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            主观题阅卷
                                        </div>
                                        <div class="panel-body">
                                            <h3 style="text-align: center;font-weight: bolder">改卷<font color="#FE233E">（编号：${requestScope.oltsUsers[0].fspAnswer[0].examNo}）</font></h3>
                                            <div id="marking-paper">
                                                <!-- 每个学生 -->
                                                <c:forEach var="oltsUser" items="${requestScope.oltsUsers}" varStatus="sta_otls_user">
                                                    <form id="form${oltsUser.id}">
                                                        <a href="javascript:void (0);" onclick="changeTab(${oltsUser.id})">
                                                            <h3 class="nav-label" style="font-size: larger;font-weight: bolder;background-color: #FFE4C4;">
                                                                <i class="title">${sta_otls_user.index+1}. ${oltsUser.userName} 客观题得分：${oltsUser.oltsScore.score}</i>
                                                            </h3>
                                                        </a>
                                                        <ul id="ul${oltsUser.id}" class="nav nav-second-level">
                                                            <li id="student${sta_otls_user.index+1}">
                                                                <!-- 填空题 -->
                                                                <c:set var="i" value="1" scope="page"/>
                                                                <h3 style="margin-top: 20px"><i>（一）填空题，每题2分</i></h3>
                                                                <c:forEach var="fillInBlankAnwser" items="${oltsUser.fspAnswer}" varStatus="sta_fill_in_blank">
                                                                    <c:if test="${fillInBlankAnwser.fspQuestions.questionType eq 4}">
                                                                        <div style="margin-top: 20px">
                                                                            <p style="margin-left: 28px;font-weight: bolder" class="subject">${i}. ${fillInBlankAnwser.fspQuestions.question}</p>
                                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                                            <p style="background-color: #FEFBEC;border: 1px solid #EEEAD7;margin-left: 40px">${fillInBlankAnwser.answer}</p>
                                                                            <div style="margin-left: 40px">
                                                                                得分：<input type="number" name="score${fillInBlankAnwser.id}" class="score${oltsUser.id}" min="0" max="2" required style="width: 60px;height: 20px">分
                                                                            </div>
                                                                        </div>
                                                                    </c:if>
                                                                </c:forEach>

                                                                <!-- 简答题 -->
                                                                <c:set var="i" value="1" scope="page"/>
                                                                <h3 style="margin-top: 20px"><i>（二）简答题，每题5分</i></h3>
                                                                <c:forEach var="simpleAnwser" items="${oltsUser.fspAnswer}" varStatus="sta_simple">
                                                                    <c:if test="${simpleAnwser.fspQuestions.questionType eq 5}">
                                                                        <div style="margin-top: 20px">
                                                                            <p style="margin-left: 28px;font-weight: bolder" class="subject">${i}. ${simpleAnwser.fspQuestions.question}</p>
                                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                                            <p style="background-color: #FEFBEC;border: 1px solid #EEEAD7;margin-left: 40px">${simpleAnwser.answer}</p>
                                                                            <div style="margin-left: 40px">
                                                                                得分：<input type="number" name="score${simpleAnwser.id}" class="score${oltsUser.id}" min="0" max="5" required style="width: 60px;height: 20px">分
                                                                            </div>
                                                                        </div>
                                                                    </c:if>
                                                                </c:forEach>

                                                                <!-- 编程题 -->
                                                                <c:set var="i" value="1" scope="page"/>
                                                                <h3 style="margin-top: 20px"><i>（三）编程题，每题10分</i></h3>
                                                                <c:forEach var="programmingAnwser" items="${oltsUser.fspAnswer}" varStatus="sta_programming">
                                                                    <c:if test="${programmingAnwser.fspQuestions.questionType eq 6}">
                                                                        <div style="margin-top: 20px">
                                                                            <p style="margin-left: 28px;font-weight: bolder" class="subject">${i}. ${programmingAnwser.fspQuestions.question}</p>
                                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                                            <p style="background-color: #FEFBEC;border: 1px solid #EEEAD7;margin-left: 40px">${programmingAnwser.answer}</p>
                                                                            <div style="margin-left: 40px">
                                                                                得分：<input type="number" name="score${programmingAnwser.id}" class="score${oltsUser.id}" min="0" max="10" required style="width: 60px;height: 20px">分
                                                                            </div>
                                                                        </div>
                                                                    </c:if>
                                                                </c:forEach>

                                                                <!-- 完成 -->
                                                                <div style="margin-left: 40px">
                                                                    <br>
                                                                    <input id="complete${oltsUser.id}" type="button" value="完成" class="btn btn-info btn-sm" onclick="complete(${oltsUser.id})">
                                                                    &nbsp;
                                                                    <font class="title">${oltsUser.userName} 总分：
                                                                        <span id="sumScore${oltsUser.id}">
                                                                            <c:if test="${oltsUser.oltsScore.fspScore!=null}">
                                                                                ${oltsUser.oltsScore.fspScore}
                                                                            </c:if>
                                                                            <c:if test="${oltsUser.oltsScore.fspScore==null}">
                                                                                0.0
                                                                            </c:if>
                                                                        </span>
                                                                    </font>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </form>
                                                </c:forEach>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>
<script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->

<script src="vendor/js/inspinia.js"></script>
<script src="vendor/js/plugins/pace/pace.min.js"></script>

<!-- Toastr -->
<script src="vendor/js/plugins/toastr/toastr.min.js"></script>

<!-- Sweet alert -->
<script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>

<%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>



<script>
    $(document).ready(function() {
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');

        }, 1300);
    });

    function changeTab(userId) {
        var ul_obj = $('#ul'+userId);
        if (ul_obj.hasClass("collapse")){
            ul_obj.removeClass("collapse");
        }else {
            ul_obj.addClass("collapse");
        }
    }

    function complete(userId) {
        var form = document.getElementById('form'+userId);
        if (form.checkValidity()) {
            //主观题分数
            var fspScore=0;
            $('.score' + userId).each(function () {
                var score = $(this).val();
                if (score != null && score.length != 0){
                    fspScore += parseInt(score);
                }
            })
            //发送到服务器的数据
            var data = {"userId":userId,"fspScore":fspScore};
            //发送post请求到服务器
            $.post('marking/complete',data, function (json) {
                    if (json.actionFlag){
                        $('#sumScore'+userId).text(fspScore);
                        swal("Success!","保存成功！","success");
                    }else {
                        swal("Error!","保存失败！","error");
                    }
                }, 'json'
            );
        }else {
            swal("Error!","输入的分数错误！","error");
        }
    }
</script>
</body>
</html>
