<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wxk
  Date: 2018/10/8
  Time: 9:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--    <base href="${sessionScope.rootPath}/"/>--%>
    <base href="${pageContext.request.contextPath}/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Sweet alert -->
    <script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Sweet Alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


    <!-- Custom and plugin javascript -->

    <script src="vendor/js/inspinia.js"></script>
    <script src="vendor/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="vendor/js/plugins/toastr/toastr.min.js"></script>


    <title>试题查询</title>
    <script type="text/javascript">
        $(function () {
            $.ajax({
                url: "select/load_course",  // 请求URL
                type: "GET",
                data: {}, //请求的参数
                dataType: "json", // 响应的内容类型：text, html, json, script, xml, jsonp
                success: function (resp) {
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.courseName, item.id);
                        $('#courseid').append(opt);
                    });
                }
            });
            //选择课程时加载
            $('#courseid').change(function () {
                console.log("课程编号：" + $('#courseid').val());
                $('#techId').empty();
                $('#techId').append('<option value="">不限--</option>');

                $.get("select/load_TechCategory", {"id": $('#courseid').val()}, function (resp) {
                    console.dirxml(resp);
                    $.each(resp, function (i, item) { // foreach item, item={id:101,name:"海淀"}
                        var opt = new Option(item.techCtgr, item.id);
                        $('#techId').append(opt);
                    });
                }, 'json');
            });

        });


    </script>

</head>






<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li  class="active">
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li  class="active"><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>判断题添加</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>考卷维护</a>
                    </li>
                    <li class="active">
                        <strong>判断题添加</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">

                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            判断题添加
                                            <br>
                                            <ul class="nav nav-tabs">
                                                <li >
                                                    <a href="select/searchAllsmd_radio?questionType=1">查询单项选择题</a>
                                                </li>
                                                <li  >
                                                    <a href="select/searChSmd_MutiCheck?questionType=2">查询多项选择题</a>
                                                </li>
                                                <li class="active">
                                                    <a href="select/searChSmd_Judge?questionType=3">查询判断题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/fillselect.jsp">查询填空题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/askselect.jsp">查询简答题</a>
                                                </li>
                                                <li>
                                                    <a href="views/select/programselect.jsp">查询编程题</a>
                                                </li>
                                                <li>
                                                    <button class="btn btn-info" id="submitExam" onclick="submitOver()">
                                                        <i class="fa fa-cog"></i>
                                                        完成出卷
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">

                                            <c:forEach items="${mypage}" var="smdQuestion" varStatus="stat">
                                                <div class="panel-group" id="panel-${stat.index+847716}">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <a class="panel-title collapsed" data-toggle="collapse"
                                                               data-parent="#panel-${stat.index+847716}"
                                                               href="#panel-element-${stat.index+932996}">
                                                                <h4>${stat.index+1}:${smdQuestion.question}</h4>
                                                            </a>
                                                        </div>
                                                        <div id="panel-element-${stat.index+932996}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <div class="form-group">

                                                                    <font color="red">正确答案是：${smdQuestion.correct}</font>
                                                                    <br><br>
                                                                    <input type="hidden" name="" id="qwer" value="${sessionScope.examNo}">
                                                                    <button class="btn btn-info" onclick="addSingle(${smdQuestion.id})">添加试题</button>

                                                                    <br><br>
                                                                    <div class="row clearfix">
                                                                        <div class="col-md-6 column" align="center">



                                                                            <div class="modal inmodal" id="myModal${stat.index}" tabindex="-1" role="dialog"
                                                                                 aria-hidden="true">
                                                                                <div class="modal-dialog">
                                                                                    <div class="modal-content animated bounceInRight">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal">
                                                                                                <span aria-hidden="true">&times;</span><span
                                                                                                    class="sr-only">Close</span></button>

                                                                                            <h5 class="modal-title">修改判断题</h5>

                                                                                        </div>
                                                                                        <form action="select/updateJudge">
                                                                                            <div class="modal-body">

                                                                                                <input type="hidden" name="id" value="${smdQuestion.id}"/>

                                                                                                <div class="form-group"><label>修改前的题干：<br>
                                                                                                        ${stat.index+1}:${smdQuestion.question}</label>

                                                                                                    <input type="text" value="${smdQuestion.question}"
                                                                                                           class="form-control" name="question">

                                                                                                </div>

                                                                                                <div class="form-group"><label>修改前的答案：<br>
                                                                                                        ${stat.index+1}:${smdQuestion.correct}</label>

                                                                                                    <input type="text" value="${smdQuestion.correct}"
                                                                                                           class="form-control" name="correct">

                                                                                                </div>


                                                                                            </div>

                                                                                            <div class="modal-footer">
                                                                                                <button type="button" class="btn btn-white"
                                                                                                        data-dismiss="modal">关闭
                                                                                                </button>
                                                                                                <button type="submit" class="btn btn-primary">保存</button>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>

                                                                                </div>
                                                                            </div>




                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
<%--                                            <jsp:include page="../../paging.jsp">
                                                <jsp:param name="contextPath" value="select/searchsmd_judge"/>
                                                <jsp:param name="page" value="mypage"/>
                                            </jsp:include>--%>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>
<script>
    function submitOver() {
        var examNo = $('#qwer').val();
        console.log(examNo);
        $.get("Paper/setExamFlag", {"examNo": examNo, "validFlag": 1}, function (result) {
            console.log(result.respl);
            if (result.respl) {
                swal("Success", "试卷维护成功", "success");
                window.location.href = "views/paper/examination_Safeguard.jsp";
            } else {
                swal("Failed", "试卷维护失败", "error");
            }
        }, "json")
    };
    function addSingle(id) {
        var examNo=$('#qwer').val();
        console.log(examNo);
        $.get("Paper/JudgeQuestionIsExist",{"examNo":examNo,"trueFalseId":id},function (result) {
            console.log(result);
            if(result.resp){
                swal("","此题在试卷已存在","success");
            }else{
                $.get("Paper/addJudgeQuestion",{"examNo":examNo,"trueFalseId":id},function (resp) {
                    console.log(resp);
                    if(resp.respl){
                        swal("Success","添加成功","success");
                    }else{
                        swal("Failed","添加失败","error");
                    }
                },"json")
            }
        },"json")
    }


</script>
</body>


</html>
