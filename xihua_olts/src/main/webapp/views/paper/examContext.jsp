<%--
  Created by IntelliJ IDEA.
  User: czx
  Date: 2018/10/12
  Time: 12:52
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="test-paper">
    <c:if test="${requestScope.exam.singleAnwser!=null}">
    <div id="single-anwser" style="margin-top: 20px">
        <h3 class="title">（
            <c:choose>
                <c:when test="${i+1 eq 1}">
                    一
                </c:when>
                <c:when test="${i+1 eq 2}">
                    二
                </c:when>
                <c:when test="${i+1 eq 3}">
                    三
                </c:when>
                <c:when test="${i+1 eq 4}">
                    四
                </c:when>
                <c:when test="${i+1 eq 5}">
                    五
                </c:when>
                <c:when test="${i+1 eq 6}">
                    六
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
            ）单选题，每题2分，共${requestScope.exam.singleAnwser.size()}题，共${2*requestScope.exam.singleAnwser.size()}分。
        </h3>
        <c:set var="i" value="${i+1}" scope="page"/>
        <!-- 单选题的每一题 -->
        <c:forEach var="singleAnwser" items="${singleSelection}" varStatus="sta_single">
            <div id="single${sta_single.index+1}" style="margin-left: 40px;margin-top: 20px">
                <table>
                    <tr>
                        <td width="100%" style="font-weight: bolder">${sta_single.index+1}. <font class="subject">${singleAnwser.question}</font></td>
                    </tr>
                    <c:if test="${singleAnwser.options.optionA!=null}">
                        <tr>
                            <td width="100%">&nbsp;A. ${singleAnwser.options.optionA}</td>
                        </tr>
                    </c:if>
                    <c:if test="${singleAnwser.options.optionB!=null}">
                        <tr>
                            <td width="100%">&nbsp;B. ${singleAnwser.options.optionB}</td>
                        </tr>
                    </c:if>
                    <c:if test="${singleAnwser.options.optionC!=null}">
                        <tr>
                            <td width="100%">&nbsp;C. ${singleAnwser.options.optionC}</td>
                        </tr>
                    </c:if>
                    <c:if test="${singleAnwser.options.optionD!=null}">
                        <tr>
                            <td width="100%">&nbsp;D. ${singleAnwser.options.optionD}</td>
                        </tr>
                    </c:if>
                    <c:if test="${singleAnwser.options.optionE!=null}">
                        <tr>
                            <td width="100%">&nbsp;E. ${singleAnwser.options.optionE}</td>
                        </tr>
                    </c:if>
                    <tr>
                        <td width="100%">
                            <br>
                            <font style="font-weight: bolder">请选择答案：</font>
                            <c:if test="${singleAnwser.options.optionA!=null}">
                                <input type="radio" name="single${singleAnwser.id}" value="A" style="margin-left: 10px">A
                            </c:if>
                            <c:if test="${singleAnwser.options.optionB!=null}">
                                <input type="radio" name="single${singleAnwser.id}" value="B" style="margin-left: 10px">B
                            </c:if>
                            <c:if test="${singleAnwser.options.optionC!=null}">
                                <input type="radio" name="single${singleAnwser.id}" value="C" style="margin-left: 10px">C
                            </c:if>
                            <c:if test="${singleAnwser.options.optionD!=null}">
                                <input type="radio" name="single${singleAnwser.id}" value="D" style="margin-left: 10px">D
                            </c:if>
                            <c:if test="${singleAnwser.options.optionE!=null}">
                                <input type="radio" name="single${singleAnwser.id}" value="E" style="margin-left: 10px">E
                            </c:if>
                            <button class="btn btn-warning btn-md demo3" onclick="deletesingle(${singleAnwser.id})">删除</button>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:if>

<!-- 多选题 -->
    <c:if test="${requestScope.exam.multipleAnwser!=null}">
    <div id="multiple-anwser" style="margin-top: 30px">
        <h3 class="title">（
            <c:choose>
                <c:when test="${i+1 eq 1}">
                    一
                </c:when>
                <c:when test="${i+1 eq 2}">
                    二
                </c:when>
                <c:when test="${i+1 eq 3}">
                    三
                </c:when>
                <c:when test="${i+1 eq 4}">
                    四
                </c:when>
                <c:when test="${i+1 eq 5}">
                    五
                </c:when>
                <c:when test="${i+1 eq 6}">
                    六
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
            ）多选题，每题4分，共${requestScope.exam.multipleAnwser.size()}题，共${4*requestScope.exam.multipleAnwser.size()}分。
        </h3>
        <c:set var="i" value="${i+1}" scope="page"/>
        <!-- 多选题的每一题 -->
        <c:forEach var="multipleAnwser" items="${requestScope.exam.multipleAnwser}" varStatus="sta_multiple">
            <div id="multiple${sta_multiple.index+1}" style="margin-left: 40px;margin-top: 20px">
                <table>
                    <tr>
                        <td width="100%" style="font-weight: bolder">${sta_multiple.index+1}. <font class="subject">${multipleAnwser.question}</font></td>
                    </tr>
                    <c:if test="${multipleAnwser.options.optionA!=null}">
                        <tr>
                            <td width="100%">&nbsp;A. ${multipleAnwser.options.optionA}</td>
                        </tr>
                    </c:if>
                    <c:if test="${multipleAnwser.options.optionB!=null}">
                        <tr>
                            <td width="100%">&nbsp;B. ${multipleAnwser.options.optionB}</td>
                        </tr>
                    </c:if>
                    <c:if test="${multipleAnwser.options.optionC!=null}">
                        <tr>
                            <td width="100%">&nbsp;C. ${multipleAnwser.options.optionC}</td>
                        </tr>
                    </c:if>
                    <c:if test="${multipleAnwser.options.optionD!=null}">
                        <tr>
                            <td width="100%">&nbsp;D. ${multipleAnwser.options.optionD}</td>
                        </tr>
                    </c:if>
                    <c:if test="${multipleAnwser.options.optionE!=null}">
                        <tr>
                            <td width="100%">&nbsp;E. ${multipleAnwser.options.optionE}</td>
                        </tr>
                    </c:if>
                    <tr>
                        <td width="100%">
                            <br>
                            <font style="font-weight: bolder">请选择答案：</font>
                            <c:if test="${multipleAnwser.options.optionA!=null}">
                                <input type="checkbox" name="multiple${multipleAnwser.id}" value="A" style="margin-left: 10px">A
                            </c:if>
                            <c:if test="${multipleAnwser.options.optionB!=null}">
                                <input type="checkbox" name="multiple${multipleAnwser.id}" value="B" style="margin-left: 10px">B
                            </c:if>
                            <c:if test="${multipleAnwser.options.optionC!=null}">
                                <input type="checkbox" name="multiple${multipleAnwser.id}" value="C" style="margin-left: 10px">C
                            </c:if>
                            <c:if test="${multipleAnwser.options.optionD!=null}">
                                <input type="checkbox" name="multiple${multipleAnwser.id}" value="D" style="margin-left: 10px">D
                            </c:if>
                            <c:if test="${multipleAnwser.options.optionE!=null}">
                                <input type="checkbox" name="multiple${multipleAnwser.id}" value="E" style="margin-left: 10px">E
                            </c:if>
                            <button class="btn btn-warning btn-md demo3" onclick="deleteMuti(${multipleAnwser.id})">删除</button>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:if>

<!-- 判断题 -->
    <c:if test="${requestScope.exam.determineAnwser!=null}">
    <div id="determine-anwser" style="margin-top: 30px">
        <h3 class="title">（
            <c:choose>
                <c:when test="${i+1 eq 1}">
                    一
                </c:when>
                <c:when test="${i+1 eq 2}">
                    二
                </c:when>
                <c:when test="${i+1 eq 3}">
                    三
                </c:when>
                <c:when test="${i+1 eq 4}">
                    四
                </c:when>
                <c:when test="${i+1 eq 5}">
                    五
                </c:when>
                <c:when test="${i+1 eq 6}">
                    六
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
            ）判断题，每题2分，共${requestScope.exam.determineAnwser.size()}题，共${2*requestScope.exam.determineAnwser.size()}分。
        </h3>
        <c:set var="i" value="${i+1}" scope="page"/>
        <!-- 判断题的每一题 -->
        <c:forEach var="determineAnwser" items="${datermine}" varStatus="sta_determine">
            <div id="determine${sta_determine.index+1}" style="margin-left: 40px;margin-top: 20px">
                <table>
                    <tr>
                        <td width="100%" style="font-weight: bolder">${sta_determine.index+1}. <font class="subject">${determineAnwser.question}</font></td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <br>
                            <font style="font-weight: bolder">请选择答案：</font>
                            <input type="radio" name="determine${determineAnwser.id}" value="true" style="margin-left: 10px">正确
                            <input type="radio" name="determine${determineAnwser.id}" value="false" style="margin-left: 40px">错误
                            <button class="btn btn-warning btn-md demo3" onclick="deleteJudge(${determineAnwser.id})">删除</button>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:if>

<!-- 填空题 -->
    <c:if test="${requestScope.exam.fillInBlankAnwser!=null}">
    <div id="fill_in_blank" style="margin-top: 30px">
        <h3 class="title">（
            <c:choose>
                <c:when test="${i+1 eq 1}">
                    一
                </c:when>
                <c:when test="${i+1 eq 2}">
                    二
                </c:when>
                <c:when test="${i+1 eq 3}">
                    三
                </c:when>
                <c:when test="${i+1 eq 4}">
                    四
                </c:when>
                <c:when test="${i+1 eq 5}">
                    五
                </c:when>
                <c:when test="${i+1 eq 6}">
                    六
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
            ）填空题，每题2分，共${requestScope.exam.fillInBlankAnwser.size()}题，共${2*requestScope.exam.fillInBlankAnwser.size()}分。
        </h3>
        <c:set var="i" value="${i+1}" scope="page"/>
        <!-- 填空题的每一题 -->
        <c:forEach var="fillInBlankAnwser" items="${fillInBlank}" varStatus="sta_fill_in_blank">
            <div id="fill_in_blank${sta_fill_in_blank.index+1}" style="margin-left: 40px;margin-top: 20px">
                <table>
                    <tr>
                        <td width="100%" style="font-weight: bolder">${sta_fill_in_blank.index+1}. <font class="subject">${fillInBlankAnwser.question}</font></td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <br>
                            <font style="font-weight: bolder">请输入答案：</font>
                            <input type="text" name="fill_in_blank${fillInBlankAnwser.id}" style="width: 300px;margin-left: 10px">
                            <button class="btn btn-warning btn-md demo3" onclick="deletesingle(${fillInBlankAnwser.id})">删除</button>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:if>

<!-- 简答题 -->
    <c:if test="${requestScope.exam.simpleAnwser!=null}">
    <div id="simple" style="margin-top: 30px">
        <h3 class="title">（
            <c:choose>
                <c:when test="${i+1 eq 1}">
                    一
                </c:when>
                <c:when test="${i+1 eq 2}">
                    二
                </c:when>
                <c:when test="${i+1 eq 3}">
                    三
                </c:when>
                <c:when test="${i+1 eq 4}">
                    四
                </c:when>
                <c:when test="${i+1 eq 5}">
                    五
                </c:when>
                <c:when test="${i+1 eq 6}">
                    六
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
            ）简答题，每题5分，共${requestScope.exam.simpleAnwser.size()}题，共${5*requestScope.exam.simpleAnwser.size()}分。
        </h3>
        <c:set var="i" value="${i+1}" scope="page"/>
        <!-- 简答题的每一题 -->
        <c:forEach var="simpleAnwser" items="${simple}" varStatus="sta_simple">
            <div id="simple${sta_simple.index+1}" style="margin-left: 40px;margin-top: 20px">
                <table>
                    <tr>
                        <td width="100%" style="font-weight: bolder">${sta_simple.index+1}. <font class="subject">${simpleAnwser.question}</font></td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <textarea name="simple${simpleAnwser.id}" cols="30" rows="5" style="width: 800px"></textarea>
                            <button class="btn btn-warning btn-md demo3" onclick="deletesingle(${simpleAnwser.id})">删除</button>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:if>

<!-- 编程题 -->
    <c:if test="${requestScope.exam.programmingAnwser!=null}">
    <div id="programming" style="margin-top: 30px">
        <h3 class="title">（
            <c:choose>
                <c:when test="${i+1 eq 1}">
                    一
                </c:when>
                <c:when test="${i+1 eq 2}">
                    二
                </c:when>
                <c:when test="${i+1 eq 3}">
                    三
                </c:when>
                <c:when test="${i+1 eq 4}">
                    四
                </c:when>
                <c:when test="${i+1 eq 5}">
                    五
                </c:when>
                <c:when test="${i+1 eq 6}">
                    六
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
            ）编程题，每题10分，共${requestScope.exam.programmingAnwser.size()}题，共${10*requestScope.exam.programmingAnwser.size()}分。
        </h3>
        <c:set var="i" value="${i+1}" scope="page"/>
        <!-- 编程题的每一题 -->
        <c:forEach var="programming" items="${programming}" varStatus="sta_programming">
            <div id="programming${sta_programming.index+1}" style="margin-left: 40px;margin-top: 20px">
                <table>
                    <tr>
                        <td width="100%" style="font-weight: bolder">${sta_programming.index+1}. <font class="subject">${programming.question}</font></td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <textarea name="programming${programming.id}" cols="30" rows="10" style="width: 800px"></textarea>

                            <button class="btn btn-warning btn-md demo3" onclick="deletesingle(${programming.id})">删除</button>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:if>

<!-- 交卷按钮 -->

</div>
