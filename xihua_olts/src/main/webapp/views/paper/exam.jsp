<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    session.setAttribute("rootPath",request.getContextPath());
%>
<html>

<head>
    <base href="${sessionScope.rootPath}/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>蓝桥考试管理系统</title>

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="vendor/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="vendor/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="vendor/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style>
        .title{
            color: #FE233E;
        }
        .subject{
            color: #2E38F1;
        }
    </style>

</head>

<body>


<div id="wrapper">
    <!-- 左侧导航栏 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <!-- 用户信息 -->
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="upload/${sessionScope.logUser.path}" width="50" height="50" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${sessionScope.logUser.userName}</strong>
                             </span> <span class="text-muted text-xs block">个人信息 <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs bg-info">
                            <li>用户名：${sessionScope.logUser.userName}</li>
                            <li>性别：${sessionScope.logUser.gender}</li>
                            <li>QQ:${sessionScope.logUser.qq}</li>
                            <li>专业：${sessionScope.logUser.marjor}</li>
                            <li>手机：${sessionScope.logUser.mobile}</li>
                            <li>身份证：${sessionScope.logUser.idCardNo}</li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <!-- 导航链接 -->
                <c:if test="${logUser.userType != null}">
                    <li class="active">
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题新增</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/add/radiochoice.jsp">单选题新增</a></li>
                            <li><a href="views/add/multiplechoice.jsp">多选题新增</a></li>
                            <li><a href="views/add/completion.jsp">填空题新增</a></li>
                            <li><a href="views/add/checking.jsp">判断题新增</a></li>
                            <li><a href="views/add/shortanswer.jsp">简答题新增</a></li>
                            <li><a href="views/add/programming.jsp">编程题新增</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">试题查询</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="views/select/selectnavigation.jsp">单选题查询</a></li>
                            <li><a href="views/select/muticheckselect.jsp">多选题查询</a></li>
                            <li><a href="views/select/fillselect.jsp">填空题查询</a></li>
                            <li><a href="views/select/judgeselect.jsp">判断题查询</a></li>
                            <li><a href="views/select/askselect.jsp">简答题查询</a></li>
                            <li><a href="views/select/programselect.jsp">编程题查询</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${logUser.userType != null}">
                    <li>
                        <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">分类管理</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="classify/selectCourses">课程名</a></li>
                            <li><a href="tech/listCourses">知识点</a></li>
                        </ul>
                    </li>
                </c:if>

                <li class="active">
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">考卷管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType == null}">
                            <li class="active"><a href="paper/selectPaper">考试</a></li>
                        </c:if>
                        <c:if test="${logUser.userType != null}">
                            <li><a href="views/paper/examination_Safeguard.jsp">考卷维护</a></li>
                            <li><a href="marking/selectAllAnwser">主观题阅卷</a></li>
                            <li><a href="views/paper/select_Score.jsp">查看成绩</a></li>
                        </c:if>
                    </ul>
                </li>

                <li>
                    <a href="index.jsp"><i class="fa fa-th-large"></i> <span class="nav-label">用户管理</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <c:if test="${logUser.userType !=null}">
                            <li class="active"><a href="views/user/user_import.jsp">批量导入用户</a></li>
                            <li><a href="user/search">批量操作用户</a></li>
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                        <c:if test="${logUser.userType == null}">
                            <li><a href="views/user/user_update.jsp">修改用户信息</a></li>
                        </c:if>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">${loginUser.userName}欢迎登陆</span>
                    </li>
                    <li>
                        <a href="user/exit">
                            <i class="fa fa-sign-out"></i> 注销
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>考试</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.jsp">首页</a>
                    </li>
                    <li>
                        <a>考卷管理</a>
                    </li>
                    <li class="active">
                        <strong>考试</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">

                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            单选题新增
                                        </div>
                                        <div class="panel-body">
                                            <c:set var="i" value="1" scope="page"/>
                                            <h3 style="text-align: center;font-weight: bolder">试卷<font class="title">（编号：${requestScope.exam.examNo}）</font></h3>
                                            <form id="test-paper-form" action="paper/handInPaper" method="post">
                                                <div id="test-paper">
                                                    <input type="hidden" name="examNo" value="${requestScope.exam.examNo}">
                                                    <!-- 单选题 -->
                                                    <c:if test="${requestScope.exam.singleAnwser!=null}">
                                                        <div id="single-anwser" style="margin-top: 20px">
                                                            <h3 class="title">（
                                                                <c:choose>
                                                                    <c:when test="${i eq 1}">
                                                                        一
                                                                    </c:when>
                                                                    <c:when test="${i eq 2}">
                                                                        二
                                                                    </c:when>
                                                                    <c:when test="${i eq 3}">
                                                                        三
                                                                    </c:when>
                                                                    <c:when test="${i eq 4}">
                                                                        四
                                                                    </c:when>
                                                                    <c:when test="${i eq 5}">
                                                                        五
                                                                    </c:when>
                                                                    <c:when test="${i eq 6}">
                                                                        六
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    </c:otherwise>
                                                                </c:choose>
                                                                ）单选题，每题2分，共${requestScope.exam.singleAnwser.size()}题，共${2*requestScope.exam.singleAnwser.size()}分。
                                                            </h3>
                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                            <!-- 单选题的每一题 -->
                                                            <c:forEach var="singleAnwser" items="${requestScope.exam.singleAnwser}" varStatus="sta_single">
                                                                <div id="single${sta_single.index+1}" style="margin-left: 40px;margin-top: 20px">
                                                                    <table>
                                                                        <tr>
                                                                            <td width="100%" style="font-weight: bolder">${sta_single.index+1}. <font class="subject">${singleAnwser.question}</font></td>
                                                                        </tr>
                                                                        <c:if test="${singleAnwser.options.optionA!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;A. ${singleAnwser.options.optionA}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${singleAnwser.options.optionB!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;B. ${singleAnwser.options.optionB}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${singleAnwser.options.optionC!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;C. ${singleAnwser.options.optionC}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${singleAnwser.options.optionD!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;D. ${singleAnwser.options.optionD}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${singleAnwser.options.optionE!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;E. ${singleAnwser.options.optionE}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <br>
                                                                                <font style="font-weight: bolder">请选择答案：</font>
                                                                                <c:if test="${singleAnwser.options.optionA!=null}">
                                                                                    <input type="radio" name="single${singleAnwser.id}" value="A" style="margin-left: 10px">A
                                                                                </c:if>
                                                                                <c:if test="${singleAnwser.options.optionB!=null}">
                                                                                    <input type="radio" name="single${singleAnwser.id}" value="B" style="margin-left: 10px">B
                                                                                </c:if>
                                                                                <c:if test="${singleAnwser.options.optionC!=null}">
                                                                                    <input type="radio" name="single${singleAnwser.id}" value="C" style="margin-left: 10px">C
                                                                                </c:if>
                                                                                <c:if test="${singleAnwser.options.optionD!=null}">
                                                                                    <input type="radio" name="single${singleAnwser.id}" value="D" style="margin-left: 10px">D
                                                                                </c:if>
                                                                                <c:if test="${singleAnwser.options.optionE!=null}">
                                                                                    <input type="radio" name="single${singleAnwser.id}" value="E" style="margin-left: 10px">E
                                                                                </c:if>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>

                                                    <!-- 多选题 -->
                                                    <c:if test="${requestScope.exam.multipleAnwser!=null}">
                                                        <div id="multiple-anwser" style="margin-top: 30px">
                                                            <h3 class="title">（
                                                                <c:choose>
                                                                    <c:when test="${i eq 1}">
                                                                        一
                                                                    </c:when>
                                                                    <c:when test="${i eq 2}">
                                                                        二
                                                                    </c:when>
                                                                    <c:when test="${i eq 3}">
                                                                        三
                                                                    </c:when>
                                                                    <c:when test="${i eq 4}">
                                                                        四
                                                                    </c:when>
                                                                    <c:when test="${i eq 5}">
                                                                        五
                                                                    </c:when>
                                                                    <c:when test="${i eq 6}">
                                                                        六
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    </c:otherwise>
                                                                </c:choose>
                                                                ）多选题，每题4分，共${requestScope.exam.multipleAnwser.size()}题，共${4*requestScope.exam.multipleAnwser.size()}分。
                                                            </h3>
                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                            <!-- 多选题的每一题 -->
                                                            <c:forEach var="multipleAnwser" items="${requestScope.exam.multipleAnwser}" varStatus="sta_multiple">
                                                                <div id="multiple${sta_multiple.index+1}" style="margin-left: 40px;margin-top: 20px">
                                                                    <table>
                                                                        <tr>
                                                                            <td width="100%" style="font-weight: bolder">${sta_multiple.index+1}. <font class="subject">${multipleAnwser.question}</font></td>
                                                                        </tr>
                                                                        <c:if test="${multipleAnwser.options.optionA!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;A. ${multipleAnwser.options.optionA}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${multipleAnwser.options.optionB!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;B. ${multipleAnwser.options.optionB}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${multipleAnwser.options.optionC!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;C. ${multipleAnwser.options.optionC}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${multipleAnwser.options.optionD!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;D. ${multipleAnwser.options.optionD}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <c:if test="${multipleAnwser.options.optionE!=null}">
                                                                            <tr>
                                                                                <td width="100%">&nbsp;E. ${multipleAnwser.options.optionE}</td>
                                                                            </tr>
                                                                        </c:if>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <br>
                                                                                <font style="font-weight: bolder">请选择答案：</font>
                                                                                <c:if test="${multipleAnwser.options.optionA!=null}">
                                                                                    <input type="checkbox" name="multiple${multipleAnwser.id}" value="A" style="margin-left: 10px">A
                                                                                </c:if>
                                                                                <c:if test="${multipleAnwser.options.optionB!=null}">
                                                                                    <input type="checkbox" name="multiple${multipleAnwser.id}" value="B" style="margin-left: 10px">B
                                                                                </c:if>
                                                                                <c:if test="${multipleAnwser.options.optionC!=null}">
                                                                                    <input type="checkbox" name="multiple${multipleAnwser.id}" value="C" style="margin-left: 10px">C
                                                                                </c:if>
                                                                                <c:if test="${multipleAnwser.options.optionD!=null}">
                                                                                    <input type="checkbox" name="multiple${multipleAnwser.id}" value="D" style="margin-left: 10px">D
                                                                                </c:if>
                                                                                <c:if test="${multipleAnwser.options.optionE!=null}">
                                                                                    <input type="checkbox" name="multiple${multipleAnwser.id}" value="E" style="margin-left: 10px">E
                                                                                </c:if>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>

                                                    <!-- 判断题 -->
                                                    <c:if test="${requestScope.exam.determineAnwser!=null}">
                                                        <div id="determine-anwser" style="margin-top: 30px">
                                                            <h3 class="title">（
                                                                <c:choose>
                                                                    <c:when test="${i eq 1}">
                                                                        一
                                                                    </c:when>
                                                                    <c:when test="${i eq 2}">
                                                                        二
                                                                    </c:when>
                                                                    <c:when test="${i eq 3}">
                                                                        三
                                                                    </c:when>
                                                                    <c:when test="${i eq 4}">
                                                                        四
                                                                    </c:when>
                                                                    <c:when test="${i eq 5}">
                                                                        五
                                                                    </c:when>
                                                                    <c:when test="${i eq 6}">
                                                                        六
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    </c:otherwise>
                                                                </c:choose>
                                                                ）判断题，每题2分，共${requestScope.exam.determineAnwser.size()}题，共${2*requestScope.exam.determineAnwser.size()}分。
                                                            </h3>
                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                            <!-- 判断题的每一题 -->
                                                            <c:forEach var="determineAnwser" items="${requestScope.exam.determineAnwser}" varStatus="sta_determine">
                                                                <div id="determine${sta_determine.index+1}" style="margin-left: 40px;margin-top: 20px">
                                                                    <table>
                                                                        <tr>
                                                                            <td width="100%" style="font-weight: bolder">${sta_determine.index+1}. <font class="subject">${determineAnwser.question}</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <br>
                                                                                <font style="font-weight: bolder">请选择答案：</font>
                                                                                <input type="radio" name="determine${determineAnwser.id}" value="true" style="margin-left: 10px">正确
                                                                                <input type="radio" name="determine${determineAnwser.id}" value="false" style="margin-left: 40px">错误
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>

                                                    <!-- 填空题 -->
                                                    <c:if test="${requestScope.exam.fillInBlankAnwser!=null}">
                                                        <div id="fill_in_blank" style="margin-top: 30px">
                                                            <h3 class="title">（
                                                                <c:choose>
                                                                    <c:when test="${i eq 1}">
                                                                        一
                                                                    </c:when>
                                                                    <c:when test="${i eq 2}">
                                                                        二
                                                                    </c:when>
                                                                    <c:when test="${i eq 3}">
                                                                        三
                                                                    </c:when>
                                                                    <c:when test="${i eq 4}">
                                                                        四
                                                                    </c:when>
                                                                    <c:when test="${i eq 5}">
                                                                        五
                                                                    </c:when>
                                                                    <c:when test="${i eq 6}">
                                                                        六
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    </c:otherwise>
                                                                </c:choose>
                                                                ）填空题，每题2分，共${requestScope.exam.fillInBlankAnwser.size()}题，共${2*requestScope.exam.fillInBlankAnwser.size()}分。
                                                            </h3>
                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                            <!-- 填空题的每一题 -->
                                                            <c:forEach var="fillInBlankAnwser" items="${requestScope.exam.fillInBlankAnwser}" varStatus="sta_fill_in_blank">
                                                                <div id="fill_in_blank${sta_fill_in_blank.index+1}" style="margin-left: 40px;margin-top: 20px">
                                                                    <table>
                                                                        <tr>
                                                                            <td width="100%" style="font-weight: bolder">${sta_fill_in_blank.index+1}. <font class="subject">${fillInBlankAnwser.question}</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <br>
                                                                                <font style="font-weight: bolder">请输入答案：</font>
                                                                                <input type="text" name="fill_in_blank${fillInBlankAnwser.id}" style="width: 300px;margin-left: 10px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>

                                                    <!-- 简答题 -->
                                                    <c:if test="${requestScope.exam.simpleAnwser!=null}">
                                                        <div id="simple" style="margin-top: 30px">
                                                            <h3 class="title">（
                                                                <c:choose>
                                                                    <c:when test="${i eq 1}">
                                                                        一
                                                                    </c:when>
                                                                    <c:when test="${i eq 2}">
                                                                        二
                                                                    </c:when>
                                                                    <c:when test="${i eq 3}">
                                                                        三
                                                                    </c:when>
                                                                    <c:when test="${i eq 4}">
                                                                        四
                                                                    </c:when>
                                                                    <c:when test="${i eq 5}">
                                                                        五
                                                                    </c:when>
                                                                    <c:when test="${i eq 6}">
                                                                        六
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    </c:otherwise>
                                                                </c:choose>
                                                                ）简答题，每题5分，共${requestScope.exam.simpleAnwser.size()}题，共${5*requestScope.exam.simpleAnwser.size()}分。
                                                            </h3>
                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                            <!-- 简答题的每一题 -->
                                                            <c:forEach var="simpleAnwser" items="${requestScope.exam.simpleAnwser}" varStatus="sta_simple">
                                                                <div id="simple${sta_simple.index+1}" style="margin-left: 40px;margin-top: 20px">
                                                                    <table>
                                                                        <tr>
                                                                            <td width="100%" style="font-weight: bolder">${sta_simple.index+1}. <font class="subject">${simpleAnwser.question}</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <textarea name="simple${simpleAnwser.id}" cols="30" rows="5" style="width: 800px"></textarea>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>

                                                    <!-- 编程题 -->
                                                    <c:if test="${requestScope.exam.programmingAnwser!=null}">
                                                        <div id="programming" style="margin-top: 30px">
                                                            <h3 class="title">（
                                                                <c:choose>
                                                                    <c:when test="${i eq 1}">
                                                                        一
                                                                    </c:when>
                                                                    <c:when test="${i eq 2}">
                                                                        二
                                                                    </c:when>
                                                                    <c:when test="${i eq 3}">
                                                                        三
                                                                    </c:when>
                                                                    <c:when test="${i eq 4}">
                                                                        四
                                                                    </c:when>
                                                                    <c:when test="${i eq 5}">
                                                                        五
                                                                    </c:when>
                                                                    <c:when test="${i eq 6}">
                                                                        六
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    </c:otherwise>
                                                                </c:choose>
                                                                ）编程题，每题10分，共${requestScope.exam.programmingAnwser.size()}题，共${10*requestScope.exam.programmingAnwser.size()}分。
                                                            </h3>
                                                            <c:set var="i" value="${i+1}" scope="page"/>
                                                            <!-- 编程题的每一题 -->
                                                            <c:forEach var="programming" items="${requestScope.exam.programmingAnwser}" varStatus="sta_programming">
                                                                <div id="programming${sta_programming.index+1}" style="margin-left: 40px;margin-top: 20px">
                                                                    <table>
                                                                        <tr>
                                                                            <td width="100%" style="font-weight: bolder">${sta_programming.index+1}. <font class="subject">${programming.question}</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <textarea name="programming${programming.id}" cols="30" rows="10" style="width: 800px"></textarea>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>

                                                    <!-- 交卷按钮 -->
                                                    <div style="text-align: center">
                                                        <br>
                                                        <button id="hand-in-paper" type="button" class="btn btn-info btn-lg">交卷</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                蓝桥小组
            </div>
            <div>
                <strong>蓝桥</strong> 考试系统 &copy; 2018-2019
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>
<script src="vendor/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="vendor/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->

<script src="vendor/js/inspinia.js"></script>
<script src="vendor/js/plugins/pace/pace.min.js"></script>

<!-- Toastr -->
<script src="vendor/js/plugins/toastr/toastr.min.js"></script>

<!-- Sweet alert -->
<script src="vendor/js/plugins/sweetalert/sweetalert.min.js"></script>


<%--
    <!-- Flot -->
    <script src="vendor/js/plugins/flot/jquery.flot.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="vendor/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="vendor/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="vendor/js/demo/peity-demo.js"></script>

    <!-- jQuery UI -->
    <script src="vendor/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="vendor/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="vendor/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="vendor/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="vendor/js/plugins/chartJs/Chart.min.js"></script>
    --%>



<script>


    $('#hand-in-paper').click(function () {
        swal({
            title: "交卷提示",      //弹出框的title
            text: "请认真检查试卷，提交后将不能修改答案和重新提交！",   //弹出框里面的提示文本
            type: "warning",        //弹出框类型
            showCancelButton: true, //是否显示取消按钮
            confirmButtonColor: "#DD6B55",//确定按钮颜色
            cancelButtonText: "取消",//取消按钮文本
            confirmButtonText: "确定",//确定按钮上面的文档
            closeOnConfirm: false,
        }, function () {
            $.ajax({
                type: "POST",
                url: "paper/handInPaper",
                data: $('#test-paper-form').serialize(),
                success: function (json) {
                    if (json.actionFlag){
                        swal("Success!", "提交成功！", "success");
                    }else {
                        swal("Failed!", "提交失败！", "error");
                    }
                },
                dataType: 'json'
            });
        });
    });
</script>
</body>
</html>
