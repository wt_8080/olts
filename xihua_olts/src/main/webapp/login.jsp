<%--
  Created by IntelliJ IDEA.
  User: WT
  Date: 2018/10/7
  Time: 10:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>olts登录</title>
    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg" style="background-image: url('upload/11.jpg'); background-repeat:no-repeat;background-size:100% 100%;" >

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <h5 class="logo-name">OLTS</h5>
        <h3>欢迎来到在线考试系统</h3>
<%--        <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
            <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
        </p>
        <p>Login in. To see it in action.</p>--%>

        <div class="m-b-md">
            <img alt="image" class="img-circle circle-border" src="images/user.png">
        </div>

        <form class="m-t" role="form" action="user/login">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="用户名" required="" name="userName">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="密码" required="" name="passWord">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">登录</button>

           <%-- <a href="#"><small>忘记密码?</small></a>--%>
            <%--<a class="btn btn-sm btn-white btn-block" href="register.jsp">新建账号</a>--%>
        </form>


        <p class="m-t"> <small>蓝桥小分队提供技术支持 &copy; 2018 - 2019</small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>

</body>
</html>
