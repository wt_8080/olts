package com.olts.vo;

/**
 * Created by Administrator on 2018/10/12.
 */
public class Msg {
    private String path;
    private boolean msg;

    public Msg() {
    }

    public Msg(String path, boolean msg) {
        this.path = path;
        this.msg = msg;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isMsg() {
        return msg;
    }

    public void setMsg(boolean msg) {
        this.msg = msg;
    }
}
