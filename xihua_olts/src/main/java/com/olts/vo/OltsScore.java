package com.olts.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wxk on 2018/10/7.
 */
public class OltsScore implements Serializable {

    private  Integer id;
    private  Double score;
    private  Double fspScore;
    private Date testDate;
    private String descrpt;
    private Integer userId;
    private String examNo;

    public OltsScore() {
    }

    public OltsScore(Integer id, Double score, Double fspScore, Date testDate, String descrpt, Integer userId, String examNo) {
        this.id = id;
        this.score = score;
        this.fspScore = fspScore;
        this.testDate = testDate;
        this.descrpt = descrpt;
        this.userId = userId;
        this.examNo = examNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getFspScore() {
        return fspScore;
    }

    public void setFspScore(Double fspScore) {
        this.fspScore = fspScore;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    public String getDescrpt() {
        return descrpt;
    }

    public void setDescrpt(String descrpt) {
        this.descrpt = descrpt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getExamNo() {
        return examNo;
    }

    public void setExamNo(String examNo) {
        this.examNo = examNo;
    }

    @Override
    public String toString() {
        return "OltsScore{" +
                "id='" + id + '\'' +
                ", score=" + score +
                ", fspScore=" + fspScore +
                ", testDate=" + testDate +
                ", descrpt='" + descrpt + '\'' +
                ", userId=" + userId +
                ", examNo='" + examNo + '\'' +
                '}';
    }
}
