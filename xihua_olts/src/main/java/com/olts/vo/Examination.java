package com.olts.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by wxk on 2018/10/7.
 */
public class Examination implements Serializable {

    private String examNo;
    private Integer userId;
    private Date examDate;
    private String singleId;
    private String multipleId;
    private String trueFalseId;
    private String fillInGapsId;
    private String simpleAnwserId;
    private String programId;
    private String descrpt;
    private Integer validFlag;

    /**
     * 单选题
     */
    private List<SmdQuestions> singleAnwser;
    /**
     * 多选题
     */
    private List<SmdQuestions> multipleAnwser;
    /**
     * 判断题
     */
    private List<SmdQuestions> determineAnwser;
    /**
     * 填空题
     */
    private List<FspQuestions> fillInBlankAnwser;
    /**
     * 简答题
     */
    private List<FspQuestions> simpleAnwser;
    /**
     * 编程题
     */
    private List<FspQuestions> programmingAnwser;

    public Examination() {
    }

    public Examination(String examNo, Integer userId, Date examDate, String singleId, String multipleId, String trueFalseId, String fillInGapsId, String simpleAnwserId, String programId, String descrpt, Integer validFlag, List<SmdQuestions> singleAnwser, List<SmdQuestions> multipleAnwser, List<SmdQuestions> determineAnwser, List<FspQuestions> fillInBlankAnwser, List<FspQuestions> simpleAnwser, List<FspQuestions> programmingAnwser) {
        this.examNo = examNo;
        this.userId = userId;
        this.examDate = examDate;
        this.singleId = singleId;
        this.multipleId = multipleId;
        this.trueFalseId = trueFalseId;
        this.fillInGapsId = fillInGapsId;
        this.simpleAnwserId = simpleAnwserId;
        this.programId = programId;
        this.descrpt = descrpt;
        this.validFlag = validFlag;
        this.singleAnwser = singleAnwser;
        this.multipleAnwser = multipleAnwser;
        this.determineAnwser = determineAnwser;
        this.fillInBlankAnwser = fillInBlankAnwser;
        this.simpleAnwser = simpleAnwser;
        this.programmingAnwser = programmingAnwser;
    }

    public String getExamNo() {
        return examNo;
    }

    public void setExamNo(String examNo) {
        this.examNo = examNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getSingleId() {
        return singleId;
    }

    public void setSingleId(String singleId) {
        this.singleId = singleId;
    }

    public String getMultipleId() {
        return multipleId;
    }

    public void setMultipleId(String multipleId) {
        this.multipleId = multipleId;
    }

    public String getTrueFalseId() {
        return trueFalseId;
    }

    public void setTrueFalseId(String trueFalseId) {
        this.trueFalseId = trueFalseId;
    }

    public String getFillInGapsId() {
        return fillInGapsId;
    }

    public void setFillInGapsId(String fillInGapsId) {
        this.fillInGapsId = fillInGapsId;
    }

    public String getSimpleAnwserId() {
        return simpleAnwserId;
    }

    public void setSimpleAnwserId(String simpleAnwserId) {
        this.simpleAnwserId = simpleAnwserId;
    }

    public String getProgramId() {
        return programId;
    }

    public void setProgramId(String programId) {
        this.programId = programId;
    }

    public String getDescrpt() {
        return descrpt;
    }

    public void setDescrpt(String descrpt) {
        this.descrpt = descrpt;
    }

    public Integer getValidFlag() {
        return validFlag;
    }

    public void setValidFlag(Integer validFlag) {
        this.validFlag = validFlag;
    }

    public List<SmdQuestions> getSingleAnwser() {
        return singleAnwser;
    }

    public void setSingleAnwser(List<SmdQuestions> singleAnwser) {
        this.singleAnwser = singleAnwser;
    }

    public List<SmdQuestions> getMultipleAnwser() {
        return multipleAnwser;
    }

    public void setMultipleAnwser(List<SmdQuestions> multipleAnwser) {
        this.multipleAnwser = multipleAnwser;
    }

    public List<SmdQuestions> getDetermineAnwser() {
        return determineAnwser;
    }

    public void setDetermineAnwser(List<SmdQuestions> determineAnwser) {
        this.determineAnwser = determineAnwser;
    }

    public List<FspQuestions> getFillInBlankAnwser() {
        return fillInBlankAnwser;
    }

    public void setFillInBlankAnwser(List<FspQuestions> fillInBlankAnwser) {
        this.fillInBlankAnwser = fillInBlankAnwser;
    }

    public List<FspQuestions> getSimpleAnwser() {
        return simpleAnwser;
    }

    public void setSimpleAnwser(List<FspQuestions> simpleAnwser) {
        this.simpleAnwser = simpleAnwser;
    }

    public List<FspQuestions> getProgrammingAnwser() {
        return programmingAnwser;
    }

    public void setProgrammingAnwser(List<FspQuestions> programmingAnwser) {
        this.programmingAnwser = programmingAnwser;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        if ((singleAnwser!=null) && (!singleAnwser.isEmpty())){
            sb.append("单选题：" + "\n");
            for (SmdQuestions smdQuestions : singleAnwser) {
                sb.append(smdQuestions.toString() + "\n");
            }
        }

        if ((multipleAnwser!=null) && (!multipleAnwser.isEmpty())){
            sb.append("多选题：" + "\n");
            for (SmdQuestions smdQuestions : multipleAnwser) {
                sb.append(smdQuestions.toString() + "\n");
            }
        }

        if ((determineAnwser !=null) && (!determineAnwser.isEmpty())){
            sb.append("判断题：" + "\n");
            for (SmdQuestions smdQuestions : determineAnwser) {
                sb.append(smdQuestions.toString() + "\n");
            }
        }

        if ((fillInBlankAnwser!=null) && (!fillInBlankAnwser.isEmpty())){
            sb.append("填空题：" + "\n");
            for (FspQuestions fspQuestions : fillInBlankAnwser) {
                sb.append(fspQuestions.toString() + "\n");
            }
        }

        if ((simpleAnwser!=null) && (!simpleAnwser.isEmpty())) {
            sb.append("简答题：" + "\n");
            for (FspQuestions fspQuestions : simpleAnwser) {
                sb.append(fspQuestions.toString() + "\n");
            }
        }

        if ((programmingAnwser!=null) && (!programmingAnwser.isEmpty())) {
            sb.append("编程题：" + "\n");
            for (FspQuestions fspQuestions : programmingAnwser) {
                sb.append(fspQuestions.toString() + "\n");
            }
        }

        return "Examination{" +
                "examNo='" + examNo + '\'' +
                ", userId=" + userId +
                ", examDate=" + examDate +
                ", singleId='" + singleId + '\'' +
                ", multipleId='" + multipleId + '\'' +
                ", trueFalseId='" + trueFalseId + '\'' +
                ", fillInGapsId='" + fillInGapsId + '\'' +
                ", simpleAnwserId='" + simpleAnwserId + '\'' +
                ", programId='" + programId + '\'' +
                ", descrpt='" + descrpt + '\'' +
                ", validFlag=" + validFlag + "\n" +
                sb.toString() +
                '}';
    }
}
