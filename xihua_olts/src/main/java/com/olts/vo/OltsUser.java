package com.olts.vo;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 王涛
 * @Created by WT on 2018/10/7.
 */
public class OltsUser implements Serializable{
    private Integer id;
    private String stuNo;
    private String idCardNo;
    private String userName;
    private String passWord;
    private String mobile;
    private String homeTel;
    private String homeAddr;
    private String schAddr;
    private String qq;
    private String email;
    private Integer userType;
    private String gender;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String nationPlace;
    private String marjor;
    private String eduBackground;
    private String graduateSchool;
    private String path;
    /**
     * 用户的成绩
     */
    private OltsScore oltsScore;
    /**
     * 用户的主观题答案
     */
    private List<FspAnswer> fspAnswer;

    public OltsUser() {
    }

    public OltsUser(Integer id, String stuNo, String idCardNo, String userName, String passWord, String mobile, String homeTel, String homeAddr, String schAddr, String qq, String email, Integer userType, String gender, Date birthday, String nationPlace, String marjor, String eduBackground, String graduateSchool, String path) {
        this.id = id;
        this.stuNo = stuNo;
        this.idCardNo = idCardNo;
        this.userName = userName;
        this.passWord = passWord;
        this.mobile = mobile;
        this.homeTel = homeTel;
        this.homeAddr = homeAddr;
        this.schAddr = schAddr;
        this.qq = qq;
        this.email = email;
        this.userType = userType;
        this.gender = gender;
        this.birthday = birthday;
        this.nationPlace = nationPlace;
        this.marjor = marjor;
        this.eduBackground = eduBackground;
        this.graduateSchool = graduateSchool;
        this.path = path;
    }

    public List<FspAnswer> getFspAnswer() {
        return fspAnswer;
    }

    public void setFspAnswer(List<FspAnswer> fspAnswer) {
        this.fspAnswer = fspAnswer;
    }

    public OltsScore getOltsScore() {
        return oltsScore;
    }

    public void setOltsScore(OltsScore oltsScore) {
        this.oltsScore = oltsScore;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getHomeTel() {
        return homeTel;
    }

    public void setHomeTel(String homeTel) {
        this.homeTel = homeTel;
    }

    public String getHomeAddr() {
        return homeAddr;
    }

    public void setHomeAddr(String homeAddr) {
        this.homeAddr = homeAddr;
    }

    public String getSchAddr() {
        return schAddr;
    }

    public void setSchAddr(String schAddr) {
        this.schAddr = schAddr;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getUserType() {
        return userType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getNationPlace() {
        return nationPlace;
    }

    public void setNationPlace(String nationPlace) {
        this.nationPlace = nationPlace;
    }

    public String getMarjor() {
        return marjor;
    }

    public void setMarjor(String marjor) {
        this.marjor = marjor;
    }

    public String getEduBackground() {
        return eduBackground;
    }

    public void setEduBackground(String eduBackground) {
        this.eduBackground = eduBackground;
    }

    public String getGraduateSchool() {
        return graduateSchool;
    }

    public void setGraduateSchool(String graduateSchool) {
        this.graduateSchool = graduateSchool;
    }

    @Override
    public String toString() {
        return "OltsUser{" +
                "id=" + id +
                ", stuNo='" + stuNo + '\'' +
                ", idCardNo='" + idCardNo + '\'' +
                ", userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", mobile='" + mobile + '\'' +
                ", homeTel='" + homeTel + '\'' +
                ", homeAddr='" + homeAddr + '\'' +
                ", schAddr='" + schAddr + '\'' +
                ", qq='" + qq + '\'' +
                ", email='" + email + '\'' +
                ", userType='" + userType + '\'' +
                ", gender='" + gender + '\'' +
                ", birthday=" + birthday +
                ", nationPlace='" + nationPlace + '\'' +
                ", marjor='" + marjor + '\'' +
                ", eduBackground='" + eduBackground + '\'' +
                ", graduateSchool='" + graduateSchool + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
