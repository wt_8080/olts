package com.olts.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wxk on 2018/10/7.
 */
public class SmdQuestions implements Serializable {
    private Integer id;
    private String question;
    private String correct;
    private Integer questionType;
    private Integer techCateId;
    private String descrpt;
    private Date pubdate;
    private SmdOptions smdOptions;
    private Courses courses;
    private TechCategory techCategory;

    /**
     * 单选、多选试题包含的选项
     */
    private SmdOptions options;

    public SmdQuestions() {
    }

    public SmdQuestions(Integer id, String question, String correct, Integer questionType, Integer techCateId, String descrpt, Date pubdate, SmdOptions options) {
        this.id = id;
        this.question = question;
        this.correct = correct;
        this.questionType = questionType;
        this.techCateId = techCateId;
        this.descrpt = descrpt;
        this.pubdate = pubdate;
        this.options = options;
    }

    public Courses getCourses() {
        return courses;
    }

    public void setCourses(Courses courses) {
        this.courses = courses;
    }

    public TechCategory getTechCategory() {
        return techCategory;
    }

    public void setTechCategory(TechCategory techCategory) {
        this.techCategory = techCategory;
    }

    public SmdOptions getSmdOptions() {
        return smdOptions;
    }

    public void setSmdOptions(SmdOptions smdOptions) {
        this.smdOptions = smdOptions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public Integer getTechCateId() {
        return techCateId;
    }

    public void setTechCateId(Integer techCateId) {
        this.techCateId = techCateId;
    }

    public String getDescrpt() {
        return descrpt;
    }

    public void setDescrpt(String descrpt) {
        this.descrpt = descrpt;
    }

    public Date getPubdate() {
        return pubdate;
    }

    public void setPubdate(Date pubdate) {
        this.pubdate = pubdate;
    }

    public SmdOptions getOptions() {
        return options;
    }

    public void setOptions(SmdOptions options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "SmdQuestions{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", correct='" + correct + '\'' +
                ", questionType=" + questionType +
                ", techCateId=" + techCateId +
                ", descrpt='" + descrpt + '\'' +
                ", pubdate=" + pubdate +
                ", options=" + options +
                '}';
    }
}

