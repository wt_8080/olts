package com.olts.vo;

import java.io.Serializable;

/**
 * Created by wxk on 2018/10/7.
 */
public class Courses implements Serializable{

    private Integer id;
    private String courseName;

    public Courses() {
    }

    public Courses(Integer id, String courseName) {
        this.id = id;
        this.courseName = courseName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Override
    public String toString() {
        return "Courses{" +
                "id=" + id +
                ", courseName='" + courseName + '\'' +
                '}';
    }
}
