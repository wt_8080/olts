package com.olts.mapper;

import com.olts.vo.FspAnswer;
import com.olts.vo.OltsUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by bingge on 2018/10/10.
 */
public interface FspAnwserMapper {
    /**
     * 插入主观题答案
     * @param anwser 主观题答案
     * @return 插入的行数
     */
    int insertAnwser(FspAnswer anwser);

    /**
     * 通过试卷编号、用户编号查询主观题
     * @param examNo 试卷编号
     * @param userId 用户编号
     * @return 行数
     */
    int selectById(@Param("examNo") String examNo, @Param("userId")int userId);

    /**
     * 查询所有用户
     * （包含用户姓名<OltsUser>，客观题分数<OltsScore>，主观题答案<FspAnswer>，主观题问题<FspQuestions>）
     * @return 用户列表（包含用户、分数、主观题答案、主观题问题）
     */
    List<OltsUser> selectAllAnwser(@Param("examNo") String examNo);
}
