package com.olts.mapper;

import com.olts.vo.Examination;
import com.olts.vo.OltsUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by CZX on 2018/10/7.
 */
@Repository
public interface OltsPaperMapper {
    List<OltsUser> selectScore(OltsUser oltsUser);
    List<OltsUser> selectScore(@Param("h") OltsUser oltsUser,
                               @Param("pageNum") int pageNum,
                               @Param("pageSize") int pageSize);
    List<OltsUser> selectAllScore(OltsUser oltsUser);
    int deleteOne(OltsUser oltsUser);
    int selectSingleQuestionIsExist(Examination examination);
    int addSingleQuestion(Examination examination);
    int selectMutiQuestionIsExist(Examination examination);
    int addMutiQuestion(Examination examination);
    int selectJudgeQuestionIsExist(Examination examination);
    int addJudgeQuestion(Examination examination);
    int setExamFlag(Examination examination);
    String selectSingleForExam(Examination examination);
    String selectMutiForExam(Examination examination);
    String selectJudgeForExam(Examination examination);
    int deleteSingleForExam(Examination examination);
    int deleteMutiForExam(Examination examination);
    int deleteJudgeForExam(Examination examination);

    List<Examination> selectExamNo();
}
