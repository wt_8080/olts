
package com.olts.mapper;

import com.github.pagehelper.Page;
import com.olts.vo.Courses;

import java.util.List;

/**
 * @author Administrator
 */
public interface BaseMapper<T, PK extends java.io.Serializable> {
	
	/**
	 * 查询
	 * @param page
	 * @return
	 */
	public List<T> select(Page<T> page);

	/**
	 * 插入
	 * @param entity
	 */
	public void insert(T entity);

	/**
	 * 更新
	 * @param entity
	 */
	public void update(T entity);

	/**
	 * 删除
	 * @param entity
	 */
	public void delete(T entity);


}
