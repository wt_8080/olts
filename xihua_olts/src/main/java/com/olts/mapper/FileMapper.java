package com.olts.mapper;

import com.olts.vo.OltsUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2018/10/12.
 */
@Repository
public interface FileMapper {

    int updatePath(@Param("id") Integer id,
                   @Param("path") String destFileName);

    int insertAllUser(@Param("users")List<OltsUser> users);
}
