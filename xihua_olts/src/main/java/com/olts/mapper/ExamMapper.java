package com.olts.mapper;

import com.olts.vo.Examination;
import com.olts.vo.SmdQuestions;

/**
 * Created by bingge on 2018/10/8.
 */
public interface ExamMapper {
    /**
     * 按编号查询试卷
     * @param id 试卷id
     * @return 根据试卷id查询的试卷对象
     */
    Examination selectExamById(String id);
    /*
    * 删除单选题
    * */
    int deleteSingleQuestion(SmdQuestions smdQuestions);
    /*
    * 删除单选选项
    * */
    int deleteSingleOption(SmdQuestions smdQuestions);
}
