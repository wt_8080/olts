package com.olts.mapper;

import com.olts.vo.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yuan on 2018/10/8.
 */
@Repository
public interface OltsAddMapper {

    List<Courses> selectCourses();

    List<TechCategory> selectTechCategoryByCoursesId(Integer coursesId);

    int insertRadio(SmdQuestions smdQuestions);

    int insertOption(SmdOptions smdOptions);

    int insertAsk(FspQuestions fspQuestions);

}

