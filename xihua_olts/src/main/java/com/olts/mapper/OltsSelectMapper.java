package com.olts.mapper;

import com.olts.vo.Courses;
import com.olts.vo.FspQuestions;
import com.olts.vo.SmdQuestions;
import com.olts.vo.TechCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wxk on 2018/10/8.
 */
@Repository
public interface OltsSelectMapper {


    /**
     * 加载课程
     * @return
     */
    List<Courses> selectCoures();

    /**
     * 加载知识点
     * @param courseId
     * @return
     */
    List<TechCategory> selectTechCategory(Integer courseId);

    /**
     * 使用mybatis的参数注解，pageNum和pageSize两个参数SQL语句中不用处理，是给pagehelp插件内部处理用的
     * @param smdQuestions
     * @param pageNum   页码
     * @param pageSize  页面记录数
     * @return
     */
    List<SmdQuestions> selectBySelective(@Param("q") SmdQuestions smdQuestions,
                                         @Param("pageNum") int pageNum,
                                         @Param("pageSize") int pageSize);


    /**
     * 查询判断题
     * @param smdQuestions
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<SmdQuestions> selectByJudge(@Param("q") SmdQuestions smdQuestions,
                                         @Param("pageNum") int pageNum,
                                         @Param("pageSize") int pageSize);


    int updateJudge(SmdQuestions smdQuestions);
    
    int deleteJudge(SmdQuestions smdQuestions);

    int updateChoice(SmdQuestions smdQuestions);

    List<FspQuestions> selectByFsp(@Param("fq") FspQuestions fspQuestions,
                                   @Param("pageNum") int pageNum,
                                   @Param("pageSize") int pageSize);

    int updateFsp(FspQuestions fspQuestions);
    int deleteFsp(FspQuestions fspQuestions);

}
