package com.olts.mapper;

import com.olts.vo.Courses;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2018/10/7 0007.
 */
@Repository
public interface OltsClassifyCoursesMapper {
    List<Courses> selectAllCourses(@Param("pageNum") int pageNum,
                                   @Param("pageSize") int pageSize);
    Courses selectCoursesById(int id);
    int insertCourses(Courses courses);
    int updateCourses(Courses courses);
    int deleteCourseById(int id);
}
