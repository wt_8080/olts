package com.olts.mapper;

import com.olts.vo.Courses;
import com.olts.vo.TechCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by BLTM on 2018/10/8.
 */
@Repository
public interface TechCategoryMapper extends BaseMapper<TechCategory,Integer> {
    /*
     *@描述 查询所有的课程名
     *@参数
     *@返回值 List
     *@作者 xx
     *@日期 2018/10/8
     *@时间 14:35
    */
    public List<Courses> selectAll();

    public List<TechCategory> selectTechByCourseId(
                                                   @Param("pageNum") Integer pageNum,
                                                   @Param("pageSize") Integer pageSize,
                                                   @Param("courses") Courses courses
                                                   );

    public int insertTechById(TechCategory techCategory);

    public int deleteTechById(TechCategory techCategory);

    public int updateTechById(TechCategory techCategory);
}
