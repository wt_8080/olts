/**
 * 
 */
package com.olts.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.olts.vo.OltsUser;

import java.util.List;

/**
 * @author Administrator
 *
 */
@Repository
public interface OltsUserMapper extends BaseMapper<OltsUser, Integer> {

	/**
	 * 登录
	 * @param user
	 * @return
	 */
	public OltsUser login(OltsUser user);

	Page<OltsUser> selectByMarJor(@Param("marjor") String marjor,
						  @Param("pageNum") int pageNum,
						  @Param("pageSize") int pageSize);

	int deleteById(@Param("param") Integer[] param);

	OltsUser selectById(Integer id);

    int updateById(OltsUser user);


	/**
	 * 注册
	 * @param registerUser
	 * @return
	 */
	int insertRegister(OltsUser registerUser);
}
