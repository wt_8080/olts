package com.olts.controller;

import com.github.pagehelper.Page;
import com.olts.service.IOltsPaperService;
import com.olts.vo.Examination;
import com.olts.vo.OltsScore;
import com.olts.vo.OltsUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by CZX on 2018/10/7.
 */
@Controller
@RequestMapping("/Paper")
public class OltsPaperController {
    @Resource
    IOltsPaperService oltsPaperService;


    @RequestMapping(value="/select_score",method = RequestMethod.GET)
    public String  selectScore(Integer id, String examNo, ModelMap map){
        OltsUser oltsUser = new OltsUser();
        OltsScore oltsScore = new OltsScore();
        oltsScore.setExamNo(examNo);
        oltsUser.setId(id);
        oltsUser.setOltsScore(oltsScore);

        System.out.println(oltsUser.getOltsScore().getExamNo());
        map.addAttribute("score",oltsPaperService.selectScore(oltsUser));
        System.out.println("11111111111111111");
        return "paper/score_table";
    }

    @RequestMapping(value = "/select_All_Score",method = RequestMethod.GET)
    public String selectAllScore(String examNo,ModelMap modelMap){
        OltsScore oltsScore = new OltsScore();
        oltsScore.setExamNo(examNo);
        OltsUser oltsUser = new OltsUser();
        oltsUser.setOltsScore(oltsScore);
        modelMap.addAttribute("AllScore",oltsPaperService.selectAllScore(oltsUser));
        return "paper/score_table";
    }

    @RequestMapping(value = "/delete_One",method = RequestMethod.GET)
    public String deleteOne(OltsUser oltsUser, OltsScore oltsScore, ModelMap modelMap, HttpServletResponse response) throws IOException {
        oltsUser.setOltsScore(oltsScore);
        int b = oltsPaperService.deleteOne(oltsUser);
        if (b>0){

            response.getWriter().print("{\"resp\":true}");
        }else {
            response.getWriter().print("{\"resp\":false}");

        }
        modelMap.addAttribute("result",b);

        return null;
    }
    @RequestMapping("/search")
    public String search(OltsUser oltsUser, Integer pageNum, ModelMap modelMap, HttpSession session){
        // 查询
        if(pageNum == null){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective",oltsUser);
        }else{ // 翻页
            oltsUser = (OltsUser) session.getAttribute("selective");
        }
        Page<OltsUser> page = (Page<OltsUser>) this.oltsPaperService.selectBySelective(oltsUser, pageNum, 5);
        modelMap.addAttribute("page", page);

        return "views/selcet_Scroe";
    }

    /*
    * 添加单选题   检查单选题是否已经在试卷中存在（试卷维护）
    *
    * */
    @RequestMapping("SingleQuestionIsExist")
    public String SingleQuestionIsExist(Examination examination,HttpServletResponse response) throws IOException {
        System.out.println(examination);
        int i = oltsPaperService.selectSingleQuestionIsExist(examination);
        if (i>0){
            /*此单选题在试卷存在*/
            response.getWriter().print("{\"resp\":true}");
        }else{
            response.getWriter().print("{\"resp\":false}");
        }
        return null;
    }
    /*
    * 添加单选题
    * */
    @RequestMapping("addSingleQuestion")
    public String addSingleQuestionIsExist(Examination examination,HttpServletResponse response) throws IOException {
        System.out.println(examination);
        int i = oltsPaperService.addSingleQuestion(examination);
        if (i>0){
            response.getWriter().print("{\"respl\":true}");
        } else{
            response.getWriter().print("{\"respl\":false}");
        }
        return null;
    }
    /*
    * 添加多选题   检查多选题是否已经在试卷中存在（试卷维护）
    *
    * */
    @RequestMapping("MutiQuestionIsExist")
    public String MutiQuestionIsExist(Examination examination,HttpServletResponse response) throws IOException {
        System.out.println(examination);
        int i = oltsPaperService.selectMutiQuestionIsExist(examination);
        if (i>0){
            /*此单选题在试卷存在*/
            response.getWriter().print("{\"resp\":true}");
        }else{
            response.getWriter().print("{\"resp\":false}");
        }
        return null;
    }
    /*
    * 添加单选题
    * */
    @RequestMapping("addMutiQuestion")
    public String addMutiQuestionIsExist(Examination examination,HttpServletResponse response) throws IOException {
        System.out.println(examination);
        int i = oltsPaperService.addMutiQuestion(examination);
        if (i>0){
            response.getWriter().print("{\"respl\":true}");
        } else{
            response.getWriter().print("{\"respl\":false}");
        }
        return null;
    }
    /*
    * 添加判断题   检查判断题是否已经在试卷中存在（试卷维护）
    *
    * */
    @RequestMapping("JudgeQuestionIsExist")
    public String JudgeQuestionIsExist(Examination examination,HttpServletResponse response) throws IOException {
        System.out.println(examination);
        int i = oltsPaperService.selectJudgeQuestionIsExist(examination);
        if (i>0){
            /*此单选题在试卷存在*/
            response.getWriter().print("{\"resp\":true}");
        }else{
            response.getWriter().print("{\"resp\":false}");
        }
        return null;
    }
    /*
    * 添加判断题
    * */
    @RequestMapping("addJudgeQuestion")
    public String addJudgeQuestionIsExist(Examination examination,HttpServletResponse response) throws IOException {
        System.out.println(examination);
        int i = oltsPaperService.addJudgeQuestion(examination);
        if (i>0){
            response.getWriter().print("{\"respl\":true}");
        } else{
            response.getWriter().print("{\"respl\":false}");
        }
        return null;
    }
    /*
    * 试卷维护 更新试卷状态
    *
    * */
    @RequestMapping("setExamFlag")
    public String setExamFlag(Examination examination,HttpServletResponse response) throws IOException {
        int i = oltsPaperService.setExamFlag(examination);
        if (i>0){
            response.getWriter().print("{\"respl\":true}");
        } else{
            response.getWriter().print("{\"respl\":false}");
        }
        return null;
    }
    @RequestMapping("selectExamNo")
    @ResponseBody
    public List<Examination> selectExamNo(){
        return oltsPaperService.selectExamNo();
    }
}
