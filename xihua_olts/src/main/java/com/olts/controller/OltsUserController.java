package com.olts.controller;

import com.github.pagehelper.Page;
import com.olts.service.IOltsUserService;
import com.olts.vo.OltsUser;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 王涛
 * @Created by WT on 2018/10/7.
 */
@Controller
@RequestMapping("/user")
public class OltsUserController {

    static Logger logger = Logger.getLogger(OltsUserController.class);

    @Resource
    IOltsUserService oltsUserService;

    @RequestMapping("/login")
    public String login(OltsUser loginUser, HttpSession session){
        OltsUser oltsUser = oltsUserService.login(loginUser);
        if (oltsUser != null) {
            session.setAttribute("logUser", oltsUser);
            return "redirect:/index.jsp";
        }else {
            return "redirect:/login.jsp";
        }
    }

    @RequestMapping(value = "/load_user_update/{id}",method = RequestMethod.GET)
    @ResponseBody
    public OltsUser selectByIdToJson(@PathVariable("id") Integer id,HttpSession session){
        OltsUser user = this.oltsUserService.selectById(id);
        if (user.getStuNo() != null) {
            user.setStuNo(user.getStuNo().trim());
        }
        if (user.getIdCardNo() != null) {
            user.setIdCardNo(user.getIdCardNo().trim());
        }
        if (user.getQq() != null) {
            user.setQq(user.getQq().trim());
        }
        return user;
    }

    @RequestMapping("/search")
    public String search(String marjor, Integer pageNum, ModelMap map,HttpSession session){
        if(pageNum == null){
            pageNum = 1;
            session.setAttribute("selectBySelective",marjor);
        }else {
            marjor = (String) session.getAttribute("selectBySelective");
        }
        Page<OltsUser> page = (Page<OltsUser>)oltsUserService.selectByMarJor(marjor,pageNum,10);
        map.addAttribute("page",page);
        return "user/user_manage";
    }

    @RequestMapping("/deleteById")
    public String deleteById(Integer[] id){
        int row = this.oltsUserService.delete(id);
        return "redirect:search";
    }

    @RequestMapping("/updateById")
    public String updateById(OltsUser user){
        int i = this.oltsUserService.updateById(user);
        return "redirect:search";
    }

    @RequestMapping("/updateByMy")
    public String updateByMy(OltsUser user,HttpSession session){
        int i = this.oltsUserService.updateById(user);
        OltsUser logUser = this.oltsUserService.selectById(user.getId());
        session.removeAttribute("logUser");
        session.setAttribute("logUser",logUser);
        return "redirect:/views/user/user_update.jsp";
    }

    @RequestMapping("register")
    public  String register(OltsUser registerUser){
        oltsUserService.insertRegister(registerUser);
        return "";
    }

    @RequestMapping("exit")
    public String exit(HttpSession session){
        session.removeAttribute("loginUser");
        return "redirect:/login.jsp";
    }
}
