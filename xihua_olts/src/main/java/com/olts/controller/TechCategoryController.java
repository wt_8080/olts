package com.olts.controller;

import com.github.pagehelper.Page;
import com.olts.service.TechCategoryService;
import com.olts.vo.Courses;
import com.olts.vo.TechCategory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by BLTM on 2018/10/8.
 */

/*
 *@program 项目名称
 *@description 描述
 *@author 作者
 *@time 2018/10/8 14:53 
*/
@Controller
@RequestMapping("/tech")
public class TechCategoryController {
    static Logger logger=Logger.getLogger(TechCategoryController.class);

    @Resource
    TechCategoryService techCategoryService;

    /*
     *@描述 用于将查询到的课程名存起来，供给页面使用
     *@参数
     *@返回值
     *@作者 xx
     *@日期 2018/10/8
     *@时间 15:24
    */
    @RequestMapping("/listCourses")
    public String listCourses(ModelMap modelMap){
        List<Courses> courses = techCategoryService.listCourses();
        modelMap.put("courses",courses);
        return "classify/classify_tech_category";
    }

    /*
     *@描述 知识点分页查询的实现
     *@参数 Integer,Courses,ModelMap
     *@返回值
     *@作者 xx
     *@日期 2018/10/11
     *@时间 13:47
    */
    @RequestMapping("/listTechByCourseId")
    public String listTechByCourseId(Integer pageNum,Courses courses, ModelMap modelMap){
        if(pageNum==null){
            pageNum=1;
        }
        Page<TechCategory> page = (Page<TechCategory>) techCategoryService.listTechs(pageNum,10,courses);
        modelMap.put("page",page);
        return "classify/classify_tech_table";
    }


    /*
     *@描述 用于新增知识点
     *@参数 TechCategory
     *@返回值 路径
     *@作者 xx
     *@日期 2018/10/11
     *@时间 13:47
    */
    @RequestMapping("saveTech")
    @ResponseBody
    public String insertTechById(TechCategory techCategory){

        if(!techCategoryService.insertTechById(techCategory)) {
            System.out.println("插入操作");
            return "{\"success\":false,\"message\":\"服务器错误\"}";
        }
        System.out.println("插入操作完成");
        return "{\"success\":true}";
    }

    @RequestMapping("updatePage")
    public String updatePage(TechCategory techCategory,ModelMap modelMap){
        modelMap.put("tech",techCategory);
        return "classify/updatePage";
    }

    @RequestMapping("updateTech")
    @ResponseBody
    public String updateTech(TechCategory techCategory){
        if(!techCategoryService.updateTechById(techCategory)) {
            System.out.println("更新操作");
            return "{\"success\":false,\"message\":\"服务器错误\"}";
        }
        System.out.println("更新操作完成");
        return "{\"success\":true}";
    }

    @RequestMapping("deletePage")
    public String deletePage(TechCategory techCategory,ModelMap modelMap){
        modelMap.put("tech",techCategory);
        return "classify/deletePage";
    }

    @RequestMapping(path="deleteById")
    @ResponseBody
    public String deleteById(TechCategory techCategory) {
        if(!techCategoryService.deleteTechById(techCategory)) {
            return "{\"success\":false,\"message\":\"服务器错误\"}";
        }
        System.out.println("删除操作完成");
        return "{\"success\":true}";
    }
}
