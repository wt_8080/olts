package com.olts.controller;

import com.olts.service.FspAnwserService;
import com.olts.service.OltsScoreService;
import com.olts.vo.OltsScore;
import com.olts.vo.OltsUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by bingge on 2018/10/12.
 */
@Controller
@RequestMapping("/marking")
public class MarkingController {
    @Resource
    private FspAnwserService fspAnwserService;
    @Resource
    private OltsScoreService oltsScoreService;

    @RequestMapping("/selectAllAnwser")
    public String selectAllAnwser(Model model){
        //试卷id先固定死，后面再从session里面取
        String examNo = "xhu201810081";
        List<OltsUser> oltsUsers = fspAnwserService.selectAllAnswer(examNo);
        model.addAttribute("oltsUsers",oltsUsers);
        return "paper/marking";
    }

    @RequestMapping("/complete")
    public String markingAnwser(OltsScore oltsScore, HttpServletResponse response) throws IOException {
        //试卷id先固定死，后面再从session里面取
        String examNo = "xhu201810081";
        oltsScore.setExamNo(examNo);
        if (oltsScore.getUserId()!=null && oltsScore.getFspScore()!=null){
            OltsScore oltsScoreSelect = oltsScoreService.selectById(oltsScore.getUserId(), oltsScore.getExamNo());
            if (oltsScoreSelect.getFspScore() == null) {
                int i = oltsScoreService.updateScore(oltsScore);
                if (i != 0) {
                    response.getWriter().write("{\"actionFlag\":true}");
                    return null;
                }
            }
        }
        response.getWriter().write("{\"actionFlag\":false}");
        return null;
    }
}
