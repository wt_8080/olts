package com.olts.controller;

import com.github.pagehelper.Page;
import com.olts.service.OltsClassifyCoursesService;
import com.olts.vo.Courses;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Administrator on 2018/10/8 0008.
 */
@Controller
@RequestMapping("/classify")
public class OltsClassifyCoursesController {

    @Resource
    private OltsClassifyCoursesService oltsClassifyCoursesService;

    @RequestMapping("/selectCourses")
    public String selectCourses(Integer pageNum, ModelMap modelMap){
        if(pageNum == null){
            pageNum = 1;
        }
        Page<Courses> page = (Page<Courses>) oltsClassifyCoursesService.selectAllCourses(pageNum,10);
        modelMap.addAttribute("mypage", page);
        return "classify/classify_courses";
    }

    @RequestMapping(value = "/insertCourses",method = RequestMethod.POST)
    public String insertCourses(HttpServletRequest request,Courses courses){
        String courseName = request.getParameter("courseName");
        courses = new Courses(null, courseName);
        int i = oltsClassifyCoursesService.insertCourses(courses);
        return (i==1?"redirect:/classify/selectCourses":"redirect:/classify/classify_courses.jsp");
    }
    @RequestMapping(value = "/selectCoursesById",method = RequestMethod.GET)
    public String selectCoursesById(@RequestParam int id,ModelMap modelMap,HttpServletResponse response) throws IOException {
        Courses courses = oltsClassifyCoursesService.selectCoursesById(id);
        if (courses != null) {
            modelMap.addAttribute("courses",courses);
            return "classify/classify_courses_update";
            //response.getWriter().write("{\"actionFlag\":true}");
        }else {
            return "classify/classify_courses_update";
            //response.getWriter().write("{\"actionFlag\":false}");
        }
    }

    @RequestMapping(value = "/updateCourses",method = RequestMethod.GET)
    public String updateCourses(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer id = Integer.valueOf(request.getParameter("cid"));
        String courseName = request.getParameter("courseNameUpdate");
        Courses courses = new Courses(id, courseName);
        int i = oltsClassifyCoursesService.updateCourses(courses);
        PrintWriter writer = response.getWriter();
        if(i != 0){
            writer.write("{\"actionFlag\": true}");
        }else {
            writer.write("{\"actionFlag\": false}");
        }
        return null;
    }

    @RequestMapping(value = "/deleteCourses")
    public String deleteCourses(@RequestParam int id, HttpServletResponse response) throws IOException {
       int i = oltsClassifyCoursesService.deleteCourseById(id);
        PrintWriter writer = response.getWriter();
        if(i != 0){
            writer.write("{\"actionFlag\": true}");
        }else {
            writer.write("{\"actionFlag\": false}");
        }
        return null;
    }
}
