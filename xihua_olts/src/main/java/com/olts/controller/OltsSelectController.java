package com.olts.controller;

import com.github.pagehelper.Page;
import com.olts.service.IOltsSelectService;
import com.olts.vo.Courses;
import com.olts.vo.FspQuestions;
import com.olts.vo.SmdQuestions;
import com.olts.vo.TechCategory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by wxk on 2018/10/8.
 */
@Controller
@RequestMapping("/select")
public class    OltsSelectController {



    @Resource
    IOltsSelectService oltsSelectService;

    /**
     * 页面上ajax加载课程下拉列表框
     * @return
     */
    @RequestMapping(value = "/load_course", method = RequestMethod.GET) //此方法只能处理GET方式的请求
    @ResponseBody  // 此注解springMVC会使用jackson.jar库将List转换成JSON, [{id:101,name:"海淀"},...]
    public List<Courses> loadCourse() { // @ResponseBody也可加到返回类型的前面
        return this.oltsSelectService.selectCourse();
    }

    /**
     * 页面上ajax加载知识点下拉列表框
     * @param courseId
     * @return
     */
    @RequestMapping(value = "/load_TechCategory", method = RequestMethod.GET)
    @ResponseBody
    public List<TechCategory> loadTechCategory(@RequestParam("id") Integer courseId) {
        return oltsSelectService.selectTechCategory(courseId);
    }

    /**
     * 搜索单选题 查询试题用。
     * @param smdQuestions
     * @param pageNum
     * @param modelMap
     * @param session
     * @return
     */
    @RequestMapping("/searchsmd_radio")
    public String searchByRadio(SmdQuestions smdQuestions, Integer pageNum, ModelMap modelMap, HttpSession session,TechCategory techCategory) {
        // 查询
        if(pageNum == null){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", smdQuestions);
        }else{ // 翻页
            smdQuestions = (SmdQuestions) session.getAttribute("selective");
        }

        Page<SmdQuestions> page = (Page<SmdQuestions>) this.oltsSelectService.selectBySelective(techCategory,smdQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);

        return "select/selectnavigation";
    }
    /*
    * 试卷维护用,添加单选题
    *
    * */
    @RequestMapping("/searchAllsmd_radio")
    public String searchByRadioAll(SmdQuestions smdQuestions, String examNo,Integer pageNum, ModelMap modelMap, HttpSession session,TechCategory techCategory) {
        // 查询
        session.setAttribute("examNo",examNo);
        if(pageNum == null){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", smdQuestions);
        }else{ // 翻页
            smdQuestions = (SmdQuestions) session.getAttribute("selective");
        }

        Page<SmdQuestions> page = (Page<SmdQuestions>) this.oltsSelectService.selectBySelective(techCategory,smdQuestions, pageNum, 10);
        modelMap.addAttribute("mypage", page);

        return "paper/addSingleQuestion";
    }
/*
* 查询多选题用
* */
        @RequestMapping("/searchsmd_muticheck")
    public String searchByMuticheck(SmdQuestions smdQuestions, Integer pageNum, ModelMap modelMap, HttpSession session,TechCategory techCategory) {
        // 查询
        if(pageNum == null){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", smdQuestions);
        }else{ // 翻页
            smdQuestions = (SmdQuestions) session.getAttribute("selective");
        }

        Page<SmdQuestions> page = (Page<SmdQuestions>) this.oltsSelectService.selectBySelective(techCategory,smdQuestions, pageNum, 5);
        modelMap.addAttribute("mypage", page);

        return "select/muticheckselect";
    }

    /*
    * 试卷维护增添多选题用
    * */
    @RequestMapping("/searChSmd_MutiCheck")
    public String searchByMuticheckAll(SmdQuestions smdQuestions, Integer pageNum, ModelMap modelMap, HttpSession session,TechCategory techCategory) {
        // 查询
        if(pageNum == null){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", smdQuestions);
        }else{ // 翻页
            smdQuestions = (SmdQuestions) session.getAttribute("selective");
        }

        Page<SmdQuestions> page = (Page<SmdQuestions>) this.oltsSelectService.selectBySelective(techCategory,smdQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);

        return "paper/addMutiCheckSelect";
    }
/*
* 查询试题用
* */
    @RequestMapping("/searchsmd_judge")
    public String searchByJudge(SmdQuestions smdQuestions, Integer pageNum, ModelMap modelMap, HttpSession session,TechCategory techCategory) {
        // 查询
        if(pageNum == null ){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", smdQuestions);

        }else{ // 翻页
            smdQuestions = (SmdQuestions) session.getAttribute("selective");
        }

        Page<SmdQuestions> page = (Page<SmdQuestions>) this.oltsSelectService.selectByJudge(techCategory,smdQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);
        return "select/judgeselect";
    }
/*
* 试卷维护添加判断题
*
*
* */

    @RequestMapping("/searChSmd_Judge")
    public String searchByJudgeAll(SmdQuestions smdQuestions, Integer pageNum, ModelMap modelMap, HttpSession session,TechCategory techCategory) {
        // 查询
        if(pageNum == null ){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", smdQuestions);

        }else{ // 翻页
            smdQuestions = (SmdQuestions) session.getAttribute("selective");
        }

        Page<SmdQuestions> page = (Page<SmdQuestions>) this.oltsSelectService.selectByJudge(techCategory,smdQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);
        return "paper/addJudgeSelect";
    }


    @RequestMapping("/updateJudge")
    public String updateJudge(SmdQuestions smdQuestions){
        int i= this.oltsSelectService.updateJudge(smdQuestions);
        if(i==1){

            return "redirect:/select/searchsmd_judge?questionType=3";
        }else {

            return null;
        }


    }
    @RequestMapping("/deleteJudge")
    public String deleteJudge(SmdQuestions smdQuestions, ModelMap modelMap,HttpServletResponse response) throws IOException {
        System.out.println(smdQuestions);
        if(this.oltsSelectService.deleteJudge(smdQuestions)){
            response.getWriter().write("{\"actionFlag\":true}");

        }else {
            response.getWriter().write("{\"actionFlag\":false}");
        }


        return null;
    }

    @RequestMapping("/updateChoice1")
    public String updateChoice1(SmdQuestions smdQuestions){
        int i= this.oltsSelectService.updateChoiceimpl(smdQuestions);
        if(i==1){

            return   "redirect:/select/searchsmd_radio?questionType=1";
        }else {

            return null;
        }


    }
    @RequestMapping("/updateChoice2")
    public String updateChoice2(SmdQuestions smdQuestions){
        int i= this.oltsSelectService.updateChoiceimpl(smdQuestions);
        if(i==1){

            return "redirect:/select/searchsmd_muticheck?questionType=2";
        }else {

            return null;
        }
    }
    @RequestMapping("/searchfsp_fill")
    public String searchByfill(FspQuestions fspQuestions, Integer pageNum, ModelMap modelMap, HttpSession session, TechCategory techCategory) {
        // 查询
        if(pageNum == null ){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", fspQuestions);

        }else{ // 翻页
            fspQuestions = (FspQuestions) session.getAttribute("selective");
        }

        Page<FspQuestions> page = (Page<FspQuestions>) this.oltsSelectService.selectByFsp(techCategory,fspQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);
        return "select/fillselect";
    }

    @RequestMapping("/searchfsp_ask")
    public String searchByAsk(FspQuestions fspQuestions, Integer pageNum, ModelMap modelMap, HttpSession session, TechCategory techCategory) {
        // 查询
        if(pageNum == null ){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", fspQuestions);

        }else{ // 翻页
            fspQuestions = (FspQuestions) session.getAttribute("selective");
        }

        Page<FspQuestions> page = (Page<FspQuestions>) this.oltsSelectService.selectByFsp(techCategory,fspQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);
        return "select/askselect";
    }

    @RequestMapping("/searchfsp_pro")
    public String searchBypro(FspQuestions fspQuestions, Integer pageNum, ModelMap modelMap, HttpSession session, TechCategory techCategory) {
        // 查询
        if(pageNum == null ){
            pageNum = 1;
            //保存查询条件
            session.setAttribute("selective", fspQuestions);

        }else{ // 翻页
            fspQuestions = (FspQuestions) session.getAttribute("selective");
        }

        Page<FspQuestions> page = (Page<FspQuestions>) this.oltsSelectService.selectByFsp(techCategory,fspQuestions, pageNum, 3);
        modelMap.addAttribute("mypage", page);
        return "select/programselect";
    }

    @RequestMapping("/updateFill")
    public String updateFill(FspQuestions fspQuestions){
        int i = this.oltsSelectService.updateFsp(fspQuestions);
        if (i==1){
            return "select/fillselect";
        }else {
            return null;
        }
    }

    @RequestMapping("/updateAsk")
    public String updateAsk(FspQuestions fspQuestions){
        int i = this.oltsSelectService.updateFsp(fspQuestions);
        if (i==1){
            return "select/askselect";
        }else {
            return null;
        }
    }
    @RequestMapping("/updatePro")
    public String updatePro(FspQuestions fspQuestions){
        int i = this.oltsSelectService.updateFsp(fspQuestions);
        if (i==1){
            return "select/programselect";
        }else {
            return null;
        }
    }

    @RequestMapping("/deleteFsp")
    public String deleteFsp(FspQuestions fspQuestions, ModelMap modelMap,HttpServletResponse response) throws IOException {

        if(this.oltsSelectService.deleteFsp(fspQuestions)){
            response.getWriter().write("{\"actionFlag\":true}");

        }else {
            response.getWriter().write("{\"actionFlag\":false}");
        }


        return null;
    }
}
