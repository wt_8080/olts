package com.olts.controller;

import com.olts.service.ExamService;
import com.olts.service.FspAnwserService;
import com.olts.service.OltsScoreService;
import com.olts.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by bingge on 2018/10/8.
 */
@Controller
@RequestMapping("/paper")
public class ExamController {
    private ExamService examService;
    private OltsScoreService oltsScoreService;
    private FspAnwserService fspAnwserService;

    @RequestMapping("/selectPaper")
    public String doSelectPaper(Model model){
        //试卷id先固定死，后面再从session里面取
        Examination examination = examService.selectExamById("xhu201810081");
        model.addAttribute("exam",examination);
        return "/paper/exam";
    }

    @RequestMapping("/handInPaper")
    public String doHandInPaper(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws IOException {
        //得到试卷编号
        String examNo = request.getParameter("examNo");
        //得到用户编号
        OltsUser user = (OltsUser)session.getAttribute("logUser");
        int userId = 0;
        if (user != null) {
            userId = user.getId();
        }
        if (StringUtils.isNotBlank(examNo) && userId != 0){
            //查询是否已提交试卷
            int rows = fspAnwserService.selectById(examNo, userId);
            if (rows==0) {//未提交试卷
                //从数据库查询出试卷
                Examination examination = examService.selectExamById(examNo);
                //客观题分数
                double score = 0.0;

                //从试卷中得到单选题列表
                List<SmdQuestions> singleAnwser = examination.getSingleAnwser();
                if (singleAnwser != null) {
                    //遍历单选题列表，计算单选题的分数
                    for (SmdQuestions single : singleAnwser) {
                        //得到单选题答案
                        String singleOption = request.getParameter("single" + single.getId());
                        if (StringUtils.isNotBlank(singleOption)) {
                            if (StringUtils.equals(singleOption, single.getCorrect())) {
                                score += 2;
                            }
                        }
                    }
                }

                //从试卷中得到多选题列表
                List<SmdQuestions> multipleAnwser = examination.getMultipleAnwser();
                if (multipleAnwser != null) {
                    //遍历多选题列表，计算多选题的分数
                    for (SmdQuestions multiple : multipleAnwser) {
                        //得到多选题答案
                        String[] multipleOptions = request.getParameterValues("multiple" + multiple.getId());
                        StringBuffer sb = new StringBuffer();
                        if (multipleOptions != null) {
                            for (String multipleOption : multipleOptions) {
                                sb.append(multipleOption.trim());
                            }
                        }
                        if (StringUtils.isNotBlank(sb.toString())) {
                            if (StringUtils.equals(sb.toString(), multiple.getCorrect())) {
                                score += 4;
                            }
                        }
                    }
                }

                //从试卷中得到判断题列表
                List<SmdQuestions> determineAnwser = examination.getDetermineAnwser();
                if (determineAnwser != null) {
                    //遍历判断题列表，计算判断题的分数
                    for (SmdQuestions determine : determineAnwser) {
                        //得到判断题答案
                        String determineOption = request.getParameter("determine" + determine.getId());
                        if (StringUtils.isNotBlank(determineOption)) {
                            if (StringUtils.equals(determineOption, determine.getCorrect())) {
                                score += 2;
                            }
                        }
                    }
                }

                //将客观题分数写入olts_score表
                oltsScoreService.insertScore(new OltsScore(null, score, null, new Date(), null, userId, examNo));

                //从试卷中得到填空题列表
                List<FspQuestions> fillInBlankAnwser = examination.getFillInBlankAnwser();
                //遍历填空题列表，计算填空题的分数
                if (fillInBlankAnwser != null){
                    for (FspQuestions fillInBlank : fillInBlankAnwser) {
                        //得到填空题答案
                        String fillInBlankValue = request.getParameter("fill_in_blank" + fillInBlank.getId());
                        if (StringUtils.isNotBlank(fillInBlankValue)) {
                            //将填空题答案写入fsp_anwser表
                            fspAnwserService.insertAnwser(new FspAnswer(null, fillInBlankValue, fillInBlank.getId(), examNo, userId));
                        }
                    }
                }

                //从试卷中得到简答题列表
                List<FspQuestions> simpleAnwser = examination.getSimpleAnwser();
                //遍历简答题列表，计算简答题的分数
                if (simpleAnwser != null) {
                    for (FspQuestions simple : simpleAnwser) {
                        //得到简答题答案
                        String simpleValue = request.getParameter("simple" + simple.getId());
                        if (StringUtils.isNotBlank(simpleValue)) {
                            //将简答题答案写入fsp_anwser表
                            fspAnwserService.insertAnwser(new FspAnswer(null, simpleValue, simple.getId(), examNo, userId));
                        }
                    }
                }


                //从试卷中得到编程题列表
                List<FspQuestions> programmingAnwser = examination.getProgrammingAnwser();
                //遍历编程题列表，计算编程题的分数
                if (programmingAnwser != null) {
                    for (FspQuestions programming : programmingAnwser) {
                        //得到编程题答案
                        String programmingValue = request.getParameter("programming" + programming.getId());
                        if (StringUtils.isNotBlank(programmingValue)) {
                            //将编程题答案写入fsp_anwser表
                            fspAnwserService.insertAnwser(new FspAnswer(null, programmingValue, programming.getId(), examNo, userId));
                        }
                    }
                }
                //ajax请求，向页面写入交卷成功的标志
                response.getWriter().write("{\"actionFlag\":true}");
                return null;
            }
        }
        //ajax请求，向页面写入交卷失败的标志
        response.getWriter().write("{\"actionFlag\":false}");
        return null;
    }

    public ExamService getExamService() {
        return examService;
    }

    @Resource
    public void setExamService(ExamService examService) {
        this.examService = examService;
    }

    public OltsScoreService getOltsScoreService() {
        return oltsScoreService;
    }

    @Resource
    public void setOltsScoreService(OltsScoreService oltsScoreService) {
        this.oltsScoreService = oltsScoreService;
    }

    public FspAnwserService getFspAnwserService() {
        return fspAnwserService;
    }

    @Resource
    public void setFspAnwserService(FspAnwserService fspAnwserService) {
        this.fspAnwserService = fspAnwserService;
    }
}
