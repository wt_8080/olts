package com.olts.controller;

import com.olts.service.IOltsAddService;
import com.olts.vo.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by yuan on 2018/10/8.
 */
@Controller
@RequestMapping("/add")
public class OltsAddController {

    @Resource
    IOltsAddService oltsaddService;

    @RequestMapping(value = "/load_courses", method = RequestMethod.GET) //此方法只能处理GET方式的请求
    @ResponseBody  // 此注解springMVC会使用jackson.jar库将List转换成JSON, [{id:101,name:"海淀"},...]
    public List<Courses> loadCourses() { // @ResponseBody也可加到返回类型的前面
        return this.oltsaddService.selectCourses();
    }

    @RequestMapping(value = "/loadTechCategoryByCoursesId", method = RequestMethod.GET)
    @ResponseBody
    public List<TechCategory> loadStreetsByCoursesId(@RequestParam("id") Integer coursesId) {
        return oltsaddService.selectTechCategoryByCoursesId(coursesId);
    }

    @RequestMapping("/insertRadio")/*类型123*/
    public String insertRadio(SmdQuestions smdQuestions, SmdOptions smdOptions,ModelMap Map) {
        int i = this.oltsaddService.insert(smdQuestions,smdOptions);
        Map.addAttribute("modelAdd",1);
        if(smdQuestions.getQuestionType()==1){
            return "forward:/views/add/radiochoice.jsp";
        } else  if(smdQuestions.getQuestionType()==2){
            return "forward:/views/add/multiplechoice.jsp";
        } else {
            return "forward:/views/add/checking.jsp";
        }
    }

    @RequestMapping("/insertAsk")/*类型456*/
    public String insertAsk(FspQuestions fspQuestions,ModelMap modelMap) {
        int i = this.oltsaddService.insertAsk(fspQuestions);
        modelMap.addAttribute("modelAdd",1);
        if(fspQuestions.getQuestionType()==4){
            return "forward:/views/add/completion.jsp";
        } else  if(fspQuestions.getQuestionType()==5){
            return "forward:/views/add/shortanswer.jsp";
        } else {
            return "forward:/views/add/programming.jsp";
        }
    }
}
