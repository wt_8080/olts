package com.olts.controller;

import com.olts.mapper.OltsPaperMapper;
import com.olts.service.ExamService;
import com.olts.service.FspAnwserService;
import com.olts.service.IOltsPaperService;
import com.olts.service.OltsScoreService;
import com.olts.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by czx on 2018/10/12.
 */
@Controller
@RequestMapping("/Exam")
public class ExamSafeController {

    @Resource
    private ExamService examService;
    @Resource
    private IOltsPaperService iOltsPaperService;
    /*
    * 加载页面
    * */
    @RequestMapping("/handInPaper")
    public String doHandInPaper(String examNo, ModelMap modelMap) throws IOException {
      /*  //得到试卷编号

        String examNo = request.getParameter("examNo");*/
      //从数据库查询出试卷
        Examination examination = examService.selectExamById(examNo);
        System.out.println(examination.toString());
        modelMap.addAttribute("exam",examination);
        //从试卷中得到单选题列表
        List<SmdQuestions> singleAnwser = examination.getSingleAnwser();
        for (SmdQuestions smdQuestions : singleAnwser) {
            System.out.println(smdQuestions.toString());
        }
        modelMap.addAttribute("singleSelection",singleAnwser);
        //从试卷中得到多选题列表
        List<SmdQuestions> multipleAnwser = examination.getMultipleAnwser();
        for (SmdQuestions smdQuestions : multipleAnwser) {
            System.out.println(smdQuestions.toString());
        }
        modelMap.addAttribute(" multiSelect",multipleAnwser);
        //从试卷中得到判断题列表
        List<SmdQuestions> determineAnwser = examination.getDetermineAnwser();
        modelMap.addAttribute("datermine",determineAnwser);
        //从试卷中得到填空题列表
        List<FspQuestions> fillInBlankAnwser = examination.getFillInBlankAnwser();
        modelMap.addAttribute("fillInBlank",fillInBlankAnwser);
        //从试卷中得到简答题列表
        List<FspQuestions> simpleAnwser = examination.getSimpleAnwser();
        modelMap.addAttribute("simple",simpleAnwser);
        //从试卷中得到编程题列表
        List<FspQuestions> programmingAnwser = examination.getProgrammingAnwser();
        modelMap.addAttribute("programming",programmingAnwser);
        return "paper/examContext";
    }

    /*
    * 删除单选
    * id  删除的单选的id
    * */
    @RequestMapping("delete_Single")
    public String deleteSingleQuestion(Examination examination,HttpServletResponse response) throws IOException {
        int i = iOltsPaperService.deleteSingleForExam(examination);
        if (i>0){
            response.getWriter().print("{\"resp\":true}");
        }else{
            response.getWriter().print("{\"resp\":false}");
        }
        return null;
    }
    /*
   * 删除多选
   * id  删除的试卷里的多选的id
   * */
    @RequestMapping("delete_Muti")
    public String deleteMutiQuestion(Examination examination,HttpServletResponse response) throws IOException {
        int i = iOltsPaperService.deleteMutiForExam(examination);
        if (i>0){
            response.getWriter().print("{\"resp\":true}");
        }else{
            response.getWriter().print("{\"resp\":false}");
        }
        return null;
    }
    /*
   * 删除判断
   * id  删除的试卷里的多选的id
   * */
    @RequestMapping("delete_Judge")
    public String deleteJudgeQuestion(Examination examination,HttpServletResponse response) throws IOException {
        int i = iOltsPaperService.deleteJudgeForExam(examination);
        if (i>0){
            response.getWriter().print("{\"resp\":true}");
        }else{
            response.getWriter().print("{\"resp\":false}");
        }
        return null;
    }
}
