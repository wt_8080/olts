package com.olts.service;

import com.olts.vo.Courses;
import com.olts.vo.TechCategory;

import java.util.List;

/**
 * Created by BLTM on 2018/10/8.
 */
public interface TechCategoryService {
    /*
     *@描述 用于将课程名全部展示出来
     *@参数
     *@返回值 List
     *@作者 xx
     *@日期 2018/10/8
     *@时间 14:49
    */
    public List<Courses> listCourses();

    /*
     *@描述  根据课程名id查询所有知识点
     *@参数
     *@返回值
     *@作者 xx
     *@日期 2018/10/8
     *@时间 17:05
    */
    public List<TechCategory> listTechs(
                                        Integer pageNum,
                                        Integer pageSize,Courses courses);

    /*
     *@描述 新增知识点
     *@参数 TechCategory
     *@返回值 boolean
     *@作者 xx
     *@日期 2018/10/10
     *@时间 9:36
    */
    public boolean insertTechById(TechCategory techCategory);

    /*
     *@描述 删除知识点
     *@参数 TechCategory
     *@返回值 boolean
     *@作者 xx
     *@日期 2018/10/10
     *@时间 9:36
    */
    public boolean deleteTechById(TechCategory techCategory);

    /*
     *@描述 更新知识点
     *@参数 TechCategory
     *@返回值 boolean
     *@作者 xx
     *@日期 2018/10/10
     *@时间 9:37
    */
    public boolean updateTechById(TechCategory techCategory);
}
