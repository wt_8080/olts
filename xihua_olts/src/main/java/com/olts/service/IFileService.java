package com.olts.service;

import com.olts.vo.OltsUser;

import java.util.List;

/**
 * Created by Administrator on 2018/10/10.
 */
public interface IFileService {
    int updatePath(Integer id,String destFileName);

    int insertUserAll(List<OltsUser> users);
}
