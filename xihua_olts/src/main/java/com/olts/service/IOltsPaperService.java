package com.olts.service;

import com.olts.vo.Examination;
import com.olts.vo.OltsUser;

import java.util.List;

/**
 * Created by CZX on 2018/10/7.
 */
public interface IOltsPaperService {
    List<OltsUser> selectScore(OltsUser oltsUser);

    List<OltsUser> selectBySelective(OltsUser oltsUser, int pageNum, int pageSize);

    List<OltsUser> selectAllScore(OltsUser oltsUser);

    int deleteOne(OltsUser oltsUser);

    int selectSingleQuestionIsExist(Examination examination);

    int addSingleQuestion(Examination examination);

    int selectMutiQuestionIsExist(Examination examination);

    int addMutiQuestion(Examination examination);

    int selectJudgeQuestionIsExist(Examination examination);

    int addJudgeQuestion(Examination examination);

    int setExamFlag(Examination examination);

    int deleteSingleForExam(Examination examination);

    int deleteMutiForExam(Examination examination);

    int deleteJudgeForExam(Examination examination);

    List<Examination> selectExamNo();
}
