package com.olts.service;

import com.github.pagehelper.Page;
import com.olts.vo.OltsUser;

import java.util.List;

/**
 * @author 王涛
 * @Created by WT on 2018/10/7.
 */
public interface IOltsUserService {

    /**
     * 登录时的查询
     * @param user
     * @return
     */
    public OltsUser login(OltsUser user);

    Page<OltsUser> selectByMarJor(String marjor,int pageNum,int pageSize);

    int delete(Integer[] param);

    OltsUser selectById(Integer id);

    int updateById(OltsUser user);

    /**
     * 注册
     * @param registerUser
     * @return
     */
    int insertRegister(OltsUser registerUser);
}
