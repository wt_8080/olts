package com.olts.service;

import com.olts.vo.*;

import java.util.List;

/**
 * Created by yuan on 2018/10/8.
 */
public interface IOltsAddService {

    /**
     * 加载课程列表
     * @return
     */
    List<Courses> selectCourses();

    /**
     * 加载知识点列表
     * @param coursesId
     * @return
     */
    List<TechCategory> selectTechCategoryByCoursesId(Integer coursesId);

    /**
     * 插入
     * @param smdQuestions
     * @param smdOptions
     * @return
     */
    int insert(SmdQuestions smdQuestions, SmdOptions smdOptions);

    int insertAsk(FspQuestions fspQuestions);
}
