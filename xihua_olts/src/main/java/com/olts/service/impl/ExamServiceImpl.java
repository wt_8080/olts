package com.olts.service.impl;

import com.olts.mapper.ExamMapper;
import com.olts.service.ExamService;
import com.olts.vo.Examination;
import com.olts.vo.SmdQuestions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by bingge on 2018/10/8.
 */
@Service("examService")
public class ExamServiceImpl implements ExamService {
    @Resource
    private ExamMapper mapper;

    @Override
    public Examination selectExamById(String id) {
        return mapper.selectExamById(id);
    }

    @Override
    public int deleteSingleQuestion(SmdQuestions smdQuestions) {
        int i = mapper.deleteSingleOption(smdQuestions);
        int i1 = mapper.deleteSingleQuestion(smdQuestions);
        if (i>0&&i1>0){
            return 1;
        }else{
            return 0;
        }
    }

    public ExamMapper getMapper() {
        return mapper;
    }

    /*@Resource
    public void setMapper(ExamMapper mapper) {
        this.mapper = mapper;
    }*/
}
