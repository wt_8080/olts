package com.olts.service.impl;

import com.olts.mapper.OltsSelectMapper;
import com.olts.service.IOltsSelectService;
import com.olts.vo.Courses;
import com.olts.vo.FspQuestions;
import com.olts.vo.SmdQuestions;
import com.olts.vo.TechCategory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wxk on 2018/10/8.
 */
@Service("oltsSelectService")

public class OltsSelectServiceImpl implements IOltsSelectService{


    static Logger logger = Logger.getLogger(OltsSelectServiceImpl.class);

    @Resource
    OltsSelectMapper oltsSelectMapper;

    @Override
    public List<Courses> selectCourse() {
        return oltsSelectMapper.selectCoures();
    }

    @Override
    public List<TechCategory> selectTechCategory(Integer courseId) {
        return oltsSelectMapper.selectTechCategory(courseId);
    }

    @Override
    public List<SmdQuestions> selectBySelective(TechCategory techCategory,SmdQuestions smdQuestions, int pageNum, int pageSize) {
        if(techCategory!=null){
            smdQuestions.setTechCategory(techCategory);
        }

        return oltsSelectMapper.selectBySelective(smdQuestions,pageNum,pageSize);
    }

    @Override
    public List<SmdQuestions> selectByJudge(TechCategory techCategory,SmdQuestions smdQuestions, int pageNum, int pageSize) {
        if(techCategory!=null){
            smdQuestions.setTechCategory(techCategory);
        }

        return oltsSelectMapper.selectByJudge(smdQuestions,pageNum,pageSize);
    }

    @Override
    public int updateJudge(SmdQuestions smdQuestions){
        return oltsSelectMapper.updateJudge(smdQuestions);
    }


    /**
     * 删除判断题
     * @param smdQuestions
     * @return
     */
    @Override
    public boolean deleteJudge(SmdQuestions smdQuestions) {

        if(oltsSelectMapper.deleteJudge(smdQuestions)==1){
            return true;
        }else {
            return false;
        }

    }
    @Override

    public int updateChoiceimpl(SmdQuestions smdQuestions) {
        int i = oltsSelectMapper.updateChoice(smdQuestions);
        System.out.println(i);
        int j = oltsSelectMapper.updateJudge(smdQuestions);
        if(i==1&&j==1) {

            return 1;

        }else {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;

    }

    @Override
    public List<FspQuestions> selectByFsp(TechCategory techCategory, FspQuestions fspQuestions, int pageNum, int pageSize) {
        if(techCategory!=null){
            fspQuestions.setTechCategory(techCategory);
        }
        return oltsSelectMapper.selectByFsp(fspQuestions,pageNum,pageSize);
    }

    @Override
    public int updateFsp(FspQuestions fspQuestions) {
        return oltsSelectMapper.updateFsp(fspQuestions);
    }

    @Override
    public boolean deleteFsp(FspQuestions fspQuestions) {
        if(oltsSelectMapper.deleteFsp(fspQuestions)==1){
            return true;
        }else {
            return false;
        }
    }
}
