package com.olts.service.impl;

import com.olts.mapper.OltsClassifyCoursesMapper;
import com.olts.service.OltsClassifyCoursesService;
import com.olts.vo.Courses;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2018/10/8 0008.
 */
@Service("oltsCoursesService")
public class OltsClassifyCoursesServiceImpl implements OltsClassifyCoursesService {

    /**
     *
     */
    @Resource
    private OltsClassifyCoursesMapper oltsClassifyCoursesMapper;
    @Override
    public List<Courses> selectAllCourses( int pageNum, int pageSize) {
        return oltsClassifyCoursesMapper.selectAllCourses(pageNum,pageSize);
    }

    @Override
    public Courses selectCoursesById(int id) {
        return oltsClassifyCoursesMapper.selectCoursesById(id);
    }

    @Override
    public int insertCourses(Courses courses) {
        return oltsClassifyCoursesMapper.insertCourses(courses);
    }

    @Override
    public int updateCourses(Courses courses) {
        return oltsClassifyCoursesMapper.updateCourses(courses);
    }

    @Override
    public int deleteCourseById(int id) {
        return oltsClassifyCoursesMapper.deleteCourseById(id);
    }

}
