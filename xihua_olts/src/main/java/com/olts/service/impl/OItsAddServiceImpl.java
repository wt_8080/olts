package com.olts.service.impl;

import com.olts.mapper.OltsAddMapper;
import com.olts.service.IOltsAddService;
import com.olts.vo.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by yuan on 2018/10/8.
 */
@Service("oltsaddService")
public class OItsAddServiceImpl implements IOltsAddService{

    @Resource
    OltsAddMapper oltsAddMapper;

    @Override
    public List<Courses> selectCourses() {
        return oltsAddMapper.selectCourses();
    }

    @Override
    public List<TechCategory> selectTechCategoryByCoursesId(Integer coursesId) {
        return oltsAddMapper.selectTechCategoryByCoursesId(coursesId);
    }

    @Override
    public int insert(SmdQuestions smdQuestions, SmdOptions smdOptions) {
        int i1 = oltsAddMapper.insertRadio(smdQuestions);
        smdOptions.setQuestionId(smdQuestions.getId());
        System.out.println(smdQuestions.getId());
        if(smdQuestions.getQuestionType()!=3) {
            int i = oltsAddMapper.insertOption(smdOptions);
            if(i!=1){
                try {
                    throw new Exception("插入选项失败！");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }
        }
        if(i1!=1){
            try {
                throw new Exception("插入问题失败！");

            } catch (Exception e) {
                e.printStackTrace();
            }
                return 0;
        }
        return 1;
    }

    @Override
    public int insertAsk(FspQuestions fspQuestions) {
        return oltsAddMapper.insertAsk(fspQuestions);
    }
}
