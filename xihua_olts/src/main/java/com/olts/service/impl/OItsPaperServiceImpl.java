package com.olts.service.impl;

import com.olts.mapper.OltsPaperMapper;
import com.olts.service.IOltsPaperService;
import com.olts.vo.Examination;
import com.olts.vo.OltsUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by CZX on 2018/10/7.
 */
@Service("oltsPaperService")
public class OItsPaperServiceImpl implements IOltsPaperService {
    @Resource
    OltsPaperMapper oltsPaperMapper;
    @Override
    public List<OltsUser> selectScore(OltsUser oltsUser) {
        return oltsPaperMapper.selectScore(oltsUser);
    }

    @Override
    public List<OltsUser> selectBySelective(OltsUser oltsUser, int pageNum, int pageSize) {
        return oltsPaperMapper.selectScore(oltsUser,pageNum,pageSize);
    }

    @Override
    public List<OltsUser> selectAllScore(OltsUser oltsUser) {
        return oltsPaperMapper.selectAllScore(oltsUser);
    }

    @Override
    public int deleteOne(OltsUser oltsUser) {
        int i = oltsPaperMapper.deleteOne(oltsUser);
        return i;
    }

    @Override
    public int selectSingleQuestionIsExist(Examination examination) {
        int i = oltsPaperMapper.selectSingleQuestionIsExist(examination);
        return i;
    }

    @Override
    public int addSingleQuestion(Examination examination) {

        return  oltsPaperMapper.addSingleQuestion(examination);
    }

    @Override
    public int selectMutiQuestionIsExist(Examination examination) {
        return oltsPaperMapper.selectMutiQuestionIsExist(examination);
    }

    @Override
    public int addMutiQuestion(Examination examination) {
        return oltsPaperMapper.addMutiQuestion(examination);
    }

    @Override
    public int selectJudgeQuestionIsExist(Examination examination) {
        return  oltsPaperMapper.selectJudgeQuestionIsExist(examination);
    }

    @Override
    public int addJudgeQuestion(Examination examination) {
        return oltsPaperMapper.addJudgeQuestion(examination);
    }

    @Override
    public int setExamFlag(Examination examination) {
        return oltsPaperMapper.setExamFlag(examination);
    }

    @Override
    public int deleteSingleForExam(Examination examination) {
        /*返回试卷中单选题的题号*/
        String s = oltsPaperMapper.selectSingleForExam(examination);
        System.out.println(s);
        String[] split = s.split(",");
        StringBuffer stringBuffer = new StringBuffer();
        for (String s1 : split) {
            if (examination.getSingleId().equals(s1)){
                continue;
            }else {

                stringBuffer.append(s1);
                stringBuffer.append(",");
            }
        }
        int last = stringBuffer.lastIndexOf(",");
        stringBuffer.deleteCharAt(last);
        /*String substring = stringBuffer.reverse().substring(1);*/
        String substring = stringBuffer.substring(0);
        stringBuffer.toString();
        String s2 = new String(stringBuffer);
        System.out.println(s2);
        Examination examination1 = new Examination();
        examination1.setExamNo(examination.getExamNo());
        examination1.toString();
        examination1.setSingleId(substring);
        int i = oltsPaperMapper.deleteSingleForExam(examination1);
        return i;
    }

    @Override
    public int deleteMutiForExam(Examination examination) {
         /*返回试卷中单选题的题号*/
        String s = oltsPaperMapper.selectMutiForExam(examination);
        System.out.println(s);
        String[] split = s.split(",");
        StringBuffer stringBuffer = new StringBuffer();
        for (String s1 : split) {
            if (examination.getMultipleId().equals(s1)){
                continue;
            }else {

                stringBuffer.append(s1);
                stringBuffer.append(",");
            }
        }
        int last = stringBuffer.lastIndexOf(",");
        stringBuffer.deleteCharAt(last);
        String substring = stringBuffer.substring(1);
        stringBuffer.toString();
        String s2 = new String(stringBuffer);
        System.out.println(s2);
        Examination examination1 = new Examination();
        examination1.setExamNo(examination.getExamNo());
        examination1.toString();
        examination1.setMultipleId(substring);

        int i = oltsPaperMapper.deleteMutiForExam(examination1);
        return i;
    }

    @Override
    public int deleteJudgeForExam(Examination examination) {
        String s = oltsPaperMapper.selectJudgeForExam(examination);
        System.out.println(s);
        String[] split = s.split(",");
        StringBuffer stringBuffer = new StringBuffer();
        for (String s1 : split) {
            if (examination.getTrueFalseId().equals(s1)){
                continue;
            }else {

                stringBuffer.append(s1);
                stringBuffer.append(",");
            }
        }
        int last = stringBuffer.lastIndexOf(",");
        stringBuffer.deleteCharAt(last);
        String substring = stringBuffer.substring(1);
        stringBuffer.toString();
        String s2 = new String(stringBuffer);
        System.out.println(s2);
        Examination examination1 = new Examination();
        examination1.setExamNo(examination.getExamNo());
        examination1.toString();
        examination1.setTrueFalseId(substring);

        int i = oltsPaperMapper.deleteJudgeForExam(examination1);
        return i;
    }

    @Override
    public List<Examination> selectExamNo() {
        return oltsPaperMapper.selectExamNo();
    }
}
