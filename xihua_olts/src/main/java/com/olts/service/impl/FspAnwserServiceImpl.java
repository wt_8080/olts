package com.olts.service.impl;

import com.olts.mapper.FspAnwserMapper;
import com.olts.service.FspAnwserService;
import com.olts.vo.FspAnswer;
import com.olts.vo.OltsUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2018/10/10.
 */
@Service("fspAnwserService")
public class FspAnwserServiceImpl implements FspAnwserService {
    private FspAnwserMapper mapper;

    @Override
    public int insertAnwser(FspAnswer anwser) {
        return mapper.insertAnwser(anwser);
    }

    @Override
    public int selectById(String examNo, int userId) {
        return mapper.selectById(examNo,userId);
    }

    @Override
    public List<OltsUser> selectAllAnswer(String examNo) {
        return mapper.selectAllAnwser(examNo);
    }

    public FspAnwserMapper getMapper() {
        return mapper;
    }

    @Resource
    public void setMapper(FspAnwserMapper mapper) {
        this.mapper = mapper;
    }
}
