package com.olts.service.impl;

import com.olts.mapper.FileMapper;
import com.olts.service.IFileService;
import com.olts.vo.OltsUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2018/10/10.
 */
@Service("fileService")
public class FileServiceImpl implements IFileService{

    @Resource
    FileMapper fileMapper;

    @Override
    public int updatePath(Integer id,String destFileName) {
        return this.fileMapper.updatePath(id,destFileName);
    }

    @Override
    public int insertUserAll(List<OltsUser> users) {
        return this.fileMapper.insertAllUser(users);
    }
}
