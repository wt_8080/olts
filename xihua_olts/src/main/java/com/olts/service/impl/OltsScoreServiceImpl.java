package com.olts.service.impl;

import com.olts.mapper.OltsScoreMapper;
import com.olts.service.OltsScoreService;
import com.olts.vo.OltsScore;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by bingge on 2018/10/10.
 */
@Service("oltsScoreService")
public class OltsScoreServiceImpl implements OltsScoreService {
    private OltsScoreMapper mapper;

    @Override
    public int insertScore(OltsScore score) {
        return mapper.insertScore(score);
    }

    @Override
    public int updateScore(OltsScore oltsScore) {
        return mapper.updateScore(oltsScore);
    }

    @Override
    public OltsScore selectById(int userId, String examNo) {
        return mapper.selectById(userId,examNo);
    }

    public OltsScoreMapper getMapper() {
        return mapper;
    }

    @Resource
    public void setMapper(OltsScoreMapper mapper) {
        this.mapper = mapper;
    }
}
