package com.olts.service.impl;

import com.olts.mapper.TechCategoryMapper;
import com.olts.service.TechCategoryService;
import com.olts.vo.Courses;
import com.olts.vo.TechCategory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by BLTM on 2018/10/8.
 */

/*
 *@program 项目名称
 *@description 描述
 *@author 作者
 *@time 2018/10/8 14:50 
*/
@Service
public class TechCategoryServiceImpl implements TechCategoryService {

    static Logger logger = Logger.getLogger(TechCategoryServiceImpl.class);

    @Resource
    private TechCategoryMapper techCategoryMapper;

    @Override
    public List<Courses> listCourses() {
        List<Courses> courses = techCategoryMapper.selectAll();
        return courses;
    }

    @Override
    public List<TechCategory> listTechs(
                                        Integer pageNum,
                                        Integer pageSize,
                                        Courses courses) {
        List<TechCategory> techCategories = techCategoryMapper.selectTechByCourseId(pageNum,pageSize,courses);
        return techCategories;
    }

    @Override
    public boolean insertTechById(TechCategory techCategory) {
        int n = techCategoryMapper.insertTechById(techCategory);
        return n>0;
    }

    @Override
    public boolean deleteTechById(TechCategory techCategory) {
        int n = techCategoryMapper.deleteTechById(techCategory);
        return n>0;
    }

    @Override
    public boolean updateTechById(TechCategory techCategory) {
        int n = techCategoryMapper.updateTechById(techCategory);
        return n>0;
    }


}
