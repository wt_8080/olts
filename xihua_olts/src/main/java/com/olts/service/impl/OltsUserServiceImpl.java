package com.olts.service.impl;

import com.github.pagehelper.Page;
import com.olts.mapper.OltsUserMapper;
import com.olts.service.IOltsUserService;
import com.olts.vo.OltsUser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.sql.SQLException;

/**
 * @author 王涛
 * @Created by WT on 2018/10/7.
 */
@Service("oltsUserService")
public class OltsUserServiceImpl implements IOltsUserService {

    static Logger logger = Logger.getLogger(OltsUserServiceImpl.class);

    @Resource
    private OltsUserMapper oltsUserMapper;

    @Override
    public OltsUser login(OltsUser user) {
        OltsUser login = oltsUserMapper.login(user);
        return login;
    }

    @Override
    public Page<OltsUser> selectByMarJor(String marjor, int pageNum, int pageSize) {
        return oltsUserMapper.selectByMarJor(marjor,pageNum,pageSize);
    }

    @Override
    public int delete(Integer[] param) {
        return this.oltsUserMapper.deleteById(param);
    }

    @Override
    public OltsUser selectById(Integer id) {
        return this.oltsUserMapper.selectById(id);
    }

    @Override
    public int updateById(OltsUser user) {
        return this.oltsUserMapper.updateById(user);
    }



    @Override
    public int insertRegister(OltsUser registerUser) {

        return oltsUserMapper.insertRegister(registerUser);
    }
}
