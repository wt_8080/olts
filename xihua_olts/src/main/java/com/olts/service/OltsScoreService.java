package com.olts.service;

import com.olts.vo.OltsScore;

/**
 * Created by bingge on 2018/10/10.
 */
public interface OltsScoreService {
    /**
     * 插入olts_score表
     * @param score 要插入的数据对象
     * @return 插入的行数
     */
    int insertScore(OltsScore score);

    /**
     * 更新成绩
     * @param oltsScore 需要更新的成绩对象，包含试卷编号、用户编号（用作查询条件），
     *                    总成绩，主观题成绩（真正更新的值）
     * @return 更新的行数
     */
    int updateScore(OltsScore oltsScore);

    /**
     * 通过试卷编号、用户编号查询成绩
     * @param userId 用户编号
     * @param examNo 试卷编号
     * @return
     */
    OltsScore selectById(int userId,String examNo);
}
