package com.olts.service;

import com.olts.vo.Examination;
import com.olts.vo.SmdQuestions;

/**
 * Created by bingge on 2018/10/8.
 */
public interface ExamService {
    /**
     * 按编号查询试卷
     * @param id 试卷id
     * @return 根据试卷id查询的试卷对象
     */
    Examination selectExamById(String id);
    int deleteSingleQuestion(SmdQuestions smdQuestions);
}
