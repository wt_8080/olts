package com.olts.service;

import com.olts.vo.Courses;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2018/10/8 0008.
 */
public interface OltsClassifyCoursesService {
    List<Courses> selectAllCourses(@Param("pageNum") int pageNum,
                                   @Param("pageSize") int pageSize);
    Courses selectCoursesById(int id);
    int insertCourses(Courses courses);
    int updateCourses(Courses courses);
    int deleteCourseById(int id);
}
