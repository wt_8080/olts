package com.olts.service;

import com.olts.vo.*;

import java.util.List;

/**
 * Created by wxk on 2018/10/8.
 */
public interface IOltsSelectService {


    /**
     * 加载课程
     * @param
     * @return
     */
    public List<Courses> selectCourse();

    /**
     *加载知识点
     * @param courseId
     * @return
     */
    public List<TechCategory> selectTechCategory(Integer courseId);

    /**
     * 查询单选，多选，判断题
     * @param smdQuestions
     * @param pageNum
     * @param pageSize
     * @return
     */
    public List<SmdQuestions> selectBySelective(TechCategory techCategory, SmdQuestions smdQuestions,int pageNum,int pageSize);

    public List<SmdQuestions> selectByJudge(TechCategory techCategory, SmdQuestions smdQuestions,int pageNum,int pageSize);

    /**
     * 更新判断题
     * @param SmdQuestions
     * @return
     */
    public int updateJudge(SmdQuestions SmdQuestions);
    
    public boolean deleteJudge(SmdQuestions smdQuestions);
    public int updateChoiceimpl(SmdQuestions SmdQuestions);

    public List<FspQuestions> selectByFsp(TechCategory techCategory,FspQuestions fspQuestions,int pageNum,int pageSize);

    public int updateFsp(FspQuestions fspQuestions);

    public  boolean deleteFsp(FspQuestions fspQuestions);
}

