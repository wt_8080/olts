package com.olts.service;

import com.olts.vo.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by wxk on 2018/10/8.
 */
@ContextConfiguration("classpath:applicationContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class OltsSelectServiceImplTest {
    @Autowired
    IOltsSelectService oltsSelectService;

    /**
     * 测试课程查询
     * @throws Exception
     */
    @Test
    public void testselectCoures() throws Exception {
        for (Courses courses : oltsSelectService.selectCourse()) {
            System.out.println(courses);
        }
    }

    /**
     * 测试知识点查询
     * @throws Exception
     */
    @Test
    public void testselectTechCategory() throws Exception {
        for (TechCategory techCategory : oltsSelectService.selectTechCategory(1)) {
            System.out.println(techCategory);
        }
    }

    @Test
    public void testselectSmd() throws Exception {

        Courses courses=new Courses();
/*        courses.setCourseName("JavaSE");*/
/*        courses.setId(1);*/
        TechCategory techCategory=new TechCategory();
        techCategory.setCourseId(1);
        SmdQuestions smdQuestions=new SmdQuestions();
        smdQuestions.setTechCategory(techCategory);




        List<SmdQuestions> page = oltsSelectService.selectBySelective(techCategory,smdQuestions, 1, 10);
        for (SmdQuestions questions : page) {
            System.out.println(questions);

        }
    }

    @Test
    public void testupdateJudge() throws Exception {
        SmdQuestions smdQuestions=new SmdQuestions();
        smdQuestions.setId(15);
        smdQuestions.setQuestion("帅晓文是chuncai");
        smdQuestions.setCorrect("√");
        oltsSelectService.updateJudge(smdQuestions);
    }

    @Test
    public void testDeleteJudge() throws Exception {
        SmdQuestions smdQuestions=new SmdQuestions();
        smdQuestions.setId(15);
        System.out.println(oltsSelectService.deleteJudge(smdQuestions));
    }

    @Test
    public void testupdateChoice() throws Exception {
        SmdQuestions smdQuestions=new SmdQuestions();
        smdQuestions.setId(9);
        SmdOptions smdOptions = new SmdOptions();
        smdOptions.setQuestionId(9);
        smdQuestions.setQuestion("帅晓文是chuncai");
        smdQuestions.setCorrect("ABCD");
        smdOptions.setOptionA("qqq");
        smdQuestions.setSmdOptions(smdOptions);

        System.out.println(oltsSelectService.updateChoiceimpl(smdQuestions));


    }

    @Test
    public void selectByFsp() throws Exception {
        TechCategory techCategory = new TechCategory();
        FspQuestions fsp = new FspQuestions();
        for (FspQuestions fspQuestions : oltsSelectService.selectByFsp(techCategory, fsp, 1, 5)) {
            System.out.println(fspQuestions);
        }
    }

    @Test
    public void update() throws Exception {
        FspQuestions fspQuestions=new FspQuestions();
        fspQuestions.setId(4);
        fspQuestions.setDescrpt("简单测试");
        oltsSelectService.updateFsp(fspQuestions);
    }
}
