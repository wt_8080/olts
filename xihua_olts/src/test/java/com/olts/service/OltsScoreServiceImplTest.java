package com.olts.service;

import com.olts.vo.OltsScore;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/**
 * Created by bingge on 2018/10/8.
 */
public class OltsScoreServiceImplTest {
    // spring容器实例
    static ApplicationContext context;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        //使用类路径中的XML文件来初始化spring容器实例
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @Test
    public void testInserScore() {
        OltsScoreService oss = (OltsScoreService)context.getBean("oltsScoreService");
        int i = oss.insertScore(new OltsScore(null,77.0,null,new Date(),null,2,"xhu201810081"));
        if (i!=0){
            System.out.println("插入成功！");
        }else {
            System.out.println("插入失败！");
        }
    }

    @Test
    public void testUpdateScore() {
        OltsScoreService oss = (OltsScoreService)context.getBean("oltsScoreService");
        int i = oss.updateScore(new OltsScore(null,77.0,15.0,null,null,2,"xhu201810081"));
        if (i!=0){
            System.out.println("更新成功！");
        }else {
            System.out.println("更新失败！");
        }
    }

    @Test
    public void testSelectScore() {
        OltsScoreService oss = (OltsScoreService)context.getBean("oltsScoreService");
        OltsScore oltsScore = oss.selectById(1, "xhu201810081");
        if (oltsScore != null) {
            System.out.println(oltsScore);
        }
    }
}
