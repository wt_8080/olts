package com.olts.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author 王涛
 * @Created by WT on 2018/10/13.
 */
@ContextConfiguration("classpath:applicationContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class OltsClassifyCoursesServiceImplTest {
    @Autowired
    OltsClassifyCoursesService oltsClassifyCoursesService;

    @Test
    public void Test() throws Exception {
        int i = oltsClassifyCoursesService.deleteCourseById(43);
        System.out.println(i);
    }
}
