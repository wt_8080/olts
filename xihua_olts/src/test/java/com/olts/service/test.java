package com.olts.service;

import com.olts.mapper.ExamMapper;
import com.olts.service.impl.ExamServiceImpl;
import com.olts.vo.Examination;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by czx on 2018/10/12.
 */
@ContextConfiguration("classpath:applicationContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class test {
    @Resource
    static ExamMapper examMapper;
    @Resource
    static ExamServiceImpl service;
    @Test
    public void testOne(){
        /*service.selectExamById("xhu201810081").*/

        Examination result = examMapper.selectExamById("xhu201810081");
        System.out.println(result.toString());
    }
}
