package com.olts.service;

import com.olts.vo.OltsUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author 王涛
 * @Created by WT on 2018/10/7.
 */
/*测试一下*/
@ContextConfiguration("classpath:applicationContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class OltsUserServiceImplTest {
    @Autowired
    IOltsUserService oltsUserService;

    @Test
    public void testLogin() throws Exception {
        OltsUser loginUser = new OltsUser();
        loginUser.setUserName("bb");
        loginUser.setPassWord("bb");
        OltsUser login = oltsUserService.login(loginUser);
        System.out.println(login);
    }

    @Test
    public void testRegister() throws Exception {
        OltsUser loginUser = new OltsUser();
        loginUser.setStuNo("3120150901603");
        loginUser.setIdCardNo("555555555555555555");
        loginUser.setUserName("wt");
        loginUser.setPassWord("wt");
        int register = oltsUserService.insertRegister(loginUser);
        System.out.println(register);
    }
}
