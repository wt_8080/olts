package com.olts.service;

import com.olts.vo.FspAnswer;
import com.olts.vo.OltsUser;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by bingge on 2018/10/8.
 */
public class FspAnwserServiceImplTest {
    // spring容器实例
    static ApplicationContext context;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        //使用类路径中的XML文件来初始化spring容器实例
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @Test
    public void testInserAnwser() {
        FspAnwserService fas = (FspAnwserService)context.getBean("fspAnwserService");
        int i = fas.insertAnwser(new FspAnswer(null,"我真的不知道。",1,"xhu201810081",1));
        if (i!=0){
            System.out.println("插入成功！");
        }else {
            System.out.println("插入失败！");
        }
    }

    @Test
    public void testSelectById() {
        FspAnwserService fas = (FspAnwserService)context.getBean("fspAnwserService");
        int i = fas.selectById("xhu201810081",1);
        if (i==0){
            System.out.println("可以交卷！");
        }else {
            System.out.println("不可以交卷！");
        }
    }

    @Test
    public void selectAllAnswer(){
        FspAnwserService fas = (FspAnwserService)context.getBean("fspAnwserService");
        List<OltsUser> list = fas.selectAllAnswer("xhu201810081");
        for (OltsUser u : list) {
            System.out.println("USER: " + u.getId() + ", " + u.getUserName());
            System.out.println("SCORE: " + u.getOltsScore().getId() + ", " + u.getOltsScore().getScore());
            for (FspAnswer answer : u.getFspAnswer()) {
                System.out.println("ANWSER: " + answer.getId()+ "," + answer.getAnswer() + "," + answer.getExamNo());
                System.out.println("QUESTION:" + answer.getFspQuestions().getId() + "," +
                        answer.getFspQuestions().getQuestion() + "," +
                        answer.getFspQuestions().getStdAnswer() + "," +
                        answer.getFspQuestions().getQuestionType());
            }
            System.out.println("---------------------------------");
        }
    }
}
