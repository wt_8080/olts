package com.olts.service;

import com.olts.vo.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by yuan on 2018/10/8.
 */
@ContextConfiguration("classpath:applicationContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class OltsAddServiceImplTest {

    @Autowired
    IOltsAddService oltsAddService;

    @Test
    public void testCourses() throws Exception {
        List<Courses> courses = oltsAddService.selectCourses();
        for (Courses cours : courses) {
            System.out.println(cours);
        }
    }

    @Test
    public void testTechCategory() throws Exception {
        List<TechCategory> techCategories = oltsAddService.selectTechCategoryByCoursesId(1);
        for (TechCategory techCategory : techCategories) {
            System.out.println(techCategory);
        }
    }

    @Test
    public void testSmdQuestions() throws Exception {
        SmdQuestions smdQuestions = new SmdQuestions();
        smdQuestions.setQuestion("单选题测试");
        smdQuestions.setCorrect("C");
        smdQuestions.setQuestionType(1);
        smdQuestions.setTechCateId(2);
        smdQuestions.setDescrpt("单选题测试");
        SmdOptions smdOptions = new SmdOptions();
        smdOptions.setOptionA("选项A");
        smdOptions.setOptionB("选项B");
        smdOptions.setOptionC("选项C");
        smdOptions.setOptionD("选项D");
        smdOptions.setQuestionId(3);
        int insert = oltsAddService.insert(smdQuestions,smdOptions);
        System.out.println(insert);
    }

    @Test
    public void testFspQuestions() throws Exception {
        FspQuestions fspQuestions = new FspQuestions();
        fspQuestions.setQuestion("这是简答题");
        fspQuestions.setStdAnswer("标准答案");
        fspQuestions.setQuestionType(4);
        fspQuestions.setTechCateId(1);
        fspQuestions.setDescrpt("简单作答");
        int i = oltsAddService.insertAsk(fspQuestions);
        System.out.println(i);
    }
}
